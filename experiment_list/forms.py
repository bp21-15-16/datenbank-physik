from django import forms
from .utilities import *
import json
from public.models import Experiment, Commit, Label, Document, Device, Category
import django.utils
from django.forms import ModelForm, inlineformset_factory

import json

import django.utils
from django import forms
from django.forms import ModelForm, inlineformset_factory
from .utilities import *


class commentForm(forms.Form):
    type = forms.ChoiceField(
        choices=[],
        widget=forms.Select(
            attrs={
                'class': "toggle_comment_type",
            }
        ),
        label="Kategorie",
    )

    message = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'cols': 0, 'rows': 5,
                'placeholder': 'Ihr Kommentar...'
            }
        )
    )


class CreateCategoryForm(ModelForm):
    """Form for creating a new category"""
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Name der Kategorie..."
            }
        ),
        label="Name",
    )

    token = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Kürzel der Kategorie..."
            }
        ),
        label="Kürzel",
    )

    parent = forms.ChoiceField(
        choices=[],
        label="Überkategorie",
        required=False,
    )

    class Meta:
        model = Category
        fields = ('name', 'token',)

    def clean_name(self):
        """Checks if category with identical name and parent already exists, denies creation if so"""
        name = self.cleaned_data.get('name')
        parent = self.data.get('parent')
        instance = self.instance
        try:
            if parent != '':
                match = Category.objects.get(name=name, parent=parent)
            else:
                match = Category.objects.get(name=name, parent__isnull=True)
            if (instance is not None) and (match == instance):
                return name
        except Category.DoesNotExist:
            # Unable to find a category, this is fine
            return name

        # Category with this name and parent was found, raise an error
        raise forms.ValidationError("Diese Kategorie mit dieser Überkategorie existiert bereits.")


class EditCategoryForm(ModelForm):
    """Form for editing an existing category"""
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Name der Kategorie..."
            }
        ),
        label="Name",
    )

    token = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Kürzel der Kategorie..."
            }
        ),
        label="Kürzel",
    )

    parent = forms.ChoiceField(
        widget=forms.Select(),
        choices=[],
        label="Überkategorie",
        required=False,
    )

    experiment = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': "experiment_select",
                'id': "experiment_id",
                'placeholder': "Experimente auswählen..."
            }
        ),
        label="Versuche",
        required=False,
    )

    class Meta:
        model = Category
        fields = ('name', 'token',)

    def clean_name(self):
        """Checks if category with identical name and parent already exists, denies change if so"""
        name = self.cleaned_data.get('name')
        parent = self.data.get('parent')
        instance = self.instance
        try:
            if parent != '':
                match = Category.objects.get(name=name, parent=parent)
            else:
                match = Category.objects.get(name=name, parent__isnull=True)
            if (instance is not None) and (match == instance):
                return name
        except Category.DoesNotExist:
            # Unable to find a category, this is fine
            return name

        # Category with this name and parent was found, raise an error
        raise forms.ValidationError("Diese Kategorie mit dieser Überkategorie existiert bereits.")


class ExperimentForm(forms.ModelForm):
    class Meta:
        model = Experiment
        name = "experiment"
        # fields = '__all__'
        fields = (
            'category', 'name', 'description', 'comment', 'preparation_time', 'status', 'execution_time',
            'safety_signs', 'experiment_number', 'tags')

    def __init__(self, *args, **kwargs):
        # to delete colon after field's name
        # kwargs.setdefault('label_suffix', '')
        super(ExperimentForm, self).__init__(*args, **kwargs)
        self.fields["category_list"].choices = getCategories(Category.objects.filter(depth=2), 2)
        if self.instance and self.instance.id is not None:
            self.fields["labels"].initial = get_experiment_labels(self.instance)
            self.fields["devices"].initial = get_experiment_devices(self.instance)

    def save(self, create_commits=False, user=None, commit=False):
        # if self.instance.id is None than the device is being created now
        # if creating a device confirmed_By is always None
        created = self.instance.id is None

        if create_commits:
            # TODO put the code below in another function
            # TODO to fill
            if created:
                experiment = super(ExperimentForm, self).save(commit=True)
                commit = Commit(experiment=experiment,
                                field="!create",
                                timestamp=django.utils.timezone.now(),
                                old_data="",
                                new_data="",
                                author=user,
                                confirmed_by=None,
                                confirmed_at=None,
                                status="auto_approved")
                commit.save()
            else:
                model = Experiment.objects.get(id=self.instance.id)
                for changed_element in self.changed_data:
                    if changed_element == "category_list":
                        continue
                    attr = getattr(model, changed_element)
                    old_data = attr if attr else ""
                    if changed_element == "tags":  # we get a list of dictionaries from tagify for the tags, we only want a list
                        old_data = json.dumps(sorted([e["value"] for e in json.loads(old_data)])) if attr else '[]'
                        new_data = '[]' if self.data[changed_element] == "" else json.dumps(sorted([e["value"] for e in json.loads(self.data[changed_element])]))
                    elif changed_element == "category":  # here we want to save the id for later reference, not the name
                        old_data = old_data.id if old_data else ""
                        new_data = self.cleaned_data[changed_element].id
                    elif bool(changed_element) and changed_element == "labels":
                        old_data = json.dumps(sorted([str(e.id) for e in Label.objects.filter(experiment=model)])) if attr else '[]'  # here we need a list of all label ids for our device
                        new_data = '[]' if self.data[changed_element] == "" else json.dumps(sorted([e["value"] for e in json.loads(self.data[changed_element])]))  # we get a list of dictionaries from tagify
                    elif bool(changed_element) and changed_element == "devices":
                        old_data = json.dumps(sorted([str(e.id) for e in Device.objects.filter(experiment=model)])) if attr else '[]'  # here we need a list of all label ids for our device
                        new_data = '[]' if self.data[changed_element] == "" else json.dumps(sorted([e["value"] for e in json.loads(self.data[changed_element])]))  # we get a list of dictionaries from tagify
                    elif bool(changed_element) and changed_element == "safety_signs":
                        old_data = "[]" if old_data == "" else old_data
                        new_data = "[]" if self.cleaned_data[changed_element] == "" else self.cleaned_data[changed_element]
                    else:  # default case: Just save whatever data is being handed over
                        old_data = old_data
                        new_data = self.cleaned_data[changed_element]

                    if similar_commits(old_data, new_data):
                        continue
                    if changed_element in ["name", "!delete", "description", "comment"]:
                        status = "not_handled"
                    else:
                        status = "auto_approved"

                    commit = Commit(experiment=model,
                                    field=changed_element,
                                    timestamp=django.utils.timezone.now(),
                                    old_data=old_data,
                                    new_data=new_data,
                                    author=user,
                                    confirmed_by=None,
                                    confirmed_at=None,
                                    status=status)
                    commit.save()
        experiment = super(ExperimentForm, self).save(commit=True)

        if not created and create_commits:
            experiment.name = self.initial["name"]
            experiment.description = self.initial["description"]
            experiment.comment = self.initial["comment"]
        experiment.save()

        return experiment

    def __str__(self):
        return "experiment"

    name = forms.CharField(
        label="Name",
        widget=forms.TextInput(
            attrs={'id': 'experiment_name',
                   'placeholder': 'Versuchsname...'}
        )
    )
    category = forms.ModelChoiceField(
        queryset=Category.objects.filter(depth=2),
        required=False,
        widget=forms.Select(
            attrs={'id': 'experiment_category',
                   'style': 'display: none'
                   }
        )
    )

    category_list = forms.ChoiceField(
        label="Kategorie",
        required=True,
        widget=forms.Select(
            attrs={'id': 'experiment_category_list',

                   }
        )
    )

    description = forms.CharField(
        label="Info",
        required=False,
        widget=forms.Textarea(

            attrs={'id': 'experiment_description',
                   'name': 'experiment[description]',
                   'cols': 0,
                   'rows': 5,
                   'placeholder': 'Versuchsinformationen...'}
        )
    )
    tags = forms.CharField(
        label="Schlagworte",
        required=False,
        widget=forms.TextInput(
            attrs={'name': 'tag',
                   'placeholder': 'Schlagworte zur Suche...'
                   }
        ),
    )
    preparation_time = forms.IntegerField(
        label="Vorbereitungsdauer (in Minuten)",
        required=False,
        widget=forms.NumberInput(
            attrs={'id': 'experiment_preparation_time',
                   'placeholder': 'Vorbereitungsdauer...'}
        ),
        initial=1440
    )
    execution_time = forms.IntegerField(
        label="Durchführungsdauer (in Minuten)",
        required=False,
        widget=forms.NumberInput(
            attrs={'id': 'experiment_execution_time',
                   'placeholder': 'Durchführungsdauer...'}
        )
    )
    status = forms.ChoiceField(
        choices=Experiment.AVAILABILTY_CHOICES,
        label="Status",
        required=False,
        widget=forms.Select(
            attrs={'id': 'experiment_status'}
        )
    )

    labels = forms.CharField(
        label="Label",
        required=False,
        widget=forms.Textarea(
            attrs={
                'id': 'experiment_labels',
                "name": 'labels',
                'class': "label_select",
                "placeholder": "Label auswählen..."

            }
        ),
    )
    comment = forms.CharField(
        label="Beschreibung",
        required=False,
        widget=forms.Textarea(
            attrs={'id': 'experiment_comment',
                   'name': 'experiment[comment]',
                   'cols': 0,
                   'rows': 5,
                   'placeholder': 'Versuchsbeschreibung...'}
        )
    )

    safety_signs = forms.CharField(
        label="Sicherheitszeichen",
        required=False,
        widget=forms.TextInput(
            attrs={'id': 'experiment_safety_signs'}
        )
    )

    devices = forms.CharField(
        label="Geräte",
        required=False,
        widget=forms.Textarea(
            attrs={
                'id': 'experiment_devices',
                "name": 'device',
                'class': "device_select",
                "placeholder": "Geräte auswählen..."

            }
        )
    )

    experiment_number = forms.IntegerField(
        label="Versuchsnummer",
        required=True,
        widget=forms.NumberInput(
            attrs={
                'id': 'experiment_experiment_number',
                'placeholder': 'Versuchsnummer...'}
        )
    )


class DocumentFormExp(forms.ModelForm):
    class Meta:
        model = Document
        name = "document"
        fields = ('id',
                  'image',
                  'image_name',
                  'comment',
                  'private',
                  'position',
                  'marked'
                  )

    def __init__(self, *args, **kwargs):
        # to delete colon after field's name
        kwargs.setdefault('label_suffix', '')
        super(DocumentFormExp, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        # override this for commit
        return super(DocumentFormExp, self).save()

    private = forms.ChoiceField(
        choices=Document.SECURITY_CHOICES,
        required=False,
        widget=forms.Select(
            attrs={
                'class': 'security_select',
            }
        )
    )

    image = forms.FileField(
        required=False,
        widget=forms.FileInput(
            attrs={
                # 'type': 'hidden',
                'class': 'image_upload',
                'style': 'display: none'
            }
        )
    )

    image_name = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                'type': 'hidden',
            }
        )

    )
    image_size = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                'type': 'hidden',
            }
        )

    )
    marked = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'class': 'check preview_image',
                'name': "document_set-__name__-marked",
                'onchange': 'cbChange(this);'
            }
        )

    )
    position = forms.IntegerField(
        required=True,
        widget=forms.NumberInput(
            attrs={
                'id': "id_document_set-__name__-position",
                'value': '__name__',
                'class': "imageposition",
                'name': "document_set-__name__-position"
            }
        )
    )
    comment = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={
                'id': 'document_set-__name__-comment',
                'name': 'document_set-__name__-comment',
                'placeholder': 'Kommentar...'
            }
        )
    )


DocumentFormSetExp = inlineformset_factory(Experiment,
                                        Document,
                                        form=DocumentFormExp,
                                        extra=0,
                                        max_num=50,
                                        absolute_max=50,
                                        can_delete=True)
