from django.apps import AppConfig


class ExperimentListConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'experiment_list'
