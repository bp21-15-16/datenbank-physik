import json
from django.contrib import messages
from django.utils.html import escape


def get_whole_token(object):
    token = object.token
    while object.parent is not None:
        object = object.parent
        token = object.token + "." + token
    return token


def getCategories(query, depth):
    category_query = list(query.select_related('parent', 'parent__parent'))
    category_query = sorted(category_query,
                            key=lambda x: (
                                ((1, int(x.parent.parent.token)) if x.parent.parent.token.isdigit() else (2, str(x.parent.parent.token))) if x.depth == 2
                                    else ((1, int(x.parent.token)) if x.parent.token.isdigit() else (2, str(x.parent.token))) if x.depth == 1
                                    else ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))),
                                ((1, int(x.parent.token)) if x.parent.token.isdigit() else (2, str(x.parent.token))) if x.depth == 2
                                    else ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))) if x.depth == 1
                                    else (0, '\x00'),
                                ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))) if x.depth == 2
                                    else (0, '\x00'),
                            ))
    category_choices = []
    if depth == 0:
        category_choices.append(['', "Dies ist eine Hauptkategorie"])
    for l in category_query:
        token = get_whole_token(l)
        category_choices.append([l.id, f'{token} | {l.name}'])
    return category_choices


def similar_commits(old_data, new_data):
    if old_data == new_data or str(old_data) == str(new_data)  :
        return True
    elif isinstance(old_data, list):
        return set(old_data) == set(new_data)
    else:
        return False


def show_error_msg(request, form, experiment_documents):
    if experiment_documents.total_error_count() != 0:
        messages.error(request, f"Es gibt einen Fehler beim Hinzufügen der Dokumente {experiment_documents.errors}")
    if not form.is_valid():
        messages.error(request, f"Es gibt einen Fehler beim Erstellen des Versuches {form.errors}")


def get_experiment_labels(object):
    labels = sorted(list(object.labels.all().values('id', 'name')), key=lambda k: k['name'])
    for d in labels:
        d['value'] = d.pop('id')
        d["name"] = d.pop("name")
        d["clean_name"] = escape(d["name"])
    return json.dumps(labels)


def get_experiment_devices(object):
    devices = sorted(list(object.devices.all().values('id', 'name')), key=lambda k: k['name'])
    for d in devices:
        d['value'] = d.pop('id')
        d["name"] = d.pop("name")
        d["clean_name"] = escape(d["name"])
    return json.dumps(devices)
