import ast
import os
import random
import time

import django.utils.timezone
from django import forms as django_forms
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView, DeleteView

import global_utilities
from accounts.decorators import *
from commits import utilities as commits_utilities
from datenbank_physik import settings
from labels import utilities as label_utilities
from labels.utilities import *
from public.models import Category, Experiment, Comment, Document, Label, Commit, Device
from . import forms
from .forms import ExperimentForm, DocumentFormSetExp
from .utilities import *


# also see devices\views.py for basically the entire file


def custom_redirect(request, experiment_id):
    return redirect(f"/experiment/{experiment_id}/description/")


def experiments_overview(request, _=None):
    start_timestamp = time.time()

    if request.user.is_authenticated and request.user.hasGroup("Lesen - Versuche - Versteckt"):
        experiment_list = list(Experiment.objects
                               .select_related('category', 'category__parent', 'category__parent__parent')
                               .filter(category_id__isnull=False)
                               .order_by('experiment_number', 'name'))
        experiment_list_without_category = Experiment.objects.filter(category_id__isnull=True)
    else:
        experiment_list = list(Experiment
                               .objects.select_related('category', 'category__parent', 'category__parent__parent')
                               .filter(category_id__isnull=False, status='available')
                               .order_by('experiment_number', 'name'))
        experiment_list_without_category = Experiment.objects\
            .filter(category_id__isnull=True)\
            .filter(status='available')

    for experiment in experiment_list:
        experiment.parent = experiment.category
        experiment.token = experiment.experiment_number
        experiment.depth = 3

    category_list = list(Category.objects.select_related('parent', 'parent__parent'))
    pending_categories = [x.category for x in
                          Commit.objects
                                .select_related('category')
                                .filter(field="!delete", category__isnull=False, status='not_handled')]

    original_object_list = category_list + experiment_list

    # Sort the entire list of experiments and their categories so that they end up in their default sorting. Experiments are already presorted.
    original_object_list = sorted(original_object_list,
                                  key=lambda x: (
                                      # primary by cpp
                                      ((1, int(x.parent.parent.parent.token)) if x.parent.parent.parent.token.isdigit() else (2, str(x.parent.parent.parent.token))) if x.depth == 3  # experiment
                                          else ((1, int(x.parent.parent.token)) if x.parent.parent.token.isdigit() else (2, str(x.parent.parent.token))) if x.depth == 2  # category
                                          else ((1, int(x.parent.token)) if x.parent.token.isdigit() else (2, str(x.parent.token))) if x.depth == 1  # cp
                                          else ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))),  # cpp
                                      str(x.parent.parent.parent.name) if x.depth == 3 else str(x.parent.parent.name) if x.depth == 2 else str(x.parent.name) if x.depth == 1 else str(x.name),
                                      # secondary by cp
                                      ((1, int(x.parent.parent.token)) if x.parent.parent.token.isdigit() else (2, str(x.parent.parent.token))) if x.depth == 3
                                          else ((1, int(x.parent.token)) if x.parent.token.isdigit() else (2, str(x.parent.token))) if x.depth == 2
                                          else ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))) if x.depth == 1
                                          else (0, '\x00'),
                                      str(x.parent.parent.name) if x.depth == 3 else str(x.parent.name) if x.depth == 2 else str(x.name) if x.depth == 1 else '\x00',
                                      # tertiary by category
                                      ((1, int(x.parent.token)) if x.parent.token.isdigit() else (2, str(x.parent.token))) if x.depth == 3
                                          else ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))) if x.depth == 2
                                          else (0, '\x00'),
                                      str(x.parent.name) if x.depth == 3 else str(x.name) if x.depth == 2 else '\x00',
                                      # lastly, just so experiments are behind their respective categories
                                      1 if x.depth == 3 else 0,))

    # even if the user is not allowed to see the image in the fullpage, if its marked we used it as a preview image regardless
    all_bg_images = {}
    for image in Document.objects.filter(experiment_id__isnull=False, marked=1):
        if os.path.exists(settings.MEDIA_ROOT + f'lowres/{image.image_name}'):
            all_bg_images[image.experiment_id] = f'lowres/{image.image_name}'
        else:
            all_bg_images[image.experiment_id] = image.image_name

    objects = []
    i = 0
    amount_of_objects = 0
    for object in original_object_list:
        if object.parent is None:
            objects.append({"level": "cpp",
                            "token": f'{object.token}',
                            "name": object.name.strip(),
                            "id": object.id,
                            "default_sorting_id": i,
                            "pending_delete": object in pending_categories,
                            })
        elif object.parent.parent is None:
            objects.append({"level": "cp",
                            "token": f'{object.parent.token}.{object.token}',
                            "name": object.name.strip(),
                            "id": object.id,
                            "default_sorting_id": i,
                            "belongs_to_cpp": object.parent.id,
                            "pending_delete": object in pending_categories,
                            })
        elif object.parent.parent.parent is None:
            objects.append({"level": "category",
                            "token": f'{object.parent.parent.token}.{object.parent.token}.{object.token}',
                            "name": object.name.strip(),
                            "id": object.id,
                            "default_sorting_id": i,
                            "belongs_to_cp": object.parent.id,
                            "belongs_to_cpp": object.parent.parent.id,
                            "pending_delete": object in pending_categories,
                            })
        else:
            try:
                bg_image = all_bg_images[object.id]
            except:
                bg_image = None

            objects.append({"level": "object",
                            "token": f'{object.parent.parent.parent.token}.{object.parent.parent.token}.{object.parent.token}.{object.experiment_number}',
                            "name": object.name,
                            "object_id": object.id,
                            "status": object.status,
                            "searchable_object_data": {"experiment_name": object.name,
                                                       "category": f"""{object.parent.parent.parent.token}{object.parent.parent.token}{object.parent.token}
                                                                       {object.parent.parent.parent.name}{object.parent.parent.name}{object.parent.name}""",
                                                       "experiment_id": object.id,
                                                       "description": object.description if object.description is not None else "",
                                                       "comment": object.comment if object.comment is not None else "",
                                                       "random": random.random(),
                                                       "tags": object.tags if object.tags is not None else ""},
                            "default_sorting_id": i,
                            "background_image": bg_image,
                            "preview_text": object.description if object.description is not None else "",
                            "belongs_to_category": object.category.id,
                            "belongs_to_cp": object.category.parent.id,
                            "belongs_to_cpp": object.category.parent.parent.id})
            amount_of_objects += 1
        i += 1

    pageButtons = []
    if request.user.is_authenticated and request.user.hasGroup("Schreiben - Versuche"):
        pageButtons.append(["Versuch hinzufügen", 'add_experiment', 'icons/ic_add_circle_outline_24px.svg'])
    if request.user.is_authenticated and request.user.hasGroup("Schreiben - Versuche - Kategorien"):
        pageButtons.append(["Kategorie hinzufügen", 'category_create', 'icons/ic_playlist_add_24px.svg'])

    main_title = 'Versuche'
    context = {'path': 'experiment',
               'search_checklist': {"experiment_name": "Versuchsname",
                                    "category": "Kategoriename und Kürzel",
                                    "experiment_id": "Versuchs ID",
                                    "description": "Info",
                                    "comment": "Beschreibung",
                                    "tags": "Schlagworte", },
               'sort_by_values': {"experiment_name?str?asc": "Nach Titel aufsteigend",
                                  "experiment_name?str?dsc": "Nach Titel absteigend",
                                  "experiment_id?num?asc": "Nach ID aufsteigend",
                                  "experiment_id?num?dsc": "Nach ID absteigend",
                                  "random?str?asc": "Zufällig", },
               'objects': objects,
               'objects_without_category': experiment_list_without_category,
               'amount_of_objects': amount_of_objects,
               'page_buttons': pageButtons,
               'main_title': main_title,
               'items': None,
               'can_edit_category': request.user.is_authenticated and request.user.hasGroup('Schreiben - Versuche - Kategorien'),
               'can_edit_object': request.user.is_authenticated and request.user.hasGroup('Schreiben - Versuche'),
               'start_timestamp': start_timestamp,
               'view_timestamp': time.time()
               }

    return render(request, 'list_with_categories/list_with_categories.html', context)


def get_experiment_query(request):
    if request.user.is_authenticated and request.user.hasGroup("Lesen - Versuche - Versteckt"):
        list_to_sort = Experiment.objects \
            .select_related('category', 'category__parent', 'category__parent__parent') \
            .filter(category_id__isnull=False) \
            .order_by('experiment_number', 'name')
    else:
        list_to_sort = Experiment.objects \
            .select_related('category', 'category__parent', 'category__parent__parent') \
            .filter(category_id__isnull=False, status='available') \
            .order_by('experiment_number', 'name')

    experiment_query = sorted(list_to_sort,
                              key=lambda x: (
                                  ((1, int(x.category.parent.parent.token)) if x.category.parent.parent.token.isdigit() else (2, str(x.category.parent.parent.token))),
                                  str(x.category.parent.parent.name),
                                  ((1, int(x.category.parent.token)) if x.category.parent.token.isdigit() else (2, str(x.category.parent.token))),
                                  str(x.category.parent.name),
                                  ((1, int(x.category.token)) if x.category.token.isdigit() else (2, str(x.category.token))),
                                  str(x.category.name),))
    return experiment_query


def get_previous_experiment(request, experiment_id):
    experiment_query = get_experiment_query(request)
    index = -1
    for experiment in experiment_query:
        index = index + 1
        if experiment.id == experiment_id:
            break
    if index == 0:
        previous_experiment = experiment_query[-1]
    else:
        previous_experiment = experiment_query[index - 1]
    return redirect(f'/experiment/{previous_experiment.id}/')


def get_next_experiment(request, experiment_id):
    experiment_query = get_experiment_query(request)
    index = -1
    for experiment in experiment_query:
        index = index + 1
        if experiment.id == experiment_id:
            break
    if experiment_query[index].id == experiment_query[-1].id:
        next_experiment = experiment_query[0]
    else:
        next_experiment = experiment_query[index + 1]
    return redirect(f'/experiment/{next_experiment.id}/')


def get_description(experiment):
    comment = experiment.comment
    description = experiment.description
    preparation_time = experiment.preparation_time
    execution_time = experiment.execution_time
    tags = []
    try:
        tags_db = experiment.tags
        for tag in json.loads(tags_db):
            tags.append(tag['value'])
    except (TypeError, json.JSONDecodeError):
        tags = []
    try:
        safety_signs = json.loads(experiment.safety_signs)
    except (TypeError, json.JSONDecodeError):
        safety_signs = []

    description = {'description': description,
                   'comment': comment,
                   'preparation_time': f"{round(preparation_time / 60 / 24, 1)} Tage" if preparation_time is not None else "1 Tag",
                   'execution_time': f"{execution_time} Minuten" if execution_time is not None else None,
                   'safety_signs': safety_signs,
                   'tags': tags,
                   }

    return description


def get_devices(experiment):
    # all labels for this experiment
    labels = []
    for label in experiment.labels.all():
        labels.append({'name': label.name,
                       'id': label.id,
                       'color': label.get_color(),
                       'devices': label.device_set, }) # and their associated devices

    # all devices without a label
    devices_without_labels = experiment.devices.all()
    devices = []
    if devices_without_labels:
        for device in devices_without_labels:
            try:
                image_name = device.document_set.get(marked=1).image_name
            except:
                image_name = None
            if os.path.exists(settings.MEDIA_ROOT + f'lowres/{image_name}'):
                image_name = f'lowres/{image_name}'
            else:
                image_name = image_name
            devices.append({'device': device,
                            'image': image_name
            })

    devices = {"labels": labels, 'devices': devices}
    return devices


def get_files(request, experiment_id):
    if request.user.is_authenticated:
        if request.user.hasGroup("Sicherheitsstufe - VLA"):
            document_query = Document.objects\
                .filter(experiment_id=experiment_id)\
                .order_by('position', 'image_name')
        elif request.user.hasGroup("Sicherheitsstufe - TU Darmstadt"):
            document_query = Document.objects\
                .filter(experiment_id=experiment_id, private__lte=1)\
                .order_by('position', 'image_name')
        else:
            document_query = Document.objects\
                .filter(experiment_id=experiment_id, private=0)\
                .order_by('position', 'image_name')
    else:
        document_query = Document.objects\
            .filter(experiment_id=experiment_id, private=0)\
            .order_by('position', 'image_name')

    files = []
    for file in document_query:
        if os.path.exists(settings.MEDIA_ROOT + f'lowres/{file.image_name}'):
            url_lowres = f'lowres/{file.image_name}'
        else:
            url_lowres = file.image_name
        files.append({"url": file.image_name,
                      "url_lowres": url_lowres,
                      "type": file.typ,
                      "general_type": file.typ.split("/")[0],
                      "specific_type": file.typ.split("/")[1],
                      "comment": file.comment if file.comment else "",
                      "spec_type": file.image_name.split(".")[-1],
                      "name": file.name})

    return files


def filter_comments(experiment_id, token):
    current_experiment = Experiment.objects.get(id=experiment_id)
    comments = current_experiment.experiment_comment.filter(type=token)
    return comments


def get_comments(experiment, user):
    form = None
    comments_public = None
    comments_demo = None
    comments_vla = None
    comment_choices = []
    can_comment = None
    write_types = []

    if user.hasGroup('Schreiben - Versuche - Tab öffentliche Kommentare'):
        write_types.append('PUBLIC')
    if user.hasGroup('Schreiben - Versuche - Tab Interna VLA'):
        write_types.append('VLA')
    if user.hasGroup('Schreiben - Versuche - Tab Interna Demo'):
        write_types.append('DEMO')

    if user.hasGroup('Lesen - Versuche - Tab öffentliche Kommentare'):
        comment_choices.append(('PUBLIC', "Für eingeloggte Nutzer sichtbar"))
        comments_public = filter_comments(experiment.id, 'PUBLIC')
    if user.hasGroup('Lesen - Versuche - Tab Interna Demo'):
        comment_choices.append(('DEMO', "Interna Demo"))
        comments_demo = filter_comments(experiment.id, 'DEMO')
    if user.hasGroup('Lesen - Versuche - Tab Interna VLA'):
        comment_choices.append(('VLA', "Interna VLA"))
        comments_vla = filter_comments(experiment.id, 'VLA')

    if comment_choices:
        form = forms.commentForm()
        form.fields['type'].choices = comment_choices

    return {
        'comments_public': comments_public,
        'comments_demo': comments_demo,
        'comments_vla': comments_vla,
        'form': form,
        'item': experiment,
        'write_types': write_types,
    }


def get_commits(experiment):
    commit_query = Commit.objects\
        .select_related('author', 'confirmed_by', 'experiment', 'device')\
        .exclude(field='!migration')\
        .filter(experiment=experiment.id)\
        .order_by('-timestamp')

    return commits_utilities.get_list_of_commits(commit_query)


def get_base_info(experiment):
    try:
        category = experiment.category

        base_info = {'title': experiment.name,
                     'id': experiment.id,
                     'token0': category.parent.parent.token,
                     'token1': category.parent.token,
                     'token2': category.token,
                     'number': experiment.experiment_number,
                     'cpp_name': category.parent.parent.name,
                     'cp_name': category.parent.name,
                     'c_name': category.name,
                     'cpp_id': category.parent.parent_id,
                     'cp_id': category.parent_id,
                     'c_id': category.id,
                     'status': experiment.status, }
    except:
        base_info = {'title': experiment.name,
                     'id': experiment.id,
                     'number': experiment.experiment_number,
                     'status': experiment.status, }
    return base_info


def experiment_fullpage(request, experiment_id, _=None):
    t1 = time.time()

    try:
        experiment = Experiment.objects.get(id=experiment_id)
    except ObjectDoesNotExist as e:
        raise Http404

    if experiment.status != 'available':  # block viewing fullpage when user has no right to see not available experiments
        if request.user.is_authenticated and request.user.hasGroup("Lesen - Versuche - Versteckt"):
            pass
        else:
            return redirect('experiments')

    files = get_files(request, experiment)

    description_content = get_description(experiment)
    description_content['amount_to_display'] = global_utilities.amount_of_media_to_display(files)
    description_content['files'] = files

    tabs = {}
    tabs['description'] = {'content': description_content,
                           'displayed_name': 'Info',
                           'url_name': 'description',
                           'template': 'fullpage/experiment_description.html', }
    if request.user.is_authenticated \
            and (request.user.hasGroup("Lesen - Versuche - Tab öffentliche Kommentare")
                 or request.user.hasGroup("Lesen - Versuche - Tab Interna VLA")
                 or request.user.hasGroup("Lesen - Versuche - Tab Interna Demo")):
        tabs['comments'] = {'content': get_comments(experiment, request.user),
                            'displayed_name': 'Kommentare',
                            'url_name': 'comments',
                            'template': 'fullpage/comments.html', }
    if request.user.is_authenticated and request.user.hasGroup("Lesen - Versuche - Tab Geräte"):
        tabs['devices'] = {'content': get_devices(experiment),
                           'displayed_name': 'Geräte',
                           'url_name': 'devices',
                           'template': 'fullpage/devices.html', }
    if request.user.is_authenticated and request.user.hasGroup("Lesen - Versuche - Tab Dateien"):
        tabs['files'] = {'content': files,
                         'displayed_name': 'Dateien',
                         'url_name': 'documents',
                         'template': 'fullpage/files.html', }
    if request.user.is_authenticated and request.user.hasGroup("Lesen - Versuche - Tab Änderungen"):
        tabs['commits'] = {'content': get_commits(experiment),
                           'displayed_name': 'Änderungen',
                           'url_name': 'commits',
                           'template': 'fullpage/commits.html', }

    context = {'main_title': 'Versuch',
               'base_info': get_base_info(experiment),
               'path': 'experiment',
               'tabs': tabs,
               'start_timestamp': t1,
               'view_timestamp': time.time(),
               }

    return render(request, 'fullpage/fullpage.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Versuche - Tab öffentliche Kommentare',
                              'Schreiben - Versuche - Tab Interna Demo',
                              'Schreiben - Versuche - Tab Interna VLA'])
def comment_add(request):
    if request.method == 'POST':
        id = request.POST.get('experiment')
        parent = request.POST.get('parent', 0)
        message = request.POST.get('message')
        type = request.POST.get('type')
        user = request.user
        current_experiment = Experiment.objects.get(id=id)

        parent_comment = None if parent == 0 else Comment.objects.get(id=parent)
        comment = Comment(message=message, user=user, experiment=current_experiment, parent_comment=parent_comment,
                          type=type)
        comment.save()

        messages.add_message(request, messages.SUCCESS, f"Kommentar von {user.username} erstellt.",
                             extra_tags='comment')
        return redirect('experiment_comments', experiment_id=current_experiment.id)


@login_required(login_url='login')
def comment_delete(request, commentid):
    current_user = request.user
    current_experiment = None
    message = False
    try:
        c = Comment.objects.get(id=commentid)
        current_experiment = c.experiment
        type = c.type
        if c.user == current_user:
            c.delete()
        else:
            if type == 'PUBLIC':
                if current_user.hasGroup('Löschen - Versuche - Tab öffentliche Kommentare'):
                    c.delete()
                    message = True
            elif type == 'DEMO':
                if current_user.hasGroup('Löschen - Versuche - Tab Interna Demo'):
                    c.delete()
                    message = True
            elif type == 'VLA':
                if current_user.hasGroup('Löschen - Versuche - Tab Interna VLA'):
                    c.delete()
                    message = True
    except Comment.DoesNotExist:
        messages.add_message(request, messages.ERROR, "Dieser Kommentar existiert nicht.", extra_tags='comment')
        return redirect('experiments')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('experiments')

    if message == True:
        messages.add_message(request, messages.SUCCESS, f"Kommentar erfolgreich gelöscht.", extra_tags='comment')

    return redirect('experiment_comments', experiment_id=current_experiment.id)

@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Versuche - Kategorien'], url='experiments')
def category_create(request, *args, **kwargs):
    parent = kwargs.get('categoryid', None)
    main_title = "Kategorie erstellen"
    if parent:
        form = forms.CreateCategoryForm(initial={'parent': parent})
        parent_category = Category.objects.get(id=parent)
        main_title = f"Unterkategorie von {get_whole_token(parent_category)} | {parent_category} erstellen"
    else:
        form = forms.CreateCategoryForm()
    form.fields['parent'].choices = getCategories(Category.objects.filter(depth__in=[0, 1]), 0)
    if request.method == 'POST':
        form = forms.CreateCategoryForm(request.POST)
        form.fields['parent'].choices = getCategories(Category.objects.filter(depth__in=[0, 1]), 0)
        if form.is_valid():
            category = form.save(commit=False)
            if category.get_depth() < 3:
                parentid = form.data['parent']
                category.parent = None if parentid == "" else Category.objects.get(id=parentid)
                category.save()
                messages.success(request, f"Kategorie {category.name} erfolgreich erstellt.")
                return redirect(f"{reverse('experiments')}?category={category.id}")
            else:
                messages.error(request, "Es sind keine Kategorien tiefer als Ebene 3 erlaubt.")

    context = {
        'form': form,
        'main_title': main_title,
    }

    return render(request, 'forms/category.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Versuche - Kategorien'], url='experiments')
def category_edit(request, categoryid):
    EXPERIMENT_LIST = label_utilities.getExperiments(Experiment.objects)

    """Generates form for editing an existing category"""
    try:
        current_category = Category.objects.get(id=categoryid)
    except ObjectDoesNotExist as e:
        raise Http404

    category_experiments = label_utilities.getExperiments(current_category.experiment_set)
    category_experiments = json.dumps([dict(x, clean_name=escape(x["name"]), value=x["id"]) for x in category_experiments])

    current_name = current_category.name
    current_token = current_category.token
    whole_token = get_whole_token(current_category)
    current_parent = current_category.parent
    current_parent_depth = current_category.get_depth() - 1

    if request.method == 'POST':
        form = forms.EditCategoryForm(request.POST, instance=current_category)
        form.fields['parent'].choices = getCategories(Category.objects.filter(depth=current_parent_depth),
                                                     current_category.get_depth())
        if form.is_valid():
            category = form.save(commit=False)
            if category.get_depth() < 3:
                parentid = form.data['parent']
                category.parent = None if parentid == "" else Category.objects.get(id=parentid)
                category.save()

                if current_category.depth == 2:
                    experiments = form.data['experiment']
                    if experiments != '':
                        experiments = json.loads(experiments)
                        experiments = [int(e.get('value')) for e in experiments]
                        # Removes all experiments from category that have been deseleted
                        for e in category.experiment_set.all():
                            if not e.id in experiments:
                                Commit(experiment_id=e.id,
                                       author_id=request.user.id,
                                       timestamp=django.utils.timezone.now(),
                                       field='category',
                                       old_data=e.category,
                                       new_data=None,
                                       status='auto_approved',
                                       ).save()
                                e.category = None
                                e.save()

                        # Adds newly selected devices
                        old_experiments = [x.id for x in category.experiment_set.all()]
                        for e in experiments:
                            if not e in old_experiments:
                                current_experiment = Experiment.objects.get(id=e)
                                old_category = None if current_experiment.category is None else current_experiment.category.id
                                Commit(experiment_id=current_experiment.id,
                                       author_id=request.user.id,
                                       timestamp=django.utils.timezone.now(),
                                       field='category',
                                       old_data=old_category,
                                       new_data=category.id,
                                       status='auto_approved',
                                       ).save()
                                current_experiment.category = category
                                current_experiment.save()

                messages.success(request, f"Kategorie {category.name} erfolgreich geändert.")
                return redirect(f"{reverse('experiments')}?category={category.id}")
            else:
                messages.error(request, "Es sind keine Kategorien tiefer als Ebene 3 erlaubt.")
    else:
        initial_values = {
            'name': current_name,
            'token': current_token,
            'parent': "" if current_parent is None else current_parent.id,
            'experiment': category_experiments,
        }
        form = forms.EditCategoryForm(instance=current_category, initial=initial_values)
        if current_category.depth != 2:
            form.fields['experiment'].widget = django_forms.HiddenInput()

        form.fields['parent'].choices = getCategories(Category.objects.filter(depth=current_parent_depth),
                                                     current_category.get_depth())
        if (current_category.pending_delete()):
            messages.error(request, "Für diese Kategorie wurde bereits eine Anfrage zum Löschen erstellt.")

    context = {
        'form': form,
        'main_title': f"{whole_token} | {current_name} bearbeiten",
        'id': current_category.id,
        'delete_url': 'category_delete',
        'experiment_list': EXPERIMENT_LIST,
    }

    return render(request, 'forms/category.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Versuche - Kategorien'], url='experiments')
def category_delete(request, categoryid):
    """Deletes a category"""
    name = None
    try:
        category = Category.objects.get(id=categoryid)
        name = category.name
        commit = Commit(category=category,
                        field="!delete",
                        timestamp=django.utils.timezone.now(),
                        old_data=None,
                        new_data=None,
                        author=request.user,
                        confirmed_by=None,
                        confirmed_at=None,
                        status="not_handled")
        commit.save()
    except Category.DoesNotExist:
        messages.error(request, "Die angeforderte Kategorie existiert nicht.")
        return redirect('experiments')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('experiments')

    if name is not None:
        messages.success(request, f"Es wurde eine Anfrage zum Löschen der Kategorie {name} erstellt. (Siehe offene Änderungen)")

    return redirect(f"{reverse('experiments')}?category={categoryid}")


class ExperimentDocumentCreate(CreateView):
    model = Experiment
    # form_class to override layout of Form layout
    form_class = ExperimentForm
    template_name = "forms/add_experiment.html"

    def get_success_url(self):
        return reverse('experiment_view', args=(self.object.id,))

    @method_decorator(login_required(login_url="login"))
    @method_decorator(allowed_users(
        allowed_roles=
        ['Schreiben - Versuche',
         "Schreiben – Versuche – Dateien hinzufügen"],
        url='experiment')
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(ExperimentDocumentCreate, self).get_context_data(**kwargs)
        data["label_list"] = list(Label.objects.values('id', 'name', 'color__color'))
        data["device_list"] = list(Device.objects.values('id', 'name'))
        data["main_title"] = "Versuch hinzufügen"
        print("creating experiment")
        if self.request.POST:
            data['experimentdocuments'] = DocumentFormSetExp(self.request.POST,
                                                      self.request.FILES)

        else:
            data['experimentdocuments'] = DocumentFormSetExp()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        experiment_documents = context['experimentdocuments']
        labels = self.request.POST.get("labels")
        devices = self.request.POST.get("devices")
        show_error_msg(self.request, context["form"], experiment_documents)
        with transaction.atomic():
            self.object = form.save(user=self.request.user, create_commits=True)
            exp_id = self.object.id
            print(f"the experiment id is:{exp_id}")
            print(self.object.id)
            if experiment_documents.is_valid():
                experiment_documents.instance = self.object
                experiment_documents.save()
        if labels is not None and labels != "":
            labels_list = [int(x["value"]) for x in ast.literal_eval(labels)]
            self.object.labels.add(*labels_list)
        if devices is not None and devices != "":
            devices_list = [int(x["value"]) for x in ast.literal_eval(devices)]
            self.object.devices.add(*devices_list)
        messages.success(self.request, f"Versuch {self.object.name} erfolgreich erstellt.")
        return super(ExperimentDocumentCreate, self).form_valid(form)


class ExperimentDocumentUpdate(UpdateView):
    model = Experiment
    form_class = ExperimentForm
    template_name = "forms/add_experiment.html"

    def get_success_url(self):
        return reverse('experiment_view', args=(self.object.id,))

    @method_decorator(login_required(login_url="login"))
    @method_decorator(allowed_users(
        allowed_roles=
        ['Schreiben - Versuche',
         "Schreiben – Versuche – Dateien hinzufügen"],
        url='experiment')
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(ExperimentDocumentUpdate, self).get_context_data(**kwargs)
        data["pk"] = data["object"].id
        data["submit_button"] = "false"
        data["has_open_commits"] = bool(Commit.objects.filter(experiment=self.object, status="not_handled"))
        if data["has_open_commits"]:
            messages.error(self.request,
                           "Für diesen Versuch existieren noch offene Änderungen. "
                           "Solange die Änderungen offen sind, kann der Versuch nicht editiert werden.")
            data["submit_button"] = "true"
        data["label_list"] = list(Label.objects.values('id', 'name', 'color__color'))
        data["device_list"] = list(Device.objects.values('id', 'name'))
        data["user"] = self.request.user
        data["main_title"] = f"{self.object.id} | {self.object.name} bearbeiten"
        if self.request.POST:
            data['experimentdocuments'] = DocumentFormSetExp(self.request.POST,
                                                      self.request.FILES,
                                                      instance=self.object)
        else:
            data['experimentdocuments'] = DocumentFormSetExp(instance=self.object)
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        experimentdocuments = context['experimentdocuments']
        labels = self.request.POST.get("labels")
        devices = self.request.POST.get("devices")
        if context["has_open_commits"]:
            messages.error(self.request,
                           "Für diesen Versuch existieren noch offene Änderungen. "
                           "Solange die Änderungen offen sind, kann der Versuch nicht editiert werden.")
            return HttpResponseRedirect(self.get_success_url())

        show_error_msg(self.request, context["form"], experimentdocuments)
        with transaction.atomic():
            self.object = form.save(user=self.request.user, create_commits=True)
            if experimentdocuments.is_valid():
                try:
                    experimentdocuments.instance = self.object
                    experimentdocuments.save()
                except Exception as e:
                    messages.error(self.request,
                                   f"Es gibt einen Fehler bei der Änderung der Dokumente {e}")

        if labels is not None and labels != "":
            labels_list = [int(x["value"]) for x in ast.literal_eval(labels)]
            self.object.labels.clear()  # delete old labels
            self.object.labels.add(*labels_list)  # add all selected ones
        else:
            self.object.labels.clear()
        if devices is not None and devices != "":
            devices_list = [int(x["value"]) for x in ast.literal_eval(devices)]
            self.object.devices.clear()
            self.object.devices.add(*devices_list)
        else:
            self.object.devices.clear()
        messages.success(self.request, f"Versuch {self.object.name} erfolgreich geändert.")
        return super(ExperimentDocumentUpdate, self).form_valid(form)


class ExperimentDelete(DeleteView):
    model = Experiment
    success_url = "experiment"

    @method_decorator(login_required(login_url="login"))
    @method_decorator(allowed_users(
        allowed_roles=
        ['Schreiben - Versuche'],
        url='experiment')
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk):
        model = get_object_or_404(Experiment, pk=pk)
        try:
            commit = Commit(experiment=model,
                            field="!delete",
                            timestamp=django.utils.timezone.now(),
                            old_data="",
                            new_data="",
                            author=self.request.user,
                            confirmed_by=None,
                            confirmed_at=None,
                            status="not_handled")
            commit.save()
        except Exception as e:
            messages.error(self.request,
                           f"Es gibt einen Fehler beim Löschen des Versuchs : {e}")

        messages.success(self.request,
                         f"Es wurde eine Anfrage zum Löschen des Versuchs {model.name} erstellt. (Siehe offene Änderungen)")
        return redirect(reverse('experiment_view', args=(pk,)))