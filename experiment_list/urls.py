from django.urls import path, re_path
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.shortcuts import redirect

urlpatterns = [
    path('', views.experiments_overview, name="experiments"),

    path('add/', views.ExperimentDocumentCreate.as_view(), name='add_experiment'),
    path('<int:experiment_id>/', views.custom_redirect, name="experiment_view"),
    path('<int:pk>/edit/', views.ExperimentDocumentUpdate.as_view(), name='experiment-update'),
    path('<int:pk>/delete/', views.ExperimentDelete.as_view(), name='experiment-delete'),
    path('<int:experiment_id>/devices/', views.experiment_fullpage, name="experiment_devices"),
    path('<int:experiment_id>/comments/', views.experiment_fullpage, name="experiment_comments"),
    path('<int:experiment_id>/previous/', views.get_previous_experiment),
    path('<int:experiment_id>/next/', views.get_next_experiment),
    path('<int:experiment_id>/<str:_>/', views.experiment_fullpage),  # all fullpage sub links such as description, comments files, commits, ...

    path('comment/', views.comment_add, name='comment_add'),
    path('comment/<int:commentid>/delete/', views.comment_delete, name='comment_delete'),

    path('category/create/', views.category_create, name="category_create"),
    re_path(r'^category/create/(?P<categoryid>\d+)/$', views.category_create, name="location_create_sub"),
    path('category/<int:_>/', views.experiments_overview),
    path('category/<int:categoryid>/edit/', views.category_edit, name="category_edit"),
    path('category/<int:categoryid>/delete/', views.category_delete, name="category_delete"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
