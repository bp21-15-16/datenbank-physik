from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.devices_overview, name="devices"),

    path('add/', views.DeviceDocumentCreate.as_view(), name='device_add'),
    path('<int:device_id>/', views.custom_redirect),
    path('<int:pk>/edit/', views.DeviceDocumentUpdate.as_view(), name='device-update'),
    path('<int:pk>/delete/', views.DeviceDelete.as_view(), name='device-delete'),
    path('<int:device_id>/description/', views.device_fullpage, name="device_description"),
    path('<int:device_id>/inventory/', views.device_fullpage, name="device_view"),
    path('<int:device_id>/previous/', views.get_previous_device),
    path('<int:device_id>/next/', views.get_next_device),
    path('<int:device_id>/<str:_>/', views.device_fullpage, name="device_fullpage"),

    path('category/create/', views.location_create, name="location_create"),
    re_path(r'^category/create/(?P<categoryid>\d+)/$', views.location_create, name="location_create_sub"),
    path('category/<int:_>/', views.devices_overview),
    path('category/<int:locationid>/edit/', views.location_edit, name="location_edit"),
    path('category/<int:locationid>/delete/', views.location_delete, name="location_delete"),
]
