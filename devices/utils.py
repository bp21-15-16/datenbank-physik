import json
from django.contrib import messages
from django.utils.html import escape


def similar_commits(old_data, new_data):
    if old_data == new_data or str(old_data) == str(new_data)  :
        return True
    elif isinstance(old_data, list):
        return set(old_data) == set(new_data)
    else:
        return False


def show_error_msg(request, form, device_documents):
    if device_documents.total_error_count() != 0:
        messages.error(request, f"Es gibt einen Fehler beim Hinzufügen der Dokumente {device_documents.errors}")
    if not form.is_valid():
        messages.error(request, f"Es gibt einen Fehler beim Erstellen des Geräts {form.errors}")


def get_device_labels(object):
    labels = sorted(list(object.labels.all().values('id', 'name')), key=lambda k: k['name'])
    for d in labels:
        d['value'] = d.pop('id')
        d["name"] = d.pop("name")
        d["clean_name"] = escape(d["name"])
    return json.dumps(labels)

