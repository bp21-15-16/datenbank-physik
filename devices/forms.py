import json

import django.utils
from django import forms
from django.forms import ModelForm, inlineformset_factory

from public.models import Device, Commit, Label, Document, Location
from .utilities import get_locations
from .utils import get_device_labels, similar_commits


class CreateLocationForm(ModelForm):
    """Form for creating a new location"""
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Name des Standorts..."
            }
        ),
        label="Name",
    )

    token = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Kürzel des Standorts..."
            }
        ),
        label="Kürzel",
    )

    parent = forms.ChoiceField(
        choices=[],
        label="Überstandort",
        required=False,
    )

    class Meta:
        model = Location
        fields = ('name', 'token')

    def clean_name(self):
        """Checks if location with identical name and parent already exists, denies creation if so"""
        name = self.cleaned_data.get('name')
        parent = self.data.get('parent')
        instance = self.instance
        try:
            if parent != '':
                match = Location.objects.get(name=name, parent=parent)
            else:
                match = Location.objects.get(name=name, parent__isnull=True)
            if (instance is not None) and (match == instance):
                return name
        except Location.DoesNotExist:
            # Unable to find a location, this is fine
            return name

        # Location with this name and parent was found, raise an error
        raise forms.ValidationError("Dieser Standort mit diesem Überstandort existiert bereits.")


class EditLocationForm(ModelForm):
    """Form for editing an existing location"""
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Name des Standorts..."
            }
        ),
        label="Name",
    )

    token = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Kürzel des Standorts..."
            }
        ),
        label="Kürzel",
    )

    parent = forms.ChoiceField(
        widget=forms.Select(),
        choices=[],
        label="Überstandort",
        required=False,
    )

    device = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': "device_select",
                'id': "device_id",
                'placeholder': "Geräte auswählen..."
            }
        ),
        label="Geräte",
        required=False,
    )

    class Meta:
        model = Location
        fields = ('name', 'token',)

    def clean_name(self):
        """Checks if location with identical name and parent already exists, denies change if so"""
        name = self.cleaned_data.get('name')
        parent = self.data.get('parent')
        instance = self.instance
        try:
            if parent != '':
                match = Location.objects.get(name=name, parent=parent)
            else:
                match = Location.objects.get(name=name, parent__isnull=True)
            if (instance is not None) and (match == instance):
                return name
        except Location.DoesNotExist:
            # Unable to find a location, this is fine
            return name

        # Location with this name and parent was found, raise an error
        raise forms.ValidationError("Dieser Standort mit diesem Überstandort existiert bereits.")


class DeviceForm(forms.ModelForm):
    class Meta:
        model = Device
        name = "device"
        # fields = '__all__'
        fields = (
            'name', 'description', 'ordernumber', 'inventorynumber', 'ifpcode', 'comment', 'supplier', 'amount',
            'location', 'tags')

    def __init__(self, *args, **kwargs):
        # to delete colon after field's name
        # kwargs.setdefault('label_suffix', '')
        super(DeviceForm, self).__init__(*args, **kwargs)
        self.fields["location_list"].choices = get_locations(Location.objects.filter(depth=2), 2)
        if self.instance and self.instance.id is not None:
            self.fields["labels"].initial = get_device_labels(self.instance)

    def save(self, create_commits=False, user=None, commit=False):
        # if self.instance.id is None than the device is being created now
        # if creating a device confirmed_By is always None
        created = self.instance.id is None

        if create_commits:
            # TODO put the code below in another function
            # TODO to fill
            if created:
                device = super(DeviceForm, self).save(commit=True)
                commit = Commit(device=device,
                                field="!create",
                                timestamp=django.utils.timezone.now(),
                                old_data="",
                                new_data="",
                                author=user,
                                confirmed_by=None,
                                confirmed_at=None,
                                status="auto_approved")
                commit.save()
            else:
                model = Device.objects.get(id=self.instance.id)
                for changed_element in self.changed_data:
                    if changed_element == "location_list":
                        continue
                    attr = getattr(model, changed_element)
                    old_data = attr if attr else ""
                    if changed_element == "tags":  # we get a list of dictionaries from tagify for the tags, we only want a list
                        old_data = json.dumps(sorted([e["value"] for e in json.loads(old_data)])) if attr else '[]'
                        new_data = '[]' if self.data[changed_element] == "" else json.dumps(sorted([e["value"] for e in json.loads(self.data[changed_element])]))
                    elif changed_element == "location":  # here we want to save the id for later reference, not the name
                        old_data = old_data.id if old_data else ""
                        new_data = self.cleaned_data[changed_element].id
                    elif bool(changed_element) and changed_element == "labels":
                        old_data = json.dumps(sorted([str(e.id) for e in Label.objects.filter(device=model)])) if attr else '[]'  # here we need a list of all label ids for our device
                        new_data = '[]' if self.data[changed_element] == "" else json.dumps(sorted([e["value"] for e in json.loads(self.data[changed_element])]))  # we get a list of dictionaries from tagify
                    else:  # default case: Just save whatever data is being handed over
                        old_data = old_data
                        new_data = self.cleaned_data[changed_element]

                    if similar_commits(old_data, new_data):
                        continue
                    if changed_element in ["name", "!delete", "description", "comment"]:
                        status = "not_handled"
                    else:
                        status = "auto_approved"

                    commit = Commit(device=model,
                                    field=changed_element,
                                    timestamp=django.utils.timezone.now(),
                                    old_data=old_data,
                                    new_data=new_data,
                                    author=user,
                                    confirmed_by=None,
                                    confirmed_at=None,
                                    status=status)
                    commit.save()
        device = super(DeviceForm, self).save(commit=True)

        if not created and create_commits:
            device.name = self.initial["name"]
            device.description = self.initial["description"]
            device.comment = self.initial["comment"]
        device.save()

        return device

    def __str__(self):
        return "device"

    name = forms.CharField(
        label="Name",
        widget=forms.TextInput(
            attrs={'id': 'device_name',
                   'placeholder': 'Gerätename...'}
        )
    )
    location = forms.ModelChoiceField(
        queryset=Location.objects.filter(depth=2),
        required=False,
        widget=forms.Select(
            attrs={'id': 'device_location',
                   'style': 'display: none'
                   }
        )
    )

    location_list = forms.ChoiceField(
        label="Standort",
        required=True,
        widget=forms.Select(
            attrs={'id': 'device_location_list',

                   }
        )
    )

    description = forms.CharField(
        label="Info",
        required=False,
        widget=forms.Textarea(

            attrs={'id': 'device_description',
                   'name': 'device[description]',
                   'cols': 0,
                   'rows': 5,
                   'placeholder': 'Geräteinformation...'
                   }
        )
    )
    tags = forms.CharField(
        label="Schlagworte",
        required=False,
        widget=forms.TextInput(
            attrs={'name': 'tag',
                   'placeholder': 'Schlagworte zur Suche...'
                   }
        ),
    )
    ordernumber = forms.CharField(
        label="Bestellnummer",
        required=False,
        widget=forms.TextInput(
            attrs={'id': 'device_ordernumber',
                   'placeholder': 'Bestellnummer...'}
        )
    )
    inventorynumber = forms.IntegerField(
        label="Inventarnummer",
        required=False,
        widget=forms.NumberInput(
            attrs={'id': 'device_inventorynumber',
                   'placeholder': 'Inventarnummer...'}
        )
    )
    ifpcode = forms.CharField(
        label="IFP-Code",
        required=False,
        widget=forms.TextInput(
            attrs={'id': 'device_ifpcode',
                   'placeholder': 'IFP-Code...'}
        )
    )

    labels = forms.CharField(
        label="Label",
        required=False,
        widget=forms.Textarea(
            attrs={
                'id': 'device_labels',
                "name": 'labels',
                'class': "label_select",
                "placeholder": "Label auswählen..."

            }
        ),
    )
    comment = forms.CharField(
        label="Beschreibung",
        required=False,
        widget=forms.Textarea(
            attrs={'id': 'device_comment',
                   'name': 'device[comment]',
                   'cols': 0,
                   'rows': 5,
                   'placeholder': 'Gerätebeschreiben...'
                   }
        )
    )

    supplier = forms.CharField(
        label="Hersteller",
        required=False,
        widget=forms.TextInput(
            attrs={'id': 'device_supplier',
                   'placeholder': 'Hersteller...'}
        )
    )
    amount = forms.IntegerField(
        label="Anzahl insgesamt",
        required=True,
        widget=forms.NumberInput(
            attrs={
                'id': 'device_amount',
                'placeholder': 'Anzahl...',

            }
        ),
        initial=1
    )


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        name = "document"
        fields = ('id',
                  'image',
                  'image_name',
                  'comment',
                  'private',
                  'position',
                  'marked'
                  )

    def __init__(self, *args, **kwargs):
        # to delete colon after field's name
        kwargs.setdefault('label_suffix', '')
        super(DocumentForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        # override this for commit
        return super(DocumentForm, self).save()

    private = forms.ChoiceField(
        choices=Document.SECURITY_CHOICES,
        required=False,
        widget=forms.Select(
            attrs={
                'class': 'security_select',
            }
        )
    )

    image = forms.FileField(
        required=False,
        widget=forms.FileInput(
            attrs={
                # 'type': 'hidden',
                'class': 'image_upload',
                'style': 'display: none'
            }
        )
    )

    image_name = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                'type': 'hidden',
            }
        )

    )
    image_size = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                'type': 'hidden',
            }
        )

    )
    marked = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'class': 'check preview_image',
                'name': "document_set-__name__-marked",
                'onchange': 'cbChange(this);'
            }
        )

    )
    position = forms.IntegerField(
        required=True,
        widget=forms.NumberInput(
            attrs={
                'id': "id_document_set-__name__-position",
                'value': '__name__',
                'class': "imageposition",
                'name': "document_set-__name__-position"
            }
        )
    )
    comment = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={
                'id': 'document_set-__name__-comment',
                'name': 'document_set-__name__-comment',
                'placeholder': 'Kommentar...'
            }
        )
    )


DocumentFormSet = inlineformset_factory(Device,
                                        Document,
                                        form=DocumentForm,
                                        extra=0,
                                        max_num=50,
                                        absolute_max=50,
                                        can_delete=True)
