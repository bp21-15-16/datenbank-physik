

def get_whole_token(object):
    """Returns string of location token concatenated with its parents' tokens, separated by ."""
    token = object.token
    while object.parent is not None:
        object = object.parent
        token = str(object.token) + "." + str(token)
    return token


def get_locations(query, depth):
    """Returns valid parent locations"""
    location_query = list(query.select_related('parent', 'parent__parent'))
    location_query = sorted(location_query,
                            key=lambda x: (
                                ((1, int(x.parent.parent.token)) if x.parent.parent.token.isdigit() else (2, str(x.parent.parent.token))) if x.depth == 2
                                    else ((1, int(x.parent.token)) if x.parent.token.isdigit() else (2, str(x.parent.token))) if x.depth == 1
                                    else ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))),
                                ((1, int(x.parent.token)) if x.parent.token.isdigit() else (2, str(x.parent.token))) if x.depth == 2
                                    else ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))) if x.depth == 1
                                    else (0, '\x00'),
                                ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))) if x.depth == 2
                                    else (0, '\x00'),
                            ))
    location_choices = []
    if depth == 0:
        location_choices.append(["", "Dies ist ein Hauptstandort"])
    for l in location_query:
        token = get_whole_token(l)
        location_choices.append([l.id, f'{token} | {l.name}'])

    return location_choices
