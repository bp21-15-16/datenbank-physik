import ast
import json
import os
import time

import django.utils.timezone
from django import forms as django_forms
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils.html import escape
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import CreateView, UpdateView, DeleteView

import global_utilities
from inventory.utilities import get_activeInventory
import inventory.views
from accounts.decorators import *
from commits import utilities as commits_utilities
from datenbank_physik import settings
from inventory.models import InventoryItem, LoanItem
from labels import utilities as label_utilities
from public.models import Commit, Device, Document, Label, Location
from . import forms
from .forms import DeviceForm, DocumentFormSet
from .utilities import *
from .utils import show_error_msg


class DeviceDocumentCreate(CreateView):
    model = Device
    # form_class to override layout of Form layout
    form_class = DeviceForm
    template_name = "forms/device.html"

    def get_success_url(self):
        return reverse('device_description', args=(self.object.id,))

    @method_decorator(login_required(login_url="login"))
    @method_decorator(allowed_users(
        allowed_roles=
        ['Schreiben - Geräte',
         "Schreiben – Geräte – Dateien hinzufügen"],
        url='devices')
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(DeviceDocumentCreate, self).get_context_data(**kwargs)
        data["label_list"] = list(Label.objects.values('id', 'name', 'color__color'))
        data["main_title"] = "Gerät hinzufügen"
        print("creating device")
        if self.request.POST:
            data['devicedocuments'] = DocumentFormSet(self.request.POST,
                                                      self.request.FILES)

        else:
            data['devicedocuments'] = DocumentFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        device_documents = context['devicedocuments']
        labels = self.request.POST.get("labels")
        show_error_msg(self.request, context["form"], device_documents)
        with transaction.atomic():
            self.object = form.save(user=self.request.user, create_commits=True)
            dev_id = self.object.id
            print(f"the device id is:{dev_id}")
            print(self.object.id)
            if device_documents.is_valid():
                device_documents.instance = self.object
                device_documents.save()
        if labels is not None and labels != "":
            labels_list = [int(x["value"]) for x in ast.literal_eval(labels)]
            self.object.labels.add(*labels_list)
        messages.success(self.request, f"Gerät {self.object.name} erfolgreich erstellt.")
        return super(DeviceDocumentCreate, self).form_valid(form)


class DeviceDocumentUpdate(UpdateView):
    model = Device
    form_class = DeviceForm
    template_name = "forms/device.html"

    def get_success_url(self):
        return reverse('device_description', args=(self.object.id,))

    @method_decorator(login_required(login_url="login"))
    @method_decorator(allowed_users(
        allowed_roles=
        ['Schreiben - Geräte',
         "Schreiben – Geräte – Dateien hinzufügen"],
        url='devices')
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(DeviceDocumentUpdate, self).get_context_data(**kwargs)
        data["pk"] = data["object"].id
        data["submit_button"] = "false"
        data["has_open_commits"] = bool(Commit.objects.filter(device=self.object, status="not_handled"))
        if data["has_open_commits"]:
            messages.error(self.request,
                           "Für dieses Gerät existieren noch offene Änderungen. "
                           "Solange die Änderungen offen sind, kann das Gerät nicht editiert werden.")
            data["submit_button"] = "true"
        data["label_list"] = list(Label.objects.values('id', 'name', 'color__color'))
        data["user"] = self.request.user
        data["main_title"] = f"{self.object.id} | {self.object.name} bearbeiten"
        if self.request.POST:
            data['devicedocuments'] = DocumentFormSet(self.request.POST,
                                                      self.request.FILES,
                                                      instance=self.object)
        else:
            data['devicedocuments'] = DocumentFormSet(instance=self.object)
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        devicedocuments = context['devicedocuments']
        labels = self.request.POST.get("labels")
        # print(f"this is the list of errors in device documents:{devicedocuments.errors}")
        if context["has_open_commits"]:
            messages.error(self.request,
                           "Für dieses Gerät existieren noch offene Änderungen. "
                           "Solange die Änderungen offen sind, kann das Gerät nicht editiert werden.")
            return HttpResponseRedirect(self.get_success_url())

        show_error_msg(self.request, context["form"], devicedocuments)
        with transaction.atomic():
            self.object = form.save(user=self.request.user, create_commits=True)
            if devicedocuments.is_valid():
                try:
                    devicedocuments.instance = self.object
                    devicedocuments.save()
                except Exception as e:
                    messages.error(self.request,
                                   f"Es gibt einen Fehler bei der Änderung der Dokumente {e}")

        if labels is not None and labels != "":
            labels_list = [int(x["value"]) for x in ast.literal_eval(labels)]
            self.object.labels.clear()  # delete old labels
            self.object.labels.add(*labels_list)  # add all selected ones
        messages.success(self.request, f"Gerät {self.object.name} erfolgreich geändert.")
        return super(DeviceDocumentUpdate, self).form_valid(form)


class DeviceDelete(DeleteView):
    model = Device
    success_url = "devices"

    @method_decorator(login_required(login_url="login"))
    @method_decorator(allowed_users(
        allowed_roles=
        ['Schreiben - Geräte'],
        url='devices')
    )
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk):
        model = get_object_or_404(Device, pk=pk)
        try:
            commit = Commit(device=model,
                            field="!delete",
                            timestamp=django.utils.timezone.now(),
                            old_data="",
                            new_data="",
                            author=self.request.user,
                            confirmed_by=None,
                            confirmed_at=None,
                            status="not_handled")
            commit.save()
        except Exception as e:
            messages.error(self.request,
                           f"Es gibt einen Fehler beim Löschen des Geräts : {e}")

        messages.success(self.request,
                       f"Es wurde eine Anfrage zum Löschen des Geräts {model.name} erstellt. (Siehe offene Änderungen)")
        return redirect(reverse('device_description', args=(pk,)))


def custom_redirect(request, device_id):
    return redirect(f"/device/{device_id}/description")


@ensure_csrf_cookie
@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Geräte'])
def devices_overview(request, _=None):
    start_timestamp = time.time()

    current_inventory = inventory.views.get_activeInventory()
    buttons = []
    if current_inventory and request.user.hasGroup('Schreiben - Inventur'):
        buttons.append(["Inventieren", "device_inventory_add"])
        all_devices_in_current_inventory_dict = {}
        for inventoryItem in InventoryItem.objects.filter(inventory=current_inventory.id):
            all_devices_in_current_inventory_dict[inventoryItem.item_id] = True

    all_bg_images = {}
    for image in Document.objects.filter(device_id__isnull=False, marked=1):
        if os.path.exists(settings.MEDIA_ROOT + f'lowres/{image.image_name}'):
            all_bg_images[image.device_id] = f'lowres/{image.image_name}'
        else:
            all_bg_images[image.device_id] = image.image_name

    device_list = list(Device.objects
                       .select_related('location', 'location__parent', 'location__parent__parent')
                       .filter(location_id__isnull=False)
                       .order_by("id"))
    device_list_without_category = Device.objects.filter(location_id__isnull=True)

    for device in device_list:  # can we do this with annotate in the query?
        device.parent = device.location
        device.token = device.name
        device.depth = 3

    category_list = list(Location.objects.select_related('parent', 'parent__parent'))
    pending_categories = [x.location for x in
                          Commit.objects
                              .select_related('location')
                              .filter(field="!delete", location__isnull=False, status='not_handled')
                              ]

    original_object_list = category_list + device_list

    # Sort the entire list of devices and their categories so that they end up in their default sorting.
    # the important thing is, that we first sort by token for each hierarchy level and then by name to avoid conflicts with identically named tokens.
    # if we are able to sort by token in sql it would be a good bit more efficient...
    # it's still a little different compared to sql sorting, which would be important when using the prev-next button on the fullpage
    original_object_list = sorted(original_object_list,
                                  key=lambda x: (
                                      # primary by the cpp of..
                                      ((1, int(x.parent.parent.parent.token)) if x.parent.parent.parent.token.isdigit() else (2, str(x.parent.parent.parent.token))) if x.depth == 3  # .. a device
                                          else ((1, int(x.parent.parent.token)) if x.parent.parent.token.isdigit() else (2, str(x.parent.parent.token))) if x.depth == 2  # .. a category
                                          else ((1, int(x.parent.token)) if x.parent.token.isdigit() else (2, str(x.parent.token))) if x.depth == 1  # .. a cp
                                          else ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))),  # .. a cpp
                                      str(x.parent.parent.parent.name) if x.depth == 3 else str(x.parent.parent.name) if x.depth == 2 else str(x.parent.name) if x.depth == 1 else str(x.name),
                                      # secondary by cp of..
                                      ((1, int(x.parent.parent.token)) if x.parent.parent.token.isdigit() else (2, str(x.parent.parent.token))) if x.depth == 3
                                          else ((1, int(x.parent.token)) if x.parent.token.isdigit() else (2, str(x.parent.token))) if x.depth == 2
                                          else ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))) if x.depth == 1
                                          else (0, '\x00'),
                                      str(x.parent.parent.name) if x.depth == 3 else str(x.parent.name) if x.depth == 2 else str(x.name) if x.depth == 1 else '\x00',
                                      # tertiary by category of..
                                      ((1, int(x.parent.token)) if x.parent.token.isdigit() else (2, str(x.parent.token))) if x.depth == 3
                                          else ((1, int(x.token)) if x.token.isdigit() else (2, str(x.token))) if x.depth == 2
                                          else (0, '\x00'),
                                      str(x.parent.name) if x.depth == 3 else str(x.name) if x.depth == 2 else '\x00',
                                      # lastly, just so devices are behind their respective categories
                                      1 if x.depth == 3 else 0,))
    objects = []
    i = 0
    amount_of_objects = 0
    for object in original_object_list:
        if object.parent is None:
            objects.append({"level": "cpp",
                            "token": f'{object.token}',
                            "name": object.name.strip(),
                            "id": object.id,
                            "default_sorting_id": i,
                            "pending_delete": object in pending_categories,
                            })
        elif object.parent.parent is None:
            objects.append({"level": "cp",
                            "token": f'{object.parent.token}.{object.token}',
                            "name": object.name.strip(),
                            "id": object.id,
                            "default_sorting_id": i,
                            "belongs_to_cpp": object.parent.id,
                            "pending_delete": object in pending_categories,
                            })
        elif object.parent.parent.parent is None:
            objects.append({"level": "category",
                            "token": f'{object.parent.parent.token}.{object.parent.token}.{object.token}',
                            "name": object.name.strip(),
                            "id": object.id,
                            "default_sorting_id": i,
                            "belongs_to_cp": object.parent.id,
                            "belongs_to_cpp": object.parent.parent.id,
                            "pending_delete": object in pending_categories,
                            })
        else:
            try:
                bg_image = all_bg_images[object.id]
            except:
                bg_image = None

            if current_inventory:
                try:
                    inventory_bool = all_devices_in_current_inventory_dict[object.id]
                except:
                    inventory_bool = False
            else:
                inventory_bool = None

            objects.append({"level": "object",
                            "name": object.name,
                            "object_id": object.id,
                            "searchable_object_data": {"device_name": object.name,
                                                       "category": f"""{object.parent.parent.parent.token}{object.parent.parent.token}{object.parent.token}
                                                                       {object.parent.parent.parent.name}{object.parent.parent.name}{object.parent.name}""",
                                                       "description": object.description if object.description is not None else "",
                                                       "device_id": object.id,
                                                       "ifp_code": object.ifpcode if object.ifpcode is not None else "",
                                                       "inventory_number": object.inventorynumber if object.inventorynumber is not None else "",
                                                       "tags": object.tags if object.tags is not None else "", },
                            "show_only_data": {"loaned": True if object.loaned > 0 else False,
                                               "repair": True if object.repair > 0 else False,
                                               "ifp_code_bool": True if object.ifpcode is not None and object.ifpcode != "" else False,
                                               "inventory_number_bool": True if object.inventorynumber is not None else False, },
                            "default_sorting_id": i,
                            "background_image": bg_image,
                            "preview_text": object.description if object.description is not None else "",
                            "belongs_to_category": object.location.id,
                            "belongs_to_cp": object.location.parent.id,
                            "belongs_to_cpp": object.location.parent.parent.id,
                            "inventory": inventory_bool})
            amount_of_objects += 1
        i += 1

    page_buttons = []
    if request.user.is_authenticated and request.user.hasGroup("Schreiben - Geräte"):
        page_buttons.append(["Gerät hinzufügen", 'device_add', 'icons/ic_add_circle_outline_24px.svg'])
    if request.user.is_authenticated and request.user.hasGroup("Schreiben - Standorte"):
        page_buttons.append(["Standort hinzufügen", 'location_create', 'icons/ic_add_location_24px.svg'])

    context = {'path': 'device',
               'objects': objects,
               'objects_without_category': device_list_without_category,
               'search_checklist': {"device_name": "Gerätename",
                                    "category": "Standortname und Kürzel",
                                    "description": "Beschreibung",
                                    "device_id": "Geräte ID",
                                    "ifp_code": "IFP-Code",
                                    "inventory_number": "Inventarnummer",
                                    "tags": "Schlagworte"},
               'show_only_checklist': {"loaned": "Verliehen",
                                       "repair": "In Reparatur",
                                       "ifp_code_bool": "IFP-Code",
                                       "inventory_number_bool": "Inventarnummer"},
               'sort_by_values': {"device_name?str?asc": "Nach Titel aufsteigend",
                                  "device_name?str?dsc": "Nach Titel absteigend",
                                  "device_id?num?asc": "Nach ID aufsteigend",
                                  "device_id?num?dsc": "Nach ID absteigend",
                                  "ifp_code?str?asc": "Nach IFP-Code aufsteigend",
                                  "ifp_code?str?dsc": "Nach IFP-Code absteigend",
                                  "inventory_number?num?asc": "Nach Inventarnummer aufsteigend",
                                  "inventory_number?num?dsc": "Nach Inventarnummer absteigend", },
               'amount_of_objects': amount_of_objects,
               'page_buttons': page_buttons,
               'main_title': 'Geräte',
               'items': None,
               'can_edit_category': request.user.is_authenticated and request.user.hasGroup('Schreiben - Standorte'),
               'can_edit_object': request.user.is_authenticated and request.user.hasGroup('Schreiben - Geräte'),
               'buttons': buttons,
               'start_timestamp': start_timestamp,
               'view_timestamp': time.time()}

    return render(request, 'list_with_categories/list_with_categories.html', context)


def get_device_query(request):
    list_to_sort = Device.objects\
        .select_related('location', 'location__parent','location__parent__parent')\
        .order_by('id')\
        .filter(location_id__isnull=False)
    # same sorting as overview
    device_query = sorted(list_to_sort,
                          key=lambda x: (
                              ((1, int(x.location.parent.parent.token)) if x.location.parent.parent.token.isdigit() else (2, str(x.location.parent.parent.token))),
                              str(x.location.parent.parent.name),
                              ((1, int(x.location.parent.token)) if x.location.parent.token.isdigit() else (2, str(x.location.parent.token))),
                              str(x.location.parent.name),
                              ((1, int(x.location.token)) if x.location.token.isdigit() else (2, str(x.location.token))),
                              str(x.location.name),))

    return device_query


def get_previous_device(request, device_id):
    device_query = get_device_query(request)
    index = -1
    for device in device_query:
        index = index + 1
        if device.id == device_id:
            break
    if index == 0:
        previous_device = device_query[-1]
    else:
        previous_device = device_query[index - 1]
    return redirect(f'/device/{previous_device.id}/')


def get_next_device(request, device_id):
    device_query = get_device_query(request)
    index = -1
    for device in device_query:
        index = index + 1
        if device.id == device_id:
            break
    if device_query[index].id == device_query[-1].id:
        next_device = device_query[0]
    else:
        next_device = device_query[index + 1]
    return redirect(f'/device/{next_device.id}/')


def get_base_info(device):
    try:
        location = Location.objects.get(id=device.location_id)
        base_info = {'title': device.name,
                     'id': device.id,
                     'token0': location.parent.parent.token,
                     'token1': location.parent.token,
                     'token2': location.token,
                     'cpp_name': location.parent.parent.name,
                     'cp_name': location.parent.name,
                     'c_name': location.name,
                     'cpp_id': location.parent.parent.id,
                     'cp_id': location.parent.id,
                     'c_id': location.id, }
    except:  # device has no location
        base_info = {'title': device.name,
                     'id': device.id, }

    return base_info


def get_description(device):
    comment = device.comment
    description = device.description
    tags = []
    try:
        tags_db = device.tags
        for tag in json.loads(tags_db):
           tags.append(tag['value'])
    except (TypeError, json.JSONDecodeError):  # just in case tags aren't properly stored in the db
        tags = []

    description = {'description': description,
                   'comment': comment,
                   'id': device.id,
                   'inventorynumber': device.inventorynumber,
                   'amount': device.amount,
                   'repair': device.repair,
                   'loaned': device.loaned,
                   'ordernumber': device.ordernumber,
                   'supplier': device.supplier,
                   'ifpcode': device.ifpcode,
                   'tags': tags,
                   }

    return description


def get_commits(device):
    commit_query = Commit.objects\
        .select_related('author', 'confirmed_by', 'experiment', 'device')\
        .exclude(field='!migration')\
        .filter(device=device)\
        .order_by('-timestamp')

    return commits_utilities.get_list_of_commits(commit_query)


def get_inventories(device_id):
    inventory_query = InventoryItem.objects\
        .select_related('inventory')\
        .filter(item_id=device_id)\
        .order_by('-inventory')
    inventories = []
    for inv in inventory_query:
        inventories.append({
            'name': inv.inventory.name,
            'start': inv.inventory.startdate,
            'end': inv.inventory.enddate if inv.inventory.enddate is not None else "In Durchführung",
            'id': inv.inventory_id,
            'info': inv.info if inv.info else None,
        })

    return inventories


def get_administration(device):
    active_inventory = get_activeInventory()

    loan_query = LoanItem.objects\
        .select_related('loan')\
        .filter(item_id=device.id, loan__active=True)\
        .order_by('-loan')
    loans = []
    for loan in loan_query:
        loans.append({
            'id': loan.loan.id,
            'loaned_by': loan.loan.loaned_by,
            'loaned_to': loan.loan.loaned_to if loan.loan.type == 'loan' else None,
            'startdate': loan.loan.startdate,
            'enddate': loan.loan.enddate if loan.loan.type == 'loan' else '/',
        })

    return {
        'already_in_inventory': active_inventory.has_device(device) if active_inventory else None,
        'acvive_inventory': active_inventory,
        'inventories': get_inventories(device.id),
        'loans': loans,
    }


def get_files(request, device):
    if request.user.is_authenticated:
        if request.user.hasGroup("Sicherheitsstufe - VLA"):
            document_query = Document.objects\
                .filter(device=device)\
                .order_by('position', 'image_name')
        elif request.user.hasGroup("Sicherheitsstufe - TU Darmstadt"):
            document_query = Document.objects\
                .filter(device=device, private__lte=1)\
                .order_by('position', 'image_name')
        else:
            document_query = Document.objects\
                .filter(device=device, private=0)\
                .order_by('position', 'image_name')
    else:
        document_query = Document.objects\
            .filter(device=device, private=0)\
            .order_by('position', 'image_name')

    files = []
    for file in document_query:
        if os.path.exists(settings.MEDIA_ROOT + f'lowres/{file.image_name}'):
            url_lowres = f'lowres/{file.image_name}'
        else:
            url_lowres = file.image_name
        files.append({"url": file.image_name,
                      "url_lowres": url_lowres,
                      "type": file.typ,
                      "general_type": file.typ.split("/")[0],
                      "specific_type": file.typ.split("/")[1],
                      "comment": file.comment if file.comment else "",
                      "spec_type": file.image_name.split(".")[-1],
                      "name": file.name})
    return files


def get_experiments(device):
    # all labels for that device
    labels = []
    for label in device.labels.all():
        labels.append({'name': label.name,
                       'id': label.id,
                       'color': label.get_color(),
                       'experiments': label.experiment_set, })  # and their associated experiments

    # all experiments without a label
    experiments_without_label = device.experiment_set.all()
    if experiments_without_label:  # and put them in a dedicated label of their own
        labels.append({'name': 'Weitere Versuche',
                       'id': -1,
                       'color': '#808080',
                       'experiments': experiments_without_label, })

    experiments = {"labels": labels}
    return experiments


@ensure_csrf_cookie
@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Geräte'])
def device_fullpage(request, device_id, _=None):
    t1 = time.time()

    try:
        device = Device.objects.get(id=device_id)
    except ObjectDoesNotExist as e:
        raise Http404

    files = get_files(request, device)  # files are used in description and files tab, so we grab them beforehand

    tabs = {}
    if request.user.is_authenticated and request.user.hasGroup("Lesen - Geräte - Tab Info"):
        amount_of_media_to_display = global_utilities.amount_of_media_to_display(files)
        content = get_description(device)
        content['amount_to_display'] = amount_of_media_to_display
        content['files'] = files
        tabs['description'] = {'content': content,
                               'displayed_name': 'Info',
                               'url_name': 'description',
                               'template': 'fullpage/device_description.html', }
    if request.user.is_authenticated and request.user.hasGroup("Lesen - Geräte - Verwaltung"):
        tabs['administration'] = {'content': get_administration(device),
                                  'displayed_name': 'Verwaltung',
                                  'url_name': 'inventory',
                                  'template': 'fullpage/inventory.html', }
    if request.user.is_authenticated and request.user.hasGroup("Lesen - Geräte - Tab Versuche"):
        tabs['experiments'] = {'content': get_experiments(device),
                               'displayed_name': 'Versuche',
                               'url_name': 'experiments',
                               'template': 'fullpage/experiments.html', }
    if request.user.is_authenticated and request.user.hasGroup("Lesen - Geräte - Tab Dateien"):
        tabs['files'] = {'content': files,
                         'displayed_name': 'Dateien',
                         'url_name': 'documents',
                         'template': 'fullpage/files.html', }
    if request.user.is_authenticated and request.user.hasGroup("Lesen - Geräte - Tab Änderungen"):
        tabs['commits'] = {'content': get_commits(device),
                           'displayed_name': 'Änderungen',
                           'url_name': 'commits',
                           'template': 'fullpage/commits.html', }

    context = {'base_info': get_base_info(device),
               'path': 'device',
               'tabs': tabs,
               'start_timestamp': t1,
               'view_timestamp': time.time(),
               'main_title': 'Gerät',
               }

    return render(request, 'fullpage/fullpage.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Standorte'], url='devices')
def location_create(request, *args, **kwargs):
    """Generates form for creating a new location"""
    parent = kwargs.get('categoryid', None)
    main_title = "Standort erstellen"
    if parent:
        form = forms.CreateLocationForm(initial={'parent': parent})
        parent_location = Location.objects.get(id=parent)
        main_title = f"Unterstandort von {get_whole_token(parent_location)} | {parent_location} erstellen"
    else:
        form = forms.CreateLocationForm()
    form.fields['parent'].choices = get_locations(Location.objects.filter(depth__in=[0, 1]), 0)
    if request.method == 'POST':
        form = forms.CreateLocationForm(request.POST)
        form.fields['parent'].choices = get_locations(Location.objects.filter(depth__in=[0, 1]), 0)
        if form.is_valid():
            location = form.save(commit=False)
            if location.get_depth() < 3:
                parentid = form.data['parent']
                location.parent = None if parentid == "" else Location.objects.get(id=parentid)
                location.save()
                messages.success(request, f"Standort {location.name} erfolgreich erstellt.")
                return redirect(f"{reverse('devices')}?category={location.id}")
            else:
                messages.error(request, "Es sind keine Standorte tiefer als Ebene 3 erlaubt.")

    context = {
        'form': form,
        'main_title': main_title,
    }

    return render(request, 'forms/category.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Standorte'], url='devices')
def location_edit(request, locationid):
    """Generates form for editing an existing location"""
    device_list = label_utilities.getDevices(Device.objects)

    try:
        current_location = Location.objects.get(id=locationid)
    except ObjectDoesNotExist as e:
        raise Http404

    location_devices = label_utilities.getDevices(current_location.device_set)
    location_devices = json.dumps([dict(x, clean_name=escape(x["name"]), value=x["id"]) for x in location_devices])

    current_name = current_location.name
    current_token = current_location.token
    current_parent = current_location.parent
    whole_token = get_whole_token(current_location)
    current_parent_depth = current_location.get_depth() - 1

    if request.method == 'POST':
        form = forms.EditLocationForm(request.POST, instance=current_location)

        form.fields['parent'].choices = get_locations(Location.objects.filter(depth=current_parent_depth),
                                                     current_location.get_depth())
        if form.is_valid():
            location = form.save(commit=False)
            if location.get_depth() < 3:
                parentid = form.data['parent']
                location.parent = None if parentid == "" else Location.objects.get(id=parentid)
                location.save()

                if current_location.depth == 2:
                    devices = form.data['device']
                    if devices != '':
                        devices = json.loads(devices)
                        devices = [int(d.get('value')) for d in devices]
                        # Removes all devices from location that have been deleted
                        for d in location.device_set.all():
                            if d.id not in devices:
                                Commit(device_id=d.id,
                                       author_id=request.user.id,
                                       timestamp=django.utils.timezone.now(),
                                       field='location',
                                       old_data=d.location,
                                       new_data=None,
                                       status='auto_approved',
                                       ).save()
                                d.location = None
                                d.save()

                        # Adds newly selected devices
                        old_devices = [x.id for x in location.device_set.all()]
                        for d in devices:
                            if d not in old_devices:
                                current_device = Device.objects.get(id=d)
                                old_location = None if current_device.location is None else current_device.location.id
                                Commit(device_id=current_device.id,
                                       author_id=request.user.id,
                                       timestamp=django.utils.timezone.now(),
                                       field='location',
                                       old_data=old_location,
                                       new_data=location.id,
                                       status='auto_approved',
                                       ).save()
                                current_device.location = location
                                current_device.save()

                messages.success(request, f"Standort {location.name} erfolgreich geändert.")
                return redirect(f"{reverse('devices')}?category={location.id}")
            else:
                messages.error(request, "Es sind keine Standorte tiefer als Ebene 3 erlaubt.")
    else:
        initial_values = {
            'name': current_name,
            'token': current_token,
            'parent': "" if current_parent is None else current_parent.id,
            'device': location_devices,
        }
        form = forms.EditLocationForm(instance=current_location, initial=initial_values)
        if current_location.depth != 2:
            form.fields['device'].widget = django_forms.HiddenInput()

        form.fields['parent'].choices = get_locations(Location.objects.filter(depth=current_parent_depth),
                                                      current_location.get_depth())
        if current_location.pending_delete():
            messages.error(request, "Für diesen Standort wurde bereits eine Anfrage zum Löschen erstellt.")

    context = {
        'form': form,
        'main_title': f"{whole_token} | {current_name} bearbeiten",
        'id': current_location.id,
        'delete_url': 'location_delete',
        'device_list': device_list,
    }

    return render(request, 'forms/category.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Standorte'], url='devices')
def location_delete(request, locationid):
    """Deletes a location"""
    name = None
    try:
        location = Location.objects.get(id=locationid)
        name = location.name
        commit = Commit(location=location,
                        field="!delete",
                        timestamp=django.utils.timezone.now(),
                        old_data=None,
                        new_data=None,
                        author=request.user,
                        confirmed_by=None,
                        confirmed_at=None,
                        status="not_handled")
        commit.save()
        messages.success(request,
                         f"Es wurde eine Anfrage zum Löschen des Standorts {name} erstellt. (Siehe offene Änderungen)")
    except Location.DoesNotExist:
        messages.error(request, "Der angeforderte Standort existiert nicht.")
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)

    return redirect(f"{reverse('devices')}?category={locationid}")
