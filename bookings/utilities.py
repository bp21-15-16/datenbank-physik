
def get_whole_token(object):
    token = object.token
    while object.parent is not None:
        object = object.parent
        token =  object.token + "." + token
    return token


def get_experiments(query):
    experiment_query = list(query.select_related('category', 'category__parent', 'category__parent__parent'))
    exp_choices = []
    for e in experiment_query:
        token = "" if e.category is None else get_whole_token(e.category)
        exp_choices.append({'id': e.id, 'name': f'{token}.{e.experiment_number} {e.name}'})
    return sorted(exp_choices, key=lambda d: d['name'])


def get_experiments_prep_time(query):
    experiment_query = list(query.select_related('category'))
    exp_choices = []
    for e in experiment_query:
        exp_choices.append({'id': e.id, 'preparation_time': e.preparation_time if e.preparation_time is not None else 1440})
    return exp_choices
