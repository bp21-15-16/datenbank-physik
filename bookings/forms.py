from django import forms


class CreateNewBookingForm(forms.Form):

    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'style': 'width: 100%;',
                   'placeholder': 'Buchungsname...'}))
    date = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={'class': 'datetimefield', 'type': 'date', 'style': 'width: 100%;'}))
    comment = forms.CharField(
        widget=forms.Textarea(
            attrs={'cols': 0, 'rows': 6,
                   'placeholder': 'Buchungskommentar...'}),  required=False)
    message_vla = forms.CharField(
        widget=forms.Textarea(
            attrs={'cols': 0, 'rows': 6,
                   'placeholder': 'Nachricht an die VLA... \nAuf diesem Weg können Sie die Vorlesungsassistenz z.B. '
                                  'darüber informieren, dass Versuche zu einem anderen als dem regulär vereinbarten '
                                  'Ansichtstermin abgenommen werden sollen.'}), required=False)

    experiment = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': "experiment_select",
                'id': "experiment_id",
                'placeholder': "Versuche auswählen...",
                'values': "Test"

            }
        ),
        label="Versuche",
        required=False,
    )
