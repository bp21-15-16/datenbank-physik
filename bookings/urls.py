from django.urls import path
from . import views

urlpatterns = [
    path('private_bookings/', views.create_private_booking, name="private_bookings"),
    path('all_bookings/', views.create_all_bookings, name="all_bookings"),
    path('<int:reservationid>/remove/<str:send_mail>', views.delete_booking, name="delete_booking"),
]