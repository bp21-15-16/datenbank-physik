from django.shortcuts import render
from django.contrib import messages
from . import forms, utilities
from public.models import Reservation, ReservationExperiment, Experiment
import datetime
import json
import global_utilities
import django.utils.timezone
from accounts.decorators import *
from django.contrib.auth.decorators import login_required


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Eigene Buchungen'])
def create_private_booking(request):
    delete_cookie = False
    if request.method == 'POST':
        form = forms.CreateNewBookingForm(request.POST)
        if form.is_valid():
            form = forms.CreateNewBookingForm(request.POST)
            user_id = request.user.id
            ordertime = django.utils.timezone.now()
            status = 1
            tz = django.utils.timezone.get_current_timezone()
            lecture_date = django.utils.timezone.make_aware((datetime.datetime.strptime(form.data['date'], '%Y-%m-%d %H:%M:%S')), tz)
            comment = form.data['comment']
            message = form.data['message_vla']
            name = form.data['name']

            if lecture_date < ordertime + datetime.timedelta(1):
                messages.error(request, "Die Vorbereitungsdauer wurde nicht eingehalten.")

            reservation = Reservation.objects.create(user_id = user_id, ordertime = ordertime, status = status, lecture_date = lecture_date, comment = comment, message = message, name = name)

            try:
                for e in json.loads(form.data["experiment"]):
                    ReservationExperiment.objects.create(reservation_id = reservation.id, experiment_id = e['value'], status = 1, pinned_on = ordertime)
            except:
                messages.error(request, "Sie haben keine Versuche ausgewählt!")

            subject = ('Neue Buchung erhalten')
            msg = f'Eine neue Buchung mit dem Namen "{name}" wurde von {request.user.username} für den {lecture_date.strftime("%d.%m.%Y %H:%M")} erstellt.\n'
            msg += f'\n\nEs sollen folgende Versuche gezeigt werden:\n'
            try:
                for experiment in json.loads(form.data["experiment"]):
                    msg += f'{experiment["name"]}\n'
            except:
                msg += f'Es wurde kein Versuch ausgewählt\n'
            if message:
                msg += f'\n\nNachricht an die VLA:'
                msg += f'\n{message}'
            global_utilities.send_mail(subject, msg)

            form = forms.CreateNewBookingForm()
            messages.success(request, "Die Buchung wurde erfolgreich versendet.")
            delete_cookie = True
        else:
            messages.error(request, "FEHLER: Es ist ein unbekannter Fehler aufgetreten. Bitte versuche es erneut.")
    else:
        form = forms.CreateNewBookingForm()

    bookings_query_past_user = get_user_booking_information(request, False)
    bookings_query_experiment = get_all_booking_experiment_info()

    bookings_query_future_user = get_user_booking_information(request, True)

    context = {'form': form,
               'experiment_list': utilities.get_experiments(Experiment.objects),
               'experiment_preptime': utilities.get_experiments_prep_time(Experiment.objects),
               'js_script': 'js/booking_form.js',
               'bookings_query_past': bookings_query_past_user,
               'bookings_query_experiment': bookings_query_experiment,
               'bookings_query_future': bookings_query_future_user,
               'main_title': 'Meine Buchungen'}

    if delete_cookie:
        response = render(request, 'bookings/private_bookings.html', context)
        response.delete_cookie('bookingsCookie')
        return response
    else:
        return render(request, 'bookings/private_bookings.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Alle Buchungen'], url='private_bookings')
def create_all_bookings(request):
    bookings_query_past_all = get_all_booking_information(False)
    bookings_query_past_all_experiments = get_all_booking_experiment_info()

    bookings_query_future_all = get_all_booking_information(True)

    context = {
        'bookings_query_past': bookings_query_past_all,
        'bookings_query_experiment': bookings_query_past_all_experiments,
        'bookings_query_future': bookings_query_future_all,
        'main_title': 'Alle Buchungen'}

    return render(request, 'bookings/all_bookings.html', context)


def get_all_booking_information(in_future):
    if in_future:
        bookings_query = Reservation.objects\
            .select_related("user")\
            .order_by('-lecture_date')\
            .filter(lecture_date__gt=django.utils.timezone.now())
    else:
        bookings_query = Reservation.objects\
            .select_related("user")\
            .order_by('-lecture_date')\
            .filter(lecture_date__lte=django.utils.timezone.now())
    return bookings_query


def get_all_booking_experiment_info():
    bookings_query_experiment = ReservationExperiment.objects.select_related("reservation", "experiment")
    return bookings_query_experiment


def get_user_booking_information(request, in_future):
    current_user_id = request.user.id
    if in_future:
        bookings_query = Reservation.objects\
            .select_related("user")\
            .filter(user=current_user_id, lecture_date__gt=django.utils.timezone.now())\
            .order_by('-lecture_date')
    else:
        bookings_query = Reservation.objects\
            .select_related("user")\
            .filter(user=current_user_id, lecture_date__lte=django.utils.timezone.now())\
            .order_by('-lecture_date')
    return bookings_query


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Eigene Buchungen'])
def delete_booking(request, reservationid, send_mail):
    subject = f'Die Buchung {Reservation.objects.get(id=reservationid).name} vom {Reservation.objects.get(id=reservationid).lecture_date.strftime("%d.%m.%Y %H:%M")} wurde storniert'
    msg = f'{Reservation.objects.select_related("user").get(id=reservationid).user.username} hat die Buchung ' \
          f'{Reservation.objects.get(id=reservationid).name} für den {Reservation.objects.get(id=reservationid).lecture_date.strftime("%d.%m.%Y %H:%M")} storniert.'

    try:
        ReservationExperiment.objects.filter(reservation_id=reservationid).delete()
        Reservation.objects.filter(id=reservationid).delete()
        if (send_mail == 'send_mail'):
            global_utilities.send_mail(subject, msg)

        messages.success(request, f"Buchung erfolgreich entfernt.")
    except Reservation.DoesNotExist|ReservationExperiment.DoesNotExist:
        messages.error(request, "Die angeforderte Buchung existiert nicht.")
    except Exception as e:
        pass

    return redirect(request.headers['Referer'])



