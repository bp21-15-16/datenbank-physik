from django.test import TestCase, Client, SimpleTestCase, TransactionTestCase
from django.urls import reverse, resolve

from public.models import Experiment, Category, Commit, Document, Comment, Location, Device
import experiment_list.views as elist
import django.utils.timezone

class TestViews(TestCase):
    databases = '__all__'

    def test_add_experiment_url_is_resolved(self):
        url = reverse('add_experiment')
        self.assertEquals(resolve(url).func, elist.add_experiment)

    def test_delete_experiment_url_is_resolved(self):
        url = reverse('experiment_delete')
        self.assertEquals(resolve(url).func, elist.experiments_delete)

    def test_experiment_comment_url_is_resolved(self):
        url = reverse('experiment_comments')
        self.assertEquals(resolve(url).func, elist.experiment_fullpage)

    def test_experiment_devices_url_is_resolved(self):
        url = reverse('experiment_devices')
        self.assertEquals(resolve(url).func, elist.experiment_fullpage)

    def test_model_category(self):
        ppcat = Category(name="D0", token="D0", parent=None, depth=0)
        ppcat.save()
        pcat = Category(name="D1", token="D1", parent=ppcat, depth=1)
        pcat.save()
        cat = Category(name="D2", token="D2", parent=pcat, depth=2)
        cat.save()
        cat2 = Category(name="D2.1", token="D2.1", parent=pcat, depth=2)
        cat2.save()
        foundcat = None
        findppcat = Category.objects.all().filter(depth=0).filter(name="D0")
        for x in findppcat:
            if x.token == "D0":
                foundcat = x
        self.assertEquals(foundcat, ppcat)
        childs_of_ppcat = Category.objects.all().filter(parent=foundcat)
        grandchilds_of_ppcat = None
        for x in childs_of_ppcat:
            self.assertEquals(x.parent, foundcat)
            grandchilds_of_ppcat.append(Category.objects.all().filter(parent=x))

        for queryset in grandchilds_of_ppcat:
            for x in queryset:
                self.assertEquals(x.parent.parent, foundcat)

        cat2.delete()
        cat.delete()
        pcat.delete()
        ppcat.delete()

    def test_model_Comment(self):
        exp = Experiment(category=None, name="Testing the comments", description="nothing but comments",
                         experiment_number=554, status="available", preparation_time=25, execution_time=35)
        exp.save()
        comment1 = Comment(parent_comment=None, experiment=exp, user_id=63, type="PUBLIC",
                           message="ha lol! das kenne ich!", timestamp=django.utils.timezone.now())
        comment1.save()
        comment2 = Comment(parent_comment=None, experiment=exp, user_id=63, type="VLA",
                            message = "look at this cat", timestamp = django.utils.timezone.now())
        comment2.save()
        comment3 = Comment(parent_comment=None, experiment=exp, user_id=63, type="DEMO",
                            message = "ha schösch", timestamp = django.utils.timezone.now())
        comment3.save()
        comment4 = Comment(parent_comment=None, experiment=exp, parent_comment_id= comment1.id, user_id=63, type="PUBLIC",
                            message = "ha lol! das kenne ich!", timestamp = django.utils.timezone.now())
        comment4.save()

        publiccomments_for_exp = Comment.objects.all().filter(experiment=exp).filter(type="PUBLIC")
        self.assertIn(comment1, publiccomments_for_exp)
        self.assertIn(comment4, publiccomments_for_exp)
        self.assertEquals(comment4.parent_comment, comment1)

        vlacomments_for_exp = Comment.objects.all().filter(experiment=exp).filter(type="VLA")
        self.assertIn(comment2, vlacomments_for_exp)

        democomments_for_exp = Comment.objects.all().filter(experiment=exp).filter(type="DEMO")
        self.assertIn(comment3, democomments_for_exp)

        comment4.delete()
        comment3.delete()
        comment2.delete()
        comment1.delete()
        exp.delete()

    def test_model_Location(self):
        location1 = Location(name="Pilotygebaeude", token="S202", parent=None, depth=0)
        location1.save()
        location2 = Location(name="Heenes-Buero", token="HB", parent=location1, depth=1)
        location2.save()
        location3 = Location(name="Tresor", token="T", parent=location2, depth=2)
        location3.save()
        location31 = Location(name="Schublade", token="S", parent=location2, depth=2)
        location31.save()

        getpiloty = Location.objects.all().filter(token="S202")
        piloty = None
        for x in getpiloty:
            if x.name == "Pilotygebaeude":
                piloty = x

        self.assertEquals(piloty, location1)
        get_children_of_piloty = Location.objects.all().filter(parent=location1)

        for x in get_children_of_piloty:
            self.assertEquals(x.parent, location1)

        get_children_of_heenes_buero = Location.objects.all().filter(parent=location2)

        for x in get_children_of_heenes_buero:
            self.assertEquals(x.parent, location2)

        location31.delete()
        location3.delete()
        location2.delete()
        location1.delete()

    def test_model_device(self):
        location1 = Location(name="Pilotygebaeude", token="S202", parent=None, depth=0)
        location1.save()
        location2 = Location(name="Heenes-Buero", token="HB", parent=location1, depth=1)
        location2.save()
        location3 = Location(name="Tresor", token="T", parent=location2, depth=2)
        location3.save()
        dev_sda1 = Device(name="Fluxkompensator", inventorynumber=443, description="bei 88 Meilen pro Stunde Zeitreise moeglich",
                          comment="Hammermaessig!", location=location3, repair=1)
        dev_sda1.save()

        get_devices_from_heenes_tresor = Device.objects.all().filter(location=location3)
        for x in get_devices_from_heenes_tresor:
            self.assertEquals(x, dev_sda1)

        dev_sda1.delete()
        location3.delete()
        location2.delete()
        location1.delete()

    def test_model_experiment(self):
        mothercat = Category(name="Katzen", token="K", parent=None, depth=0)
        mothercat.save()
        childcat = Category(name="Britisch-Kurzhaar", token="BKH", parent=mothercat, depth=1)
        childcat.save()
        grandchildcat = Category(name="Katze1", token="K1", parent=childcat, depth=2)
        grandchildcat.save()

        exp1 = Experiment(category=grandchildcat, name="irgendein experiment", description="eigentlich nicht",
                          comment="immernoch nicht", preparation_time=12, status="available", execution_time=555, experiment_number=2323)
        exp1.save()
        exp2 = Experiment(category=grandchildcat, name="noch ein exp", description="nein, immernoch nicht",
                          comment="nein!!!", status="draft", execution_time=34, preparation_time=2323, experiment_number=12)
        exp2.save()

        exps_from_cat = Category.objects.all().filter(category=grandchildcat)
        for x in exps_from_cat:
            if x.status == "draft":
                self.assertEquals(x, exp2)
            else:
                self.assertEquals(x, exp1)

        exp2.delete()
        exp1.delete()
        grandchildcat.delete()
        childcat.delete()
        mothercat.delete()

    def test_model_commit(self):
        mothercat = Category(name="Katzen", token="K", parent=None, depth=0)
        mothercat.save()
        childcat = Category(name="Britisch-Kurzhaar", token="BKH", parent=mothercat, depth=1)
        childcat.save()
        grandchildcat = Category(name="Katze1", token="K1", parent=childcat, depth=2)
        grandchildcat.save()

        exp1 = Experiment(category=grandchildcat, name="irgendein experiment", description="eigentlich nicht",
                          comment="immernoch nicht", preparation_time=12, status="available", execution_time=555,
                          experiment_number=2323)
        exp1.save()

        com = Commit(experiment= exp1, author_id=63, field="name", old_data="irgendein experiment", new_data="irgendein experiment!",
                     status="not_handled", timestamp=django.utils.timezone.now())
        com.save()

        get_commits_from_exp1 = Commit.objects.all().filter(experiment=exp1)
        for x in get_commits_from_exp1:
            self.assertEquals(x, com)

        com.delete()
        self.assertEquals(len(Commit.objects.all().filter(experiment=exp1)), 0)
        newcom = Commit(experiment=exp1, author_id=63, field="status", old_data="available", new_data="notavailable", status="auto_approved",
                        timestamp=django.utils.timezone.now())
        newcom.save()

        get_commits_from_exp1 = Commit.objects.all().filter(experiment=exp1)
        for x in get_commits_from_exp1:
            self.assertEquals(x, newcom)

        newcom.delete()
        exp1.delete()
        grandchildcat.delete()
        childcat.delete()
        mothercat.delete()

