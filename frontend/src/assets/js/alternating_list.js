document.addEventListener("DOMContentLoaded", function () {
    let all_extended_infos = document.querySelectorAll('table.alternating_list_extendable tr:nth-of-type(2n+2)');

    $('table.alternating_list_extendable tr:nth-of-type(2n+1)').click(function () {
        for (let i = 0; i < all_extended_infos.length; i++) {
            if (this.nextElementSibling !== all_extended_infos[i]) {
                all_extended_infos[i].style.display = "none";
            } else {
                if (this.nextElementSibling.style.display === "none") {
                    this.nextElementSibling.style.display = "";
                } else {
                    this.nextElementSibling.style.display = "none";
                }
            }
        }
    });
});