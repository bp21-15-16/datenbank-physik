//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


(function ($, undefined) {
    $.fn.getCursorPosition = function () {
        var el = $(this).get(0);
        var pos = 0;
        if ('selectionStart' in el) {
            pos = el.selectionStart;
        } else if('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    }
})($);

window.insertAtCursor = function insertAtCursor(myField, startValue, endValue) {
    var txt = myField.get(0);
    var start = txt.selectionStart;
    var end = txt.selectionEnd;
    var new_text =
        myField.val().slice(0, start)
        + startValue
        + myField.val().slice(start, end)
        + endValue
        + myField.val().slice(end);
    txt.value = new_text;
    txt.focus();
    txt.selectionStart = start+startValue.length;
    txt.selectionEnd = end+startValue.length;
    txt.style.height = "auto";
    txt.style.height = (txt.scrollHeight) + "px";
}


window.switchEditorPreview = function switchEditorPreview(self, render, textbox) {
    const csrftoken = getCookie('csrftoken');
    if (self.checked) {
        $.ajax({
            url: "/bbcode_preview/",
            type: 'POST',
            data: {
                content: textbox.val()
            },
            headers: {
                'X-CSRFToken': csrftoken
            },
        }).done(function (data) {
                render.html(data);
                if (self.checked) {
                    render.show();
                    textbox.hide();
                }
                // Attempt to typeset whatever mathjax content the user has input in the editor
                // For more info see initialization base.html and https://docs.mathjax.org/en/latest/web/typeset.html
                MathJax.typesetPromise();
            });
        let preview_button = self.parentElement.parentElement.getElementsByClassName('check_container')
        $(preview_button).css({'color': 'red', 'padding': '5px 6px 0 6px'});
        let editor_buttons = self.parentElement.parentElement.getElementsByClassName('editor_button')
        for (let i=0, n=editor_buttons.length; i<n; i++){
            $(editor_buttons[i]).hide();
        }
    } else {
        render.hide();
        textbox.show();
        let preview_button = self.parentElement.parentElement.getElementsByClassName('check_container')
        $(preview_button).css({'color': '', 'padding': '5px 6px 0 6px'});
        let editor_buttons = self.parentElement.parentElement.getElementsByClassName('editor_button')
        for (let i=0,n=editor_buttons.length; i<n; i++){
            $(editor_buttons[i]).show();
        }
    }
}


window.auto_resize_textarea = function auto_resize_textarea(textarea) {
    textarea.style.height = "auto";
    textarea.style.height = textarea.scrollHeight + "px";
}

document.addEventListener("DOMContentLoaded", function () {
    let auto_resizable_textareas = document.getElementsByClassName("auto_resizable_textarea");
    for (let i = 0, n = auto_resizable_textareas.length; i < n; i++) {
        auto_resizable_textareas[i].dispatchEvent(new InputEvent('input'));
    }
});