$(document).ready(function () {

    $('textarea[name=message]').on('input', function (event) {
        event.preventDefault();
        const field = event.currentTarget;
        const button = event.currentTarget.parentElement.parentElement.parentElement.getElementsByClassName("button")[0];
        if(field.value.length > 0)
            button.disabled = false;
        else
            button.disabled = true;
    });
    $('textarea[name=message]').trigger('input');

    $('textarea[name=message]').on('keydown', function (e) {
        if (e.key == 'Tab') {
            e.preventDefault();
            var start = this.selectionStart;
            var end = this.selectionEnd;

            // set textarea value to: text before caret + tab + text after caret
            this.value = this.value.substring(0, start) +
                "\t" + this.value.substring(end);

            // put caret at right position again
            this.selectionStart =
                this.selectionEnd = start + 1;
        }
    });

    const selector = $('.toggle_comment_type');
    if(getCookie("comment_category")) {
        selector.val(getCookie("comment_category"));
    }

    let select_val = selector.val();
    $('div[id=' + select_val + ']').toggle();
    $('form[id=new_comment]').find('input[name=type]').val(select_val);

    // const options = $.map($('.toggle_comment_type option'), function(e) { return e.value; });
    // console.log(select_val);
    // if(options.length < 2)
    //     selector.parent().parent().hide();

    $('.toggle_comment_type').on('change', function (event) {
        event.preventDefault();
        const value = event.currentTarget.value;
        $('div[id=' + value + ']').toggle();
        $('div[id=' + select_val + ']').toggle();
        $('form[id=new_comment]').find('input[name=type]').val(value);
        setCookie("comment_category", value, 0);
        select_val = value;
    });

    $('body').on('click', 'a[class=open_reply_comment]', function (event) {
        event.preventDefault();
        const id = event.currentTarget.parentElement.parentElement.id;

        $('form[id=' + id + ']').toggle();
        $('form[class=reply_comment]').each(function () {
            const other_id = $(this).attr('id');
            const style = $(this).attr('style')
            if (id != other_id && style != 'display: none')
                $(this).css('display', 'none');
            ;
        });
        $('textarea[name=message]').trigger('input');
    });
})
