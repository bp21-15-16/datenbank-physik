window.updateUrlParamInPlace = function updateUrlParamInPlace(parameter, value) { // used to replace a parameter in the url without reloading the page
    let parameters = new URLSearchParams(window.location.search); // get current url parameters
    parameters.set(parameter, value); // add or update specified parameter
    window.history.replaceState('', '', "?" + parameters + window.location.hash); // remember to add the hash when replacing current state
    return;
}

window.deleteUrlParamInPlace = function deleteUrlParamInPlace(parameter) { // used to delete a parameter in the url without reloading
    let parameters = new URLSearchParams(window.location.search); // get current url parameters
    parameters.delete(parameter); // add or update specified parameter
    window.history.replaceState('', '', "?" + parameters + window.location.hash); // remember to add the hash when replacing current state
    return;
}