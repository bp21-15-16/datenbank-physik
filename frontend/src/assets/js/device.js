//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

$(document).ready(function () {
    var ifpCode = $('#device_ifpcode');
    var inventorynumber = $('#device_inventorynumber');
    var amount = $('#device_amount');

    if (ifpCode.val() || inventorynumber.val()) {
        amount.attr('readonly', 'readonly');
        amount.val(1);
        amount.css('color', 'gray');
    }

    ifpCode.on("change", function () {
        if (this.value || inventorynumber.val()) {
            amount.attr('readonly', 'readonly');
            amount.val(1);
            amount.css('color', 'gray');
        } else {
            amount.removeAttr('readonly');
            amount.css('color', '');
        }
    });
    inventorynumber.on("change", function () {
        if (this.value || ifpCode.val()) {
            amount.attr('readonly', 'readonly');
            amount.val(1);
            amount.css('color', 'gray');
        } else {
            amount.removeAttr('readonly');
            amount.css('color', '');
        }
    });

});
