window.togglePreviousBox = function togglePreviousBox(text) {
    let checkBox = text.previousElementSibling;
    checkBox.checked = !checkBox.checked;
    onInputSearch();
}

window.toggleNextBox = function toggleNextBox(text) {
    let checkBox = text.nextElementSibling;
    checkBox.checked = !checkBox.checked;
    onInputSearch();
}