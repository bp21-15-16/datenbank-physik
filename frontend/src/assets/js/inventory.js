window.fullpageInventory = function (object, id, text, already_in_inventory) {
    $.ajax({
        url: "/inventory/fullpage/",
        type: 'POST',
        data: {
            device_id: id,
            text: text,
            already_in_inventory: already_in_inventory,
        },
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
    }).done(function (response) {
        $('#tab_inventory').html(response)
    })
}

window.addToInventory = function (object, id) {
    let value = object.attr('inventory');
    if (value == 'true')
        value = true;
    else
        value = false;
    $.ajax({
        url: "/inventory/change_device/",
        type: 'POST',
        data: {
            device_id: id,
            inventory: value
        },
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
    }).done(function () {
            value = !value;
            if(value){
                document.getElementById("bereits_inventarisiert"+id).style.display = "block";
                document.getElementById("inventarisieren"+id).style.display = "none";}
            else{
                document.getElementById("bereits_inventarisiert"+id).style.display = "none";
                document.getElementById("inventarisieren"+id).style.display = "block";}
            object.attr('inventory', value);
        })
}

window.toggleDeviceList = function (h, list) {
    if(list.css("display") == 'none')
        h.css("color", '#1a1919');
    else
        h.css("color", '#d2d2d2');
    list.toggle();
}