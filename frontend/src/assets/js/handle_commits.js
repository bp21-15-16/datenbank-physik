window.handle_commit = function handle_commit(div, commit_id, action) {
    $.ajax({
        url: "/commits/handle_commit/",
        type: 'POST',
        data: {
            commit_id: commit_id,
            action: action,
        },
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        success: function (response) {
            $(div.parentElement).replaceWith(response)
        }
    })
};