/**
 * opens the given dropdown menue and closes all other dropdown menues that are open
 * @param id: the given dropdown id
 */
window.toggleDropDownClick = function (id) {
    let current = $(".show");
    let trigger = $('#' + id);

    if (trigger.hasClass("show")) {
        trigger.slideToggle(250, "swing");
        current.removeClass("show");
    } else {
        current.slideToggle(250, "swing");
        current.removeClass("show");
        trigger.slideToggle(250, "swing");
        trigger.addClass("show");
    }
}

// window.toggleDropDownButtonfunction = function(event, id)
// {
//     if (event.key === " " || event.key === "Enter" || event.key === "Spacebar") { // "Spacebar" for IE11 support
//         // Prevent the default action to stop scrolling when space is pressed
//         event.preventDefault();
//         document.getElementById(id).classList.toggle("show");
//     }
//
// }

/**
 * closes all dropdown menues if the user clicks outside of them
 */
$(document).on("click", function (event) {
    let trigger = $(".show");
    let target = $(event.target).closest('.dropdown_anchor').get(0);
    let wrapper = $(event.target).closest('.dropdownLogin-form').get(0);

    if (!target && !wrapper) {
        trigger.slideToggle(250, "swing");
        trigger.removeClass("show");
    }
});
