// used to match and remove special characters from the search string (and the results)
// match any leading whitespace | match any non words/digits, non umlauts, non spaces | match any trailing whitespace
window.search_term_regex = /(^\s+)|([^\wäáàâëéèêïíìîöóòôüúùûß ])|(\s+$)/g;
// match same as search_term_regex above, except do actually match spaces mid text.
window.text_content_regex = /(^\s+)|([^\wäáàâëéèêïíìîöóòôüúùûß])|(\s+$)/g;

document.addEventListener("DOMContentLoaded", function () {
    let search_input = document.getElementById('search-input');

    let pathname = window.location.pathname;
    let search_location = 'experiment'; // default fallback is to search experiment
    search_input.placeholder = 'Suche in Versuchen...';

    // in case on of these regexes matches, we use a different search method instead
    if (/^\/experiment\/(category\/[0-9]*\/)?(\?.*)?$/.test(pathname)) { // experiments overview
        search_location = 'local';
        search_input.placeholder = 'Suche in Versuchen...';
    } else if (/^\/experiment\/.*/.test(pathname)) { // experiments fullpage
        search_location = 'experiment';
        search_input.placeholder = 'Suche in Versuchen...';
    } else if (/^\/device\/(\?.*)?$/.test(pathname)) { // devices overview
        search_location = 'local';
        search_input.placeholder = 'Suche in Geräten...';
    } else if (/^\/(device|inventory)\/.*/.test(pathname)) { // devices fullpage, inventory
        search_location = 'device';
        search_input.placeholder = 'Suche in Geräten...';
    } else if (/^\/commits\/(needs_confirmation\/|experiments\/|devices\/|none\/)?(\?.*)?$/.test(pathname)) { // commits overview
        search_location = 'local';
        search_input.placeholder = 'Suche in Änderungen...';
    } else if (/^\/commits\/.*/.test(pathname)) { // commits fullpage
        search_location = 'commits';
        search_input.placeholder = 'Suche in Änderungen...';
    } else if (/^\/label\/(\?.*)?$/.test(pathname)) { // labels overview
        search_location = 'local';
        search_input.placeholder = 'Suche in Labels...';
    } else if (/^\/label\/.*/.test(pathname)) { // labels fullpage
        search_location = 'label';
        search_input.placeholder = 'Suche in Labels...';
    } else if (/^\/bookings\/(private_bookings|all_bookings)\/(\?.*)?$/.test(pathname)) { // bookings overview, there is no bookings fullpage
        search_location = 'local';
        search_input.placeholder = 'Suche in Buchungen...';
    }

    let parameters = new URLSearchParams(window.location.search);
    document.getElementById("search-input").value = parameters.get("search_input"); // add search parameters into search bar on all pages

    if (search_location === 'local') {
        // Search function inspired by: https://speedysense.com/filter-html-table-using-javascript/
        // You can search locally on list_with_categories.html, label.html, commits.html
        search_input.addEventListener('input', function () {
            onInputSearch();
        });

    } else {
        search_input.addEventListener('keypress', function (e) {
            if (e.key === 'Enter') {
                window.open("/" + search_location + "/" + "?search_input=" + document.getElementById("search-input").value, "_self");
            }
        });
    }
});