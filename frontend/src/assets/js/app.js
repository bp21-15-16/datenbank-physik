/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (overview.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.less');
require('../css/basic.less');
require('../css/form.less');
require('../css/header.less');
require('../css/button_container.less');
require('../css/preview_card.less');
require('../css/list_entry.less');
require('../css/label.less');
require('../css/tagify.scss');
require('../css/jstree.less');
require('../css/slick.less');       //media-carousel
require('../css/tab.less');
require('../css/sign_selector.less');
require('../css/drop_down_menu.less');
require('../css/themes/default/style.less');
require('../css/editor.less');
require('../css/comments.less');
require('../css/calendar.less');
require('../css/reservation.less');
require('../css/search.less');
require('../css/welcome.less');
require('../css/footer.less');
require('../css/select2.less');
require('../css/messages.less');
require('../css/alternating_list.less');
require('../css/inventory.less');
require('../css/fullpage.less')
require('../css/bookings_list.less');
require('../css/list_with_categories.less');
require('../css/modal.less');
require('../css/device_document.less');
require('../css/error_pages.less');


// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
// create global $ and jQuery variables
global.$ = global.jQuery = $;
require('./editor.js');
require('./update_url_params.js');
require('./alternating_list.js');
require('./inventory');
require('./handle_commits.js');
require('./search_checkboxes.js');
require('./escape_user_html.js');
import 'select2';                       // globally assign select2 fn to $ element
import 'select2/dist/css/select2.css';  // optional if you have css loader
import Tagify from '@yaireo/tagify';
import flatpickr from "flatpickr";
import { German } from "flatpickr/dist/l10n/de.js";
flatpickr.localize(German)
require("flatpickr/dist/themes/material_red.css");
require('./modify_cookies');
require('./drop_down_menu');
require('./search_anywhere');
require('./comment');
require('./inventory');
require('./color_functions');

window.Tagify = Tagify

//enable select2 for better searching inside select stuff
$(() => {
  $('.select2-enable').select2();
});

// a little easter egg: fluttershy from mlp greeting users when opening devtools
console.log(atob("CiAgLycnJycnJycnJycnJycnJycnJycnJycnJydcIAogIHwgTWl0IExpZWJlIGdlbWFjaHQgdm9uOiAgfAogIHwgICAgTS5BLiAgUy5ELiAgICBBLkcuICAgfCAgICAgICAgX18gIF8uLS0nJydgYCctLS4uICAgICAgIAogIHwgTC5LLiAgSy5NLiAgQS5NLiAgIEouTS4gfCAgICAgICAvICBcLyAgICAgICAgICAgICAgYC4gICAgIAogIHwgICAgIFQuTi4gIEEuUy4gIFAuVy4gICAgfCAgICAgICggLyAgXF9fXyAgICAgICAgICAgICBcICAgIAogIFxfX19fX19fX19fX19fX19fX19fX19fX18gIGAuICAgICB8IHwgICAgICBgJy0uICAgIF9fICAgXCAgIAogICAgICAgICAgICAgICAgICAgICBfX18gICBgLS0tYCAgICggJy4gICAgICAgX19gLidgIFxgLiAgIFwgIAogICAgICAgICAgICAgIF9fXyAgICggICBgLiAgICAgICAgIC9cICAgICwuIC4nX19cICAvIGA6X1wgICBcIAogICAgICAgICAgICAgKCAgIGAtLiBgLiAgIGAuICAgICAgLyAgXF8gX18uYCAvIC4tfCB8Jy58PSBcICAgfAogICAgICAgICAgICAgIGAtLiAgIGAtLmAuICAgYC4gICA6ICAgIHJfXywnICggKFdXfCBcVylqICAgfCAgfAogICAgICAgICAgICAgICAgIGAuICAgIGAuXCAgIF9cICB8ICAgIHwgICAgXF9cX2AvICAgYGAtLiAvICAvIAogICAgICAgIC4tLbS0tLRgYC1fYC0sICAgYCAgKCAgfCB8ICAgIHwgICAgICAgICAgICBcX18vLmAgLmAgIAogICAgICAgLyAgICAgICAgICggYGC0ICAgIF9fIFwgIFx8ICAgIFwgICAgLSxfX19fX18uLScvICAvICAgIAogICAgICAvICAgICAgICAgICBgLS5fICAgKCAgYC5cICAnLiAgICBcICAgICAgLycuICAgLicgLicgICAgIAogICAgIC8gICAgICAgICAsLi0tJyc+YC0uIGAtLiBgICAgfCAgICAgYC4gICAoICB8ICAvICAvICAgXyAgIAogICAgKCAgICAgICAgIHwgICAgIC8gOFk4YF86IGBfOi4gfCAgICAgICBcICAgXCB8IGwgICggLDonIFwgIAogICAgfCAgICAgICAgLicgICAgfCAgICAgKCAgICggICAgKCAgICAgICAgXCAgIHxcIHwgICBcICApICApIAogICAgfCAgICAgICAuJyAgICAgfCA4WTggIGAtLS06Ll9fLVwgICAgICAgIFwgIHwgYC4gICAgYGAgLicgIAogICAgfCAgICAgICB8ICAgICAgfCAgICAgOFk4ICAgICAgICBcICAgICAgICBcIGogICBgJy0uLi0nICAgIAogICAgfCAgICAgICAnLiAgICAvIFwgICAgICAgIC8gICAgICAgfCAgICAgICAgfCAgICAgICAgICAgICAgIAogICAgfCAgICAgICAgfC4tLScgICB8ICAgICAgLy0sX19fX19ffCAgICAgICAgfCAgICAgICAgICAgICAgIAogICAgbCAgICAgICAgfCAgICAgXy8gICAgICAvICAgICAuLS4gfCAgICAgICAvIFwgICAgICAgICAgICAgIAogICAgIFwgICAgICAgJy4gICAvICAgICAgIC8gICAgICggKGAgLyAgICAgIC8gICBcICAgICAgICAgICAgIAogIF9fICBcICAgICAgIHwgICB8ICAgICAgfCAgICAgIHxcIGBgICAgIF8uJyAgICAgKSAgICAgICAgICAgIAouJyAvICAgXCAgICAgIHxfXy98ICAgICAgfCAgICAgIHwgYC0uXy4tJyggICAgICAgfCAgICAgICAgICAgIAp8IChfICAgIHwgICAgICB8ICB8ICAgICAgfCAgICAgIHwgICAgICB8ICBcICAgICAgfCAgICAgICAgICAgIAonICAgYCctYCAgICAgICB8ICB8ICAgICAgIFwgICAgIHwgICAgICAgXCAgYC5fX18vICAgICAgICAgICAgIAogYC0uLl9fX19fX19fXy8gICAgXF9fX19fX18pICAgICBcX19fX19fXykgCg=="));
