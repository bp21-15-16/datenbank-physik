import json
import time
import html

import bbcode as bbc
import bs4
import simplediff
from django.template.defaulttags import register

from public.models import Experiment


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


parser = bbc.Parser(replace_links=False, escape_html=True)


def render_img(tag_name, value, options, parent, context):
    size = options['size'] if ('size' in options and options['size'] != '') else "300"
    if value:
        return f'<img width={size} src={value} alt="Fehler! Korrekter Syntax ist: [img width=]https://upload.wikimedia.org/wikipedia/commons/0/0a/The_Great_Wave_off_Kanagawa.jpg[/img]"></img>'
    else:
        return f'Fehler! Korrekter Syntax ist: [img width=]https://upload.wikimedia.org/wikipedia/commons/0/0a/The_Great_Wave_off_Kanagawa.jpg[/img]'


parser.add_formatter('img', render_img, strip=True)


def internal_experiment_hyperlink(tag_name, value, options, parent, context):
    try:
        try:
            experiment_id = int(value)  # try to accept id 611
            experiment = Experiment.objects.get(id=experiment_id)
            experiment_name = experiment.name
            experiment_token = f'{experiment.category.parent.parent.token}.{experiment.category.parent.token}.{experiment.category.token}.{experiment.experiment_number}'
        except ValueError:
            value = value[:-1].lower().split('.') if value[-1] == "." else value.lower().split('.')  # accept both E.F.5.1 and E.F.5.1.
            experiment = Experiment.objects.filter(category__parent__parent__token__iexact=value[0],
                                                   category__parent__token__iexact=value[1],
                                                   category__token__iexact=value[2],
                                                   experiment_number=value[3]).first()  # if there are multiple with the same token, we just take the first
            experiment_id = experiment.id
            experiment_name = experiment.name
            experiment_token = f'{experiment.category.parent.parent.token}.{experiment.category.parent.token}.{experiment.category.token}.{experiment.experiment_number}'

        return f'<a href="/experiment/{experiment_id}">{experiment_token} {experiment_name}</a>'
    except:
        return '<a>Korrekter Syntax ist z.B.: [experiment]E.F.5.1[/experiment] oder [experiment]611[/experiment]</a>'
parser.add_formatter('experiment', internal_experiment_hyperlink, strip=True)


def internal_device_hyperlink(tag_name, value, options, parent, context):
    from public.models import Experiment
    try:
        device_id = int(value)  # devices have no device_number, so we can only uniquely identify them by their id
        device = Experiment.objects.get(id=device_id)
        device_name = device.name
    except ValueError:
        return '<a>Korrekter Syntax ist z.B.: [device]1757[/device]</a>'

    return f'<a href="/device/{device_id}">{device_name}</a>'
parser.add_formatter('device', internal_device_hyperlink, strip=True)


def larger(tag_name, value, options, parent, context):
    return f'<span style="font-size: 1.2em">{value}</span>'
parser.add_formatter('larger', larger, strip=True)


def smaller(tag_name, value, options, parent, context):
    return f'<span style="font-size: 0.75em">{value}</span>'
parser.add_formatter('smaller', smaller, strip=True)


@register.filter
def bbcode(text):
    if (text is not None) and (text != ""):
        return parser.format(text)
    else:
        return "<i>Hier existiert kein Eintrag.</i>"


@register.filter
def create_data_diff(data):
    try:
        decoded_data_0 = str(data[0]).replace('\r\n', '[%] ').replace('\n', '[%] ')
        if decoded_data_0 == 'None' or decoded_data_0 is None:
            decoded_data_0 = ''
        decoded_data_1 = str(data[1]).replace('\r\n', '[%] ').replace('\n', '[%] ')
        if decoded_data_1 == 'None' or decoded_data_1 is None:
            decoded_data_1 = ''
        diff = simplediff.html_diff(decoded_data_0, decoded_data_1)

        soup_old = bs4.BeautifulSoup(diff, 'html.parser')
        for div in soup_old.find_all("ins"):
            div.decompose()

        soup_new = bs4.BeautifulSoup(diff, 'html.parser')
        for div in soup_new.find_all("del"):
            div.decompose()

        data_diff = [str(soup_old).replace('[%]', '\n'), str(soup_new).replace('[%]', '\n')]
    except:
        data_diff = ['Beim Erstellen des Commits ist ein Fehler aufgetreten.', 'Beim Erstellen des Commits ist ein Fehler aufgetreten.']
    return data_diff


@register.filter
def parse_safety_signs(data): # handling of weirdly formatted representation of commits in db. should be removed on deployment
    data[0] = json.loads(html.unescape("[]" if data[0] == "" or data[0] == "None" else data[0]))
    data[1] = json.loads(html.unescape("[]" if data[1] == "" or data[1] == "None" else data[1]))
    if type(data[0]) is list and type(data[1]) is list:
        return data
    elif type(data[0]) is str and type(data[1]) is str:
        return [json.loads(data[0]), json.loads(data[0])]
    elif type(data[0]) is list and type(data[1]) is dict:
        return [data[0], list(data[1].values())]
    else:
        return [[], []]


@register.filter
def parse_labels(data):
    data[0] = json.loads(html.unescape("[]" if data[0] == "" or data[0] == "None" else data[0]))
    data[1] = json.loads(html.unescape("[]" if data[1] == "" or data[1] == "None" else data[1]))
    try:
        data[1] = list(data[1].values())
    except:
        pass

    return data


@register.filter
def parse_devices(data):
    data[0] = json.loads(html.unescape("[]" if data[0] == "" or data[0] == "None" else data[0]))
    data[1] = json.loads(html.unescape("[]" if data[1] == "" or data[1] == "None" else data[1]))

    return data


@register.filter
def parse_tags(data):
    data[0] = json.loads(html.unescape("[]" if data[0] == "" or data[0] == "None" else data[0]))
    data[1] = json.loads(html.unescape("[]" if data[1] == "" or data[1] == "None" else data[1]))
    return data


def get_parser():
    return parser


@register.filter
def loading_time(start_timestamp, view_timestamp):
    template_timestamp = time.time()
    total_time = round(template_timestamp - start_timestamp, 5)
    view_time = round(view_timestamp - start_timestamp, 5)
    return f"Ladezeit: {total_time}s (total), {view_time}s (view only)"


@register.filter
def pending_delete(category):
    return category.pending_delete()