from django.conf import settings
from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


def send_mail(subject, message, email=settings.DEFAULT_RECIPIENT_ADDRESS):
    context = {
        'title': subject,
        'content': message,
    }
    connection = mail.get_connection()
    connection.open()
    html_content = render_to_string('email_template.html', context=context)
    email = EmailMultiAlternatives(
        subject=subject,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=email,
    )
    email.attach_alternative(html_content, "text/html")
    email.send()
    connection.close()


def amount_of_media_to_display(files):
    """
    Returns the amount of images/videos which can normally be displayed by browsers
    """
    counter = 0
    for file in files:
        if ((file['general_type'] == 'image' and file['specific_type'] != 'heic')
            or (file['general_type'] == 'video' and (file['specific_type'] == "mp4" or file['specific_type'] == 'webm'))):
            counter += 1
    return counter
