from django.shortcuts import render


def custom_page_not_found(request, exception):
    context = {'main_title': '404 - Seite nicht gefunden'}
    return render(request, 'base/404.html', context, status=404)


def custom_server_error(request):
    context = {'main_title': '500 - Interner Server Fehler'}
    return render(request, 'base/500.html', context, status=500)
