# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

import mimetypes
import os
import PIL.Image, PIL.ImageOps
PIL.Image.warnings.simplefilter('error', PIL.Image.DecompressionBombWarning)
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import signals
from django.dispatch import receiver
import django.utils.timezone
import labels.models
# TODO add level : in the database there is a field level which is always 1 there!!
from accounts.models import User
from datenbank_physik import settings

from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key

alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', "Es sind nur alphanumerische Zeichen erlaubt.")


class Category(models.Model):
    """DB model for categories"""
    name = models.CharField(max_length=255)
    token = models.CharField(max_length=255, validators=[alphanumeric])
    parent = models.ForeignKey('self', models.CASCADE, blank=True, null=True)
    depth = models.IntegerField(default=0)

    def __str__(self):
        """Returns name of category as a string"""
        return self.name

    def children(self):
        """Returns children of a category"""
        res = Category.objects.filter(parent=self.pk)
        return None if not res else res

    def get_depth(self):
        """Returns depth of category, starting from 0"""
        category = self
        ctr = 0
        while not (category is None or category.parent is None):
            ctr = ctr + 1
            category = category.parent
        return ctr

    def save(self, *args, **kwargs):
        """Automatically calculates depth when saving a category"""
        self.depth = self.get_depth()
        super(Category, self).save(*args, **kwargs)

    def pending_delete(self):
        if self.commit_set.filter(field='!delete', status='not_handled').exists():
            return True
        return False

    class Meta:
        db_table = 'category'


class Label(models.Model):
    """DB model for labels"""
    name = models.CharField(max_length=255)
    color = models.ForeignKey(labels.models.Color, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name

    def get_color(self):
        return self.color.color if self.color else '#b3b3b3'

    class Meta:
        db_table = 'label'


class Comment(models.Model):
    parent_comment = models.ForeignKey('self', models.CASCADE, blank=True, null=True)
    experiment = models.ForeignKey('Experiment', models.CASCADE, related_name='experiment_comment')
    user = models.ForeignKey(User, models.SET_NULL, null=True)
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    COMMENT_TYPES = (
        ("PUBLIC", "Für eingeloggte Nutzer sichtbar"),
        ("VLA", "Interna VLA"),
        ("DEMO", "Interna DEMO"),
    )

    type = models.CharField(max_length=80, choices=COMMENT_TYPES, default="PUBLIC")

    def __str__(self):
        return self.message

    class Meta:
        db_table = 'comment'


class Location(models.Model):
    """DB model for locations"""
    name = models.CharField(max_length=255)
    token = models.CharField(max_length=255, validators=[alphanumeric])
    parent = models.ForeignKey('self', models.CASCADE, blank=True, null=True)
    depth = models.IntegerField(default=0)

    def __str__(self):
        """Returns name of location as a string"""
        return self.name

    def get_depth(self):
        """Returns depth of location, starting from 0"""
        location = self
        ctr = 0
        while not (location is None or location.parent is None):
            ctr = ctr + 1
            location = location.parent
        return ctr

    def save(self, *args, **kwargs):
        """Automatically calculates depth when saving a location"""
        self.depth = self.get_depth()
        super(Location, self).save(*args, **kwargs)

    def children(self):
        """Returns children of a location"""
        res = Location.objects.filter(parent=self.pk)
        return None if not res else res

    def pending_delete(self):
        if self.commit_set.filter(field='!delete', status='not_handled').exists():
            return True
        return False

    class Meta:
        db_table = 'location'


class Device(models.Model):
    """DB model for devices"""
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    inventorynumber = models.IntegerField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    amount = models.IntegerField()
    ordernumber = models.CharField(max_length=255, blank=True, null=True)
    supplier = models.CharField(max_length=255, blank=True, null=True)
    ifpcode = models.CharField(max_length=255, blank=True, null=True)
    loaned = models.IntegerField(default=0)
    repair = models.IntegerField(default=0)
    labels = models.ManyToManyField(Label)
    location = models.ForeignKey(Location, models.SET_NULL, blank=True, null=True)
    tags = models.TextField(blank=True, null=True)

    def __str__(self):
        """Returns name of device as a string"""
        return self.name

    def get_available(self):
        if self.amount > 0:
            return self.amount - (self.loaned + self.repair)
        else:
            return 0

    def add_loan(self, count):
        count = count + self.loaned
        available = self.get_available()
        if (count > available):
            self.loaned = available
        else:
            self.loaned = count
        self.save()

    def remove_loan(self, count):
        count = self.loaned - count
        if (count < 0):
            self.loaned = 0
        else:
            self.loaned = count
        self.save()

    def add_repair(self, count):
        count = count + self.repair
        available = self.get_available()
        if (count > available):
            self.repair = available
        else:
            self.repair = count
        self.save()

    def remove_repair(self, count):
        count = self.repair - count
        if (count < 0):
            self.repair = 0
        else:
            self.repair = count
        self.save()

    class Meta:
        db_table = 'device'


class Experiment(models.Model):
    AVAILABILTY_CHOICES = (
        ('draft', 'Entwurf'),
        ('available', 'Verfügbar'),
        ('notavailable', 'Nicht Verfügbar'),

    )
    category = models.ForeignKey(Category, models.SET_NULL, blank=True, null=True)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    preparation_time = models.IntegerField(blank=True, null=True)
    status = models.CharField(choices=AVAILABILTY_CHOICES, max_length=255)
    execution_time = models.IntegerField(blank=True, null=True)
    safety_signs = models.TextField(db_collation='utf8mb4_bin', blank=True, null=True)
    experiment_number = models.IntegerField()
    labels = models.ManyToManyField(Label)
    tags = models.TextField(blank=True, null=True)
    devices = models.ManyToManyField(Device)
    presented = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'experiment'


class Commit(models.Model):
    author = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='confirmed_by')
    confirmed_by = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    field = models.CharField(max_length=255)
    old_data = models.TextField(blank=True, null=True)
    new_data = models.TextField(blank=True, null=True)
    status = models.TextField(blank=False, null=True)
    timestamp = models.DateTimeField()
    confirmed_at = models.DateTimeField(blank=True, null=True)
    experiment = models.ForeignKey('Experiment', on_delete=models.CASCADE, blank=True, null=True)
    device = models.ForeignKey('Device', on_delete=models.CASCADE, blank=True, null=True)
    location = models.ForeignKey('Location', on_delete=models.CASCADE, blank=True, null=True)
    category = models.ForeignKey('Category', on_delete=models.CASCADE, blank=True, null=True)
    label = models.ForeignKey('Label', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        db_table = 'commit'


class Document(models.Model):
    SECURITY_CHOICES = (
        ('', 'Sicherheitsstufe wählen'),
        (0, 'Öffentlich'),
        (1, 'TU Darmstadt'),
        (2, 'VLA'),
    )

    experiment = models.ForeignKey('Experiment', on_delete=models.CASCADE, blank=True, null=True)
    device = models.ForeignKey(Device, on_delete=models.CASCADE, blank=True, null=True)
    image = models.FileField(upload_to='', blank=True, null=True)
    image_name = models.CharField(max_length=255, blank=True, null=True)
    image_size = models.IntegerField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    typ = models.CharField(max_length=255, blank=False, null=False, default="application/octet-stream")
    name = models.CharField(max_length=255, blank=True, null=True)
    marked = models.BooleanField()
    position = models.IntegerField()
    private = models.IntegerField(choices=SECURITY_CHOICES, default=0)

    def save(self, *args, **kwargs):
        self.name = self.image_name
        mimetypes.init()
        mime = mimetypes.guess_type(self.image_name)[0]
        self.typ = mime if mime else "application/octet-stream"
        # updated at
        self.updated_at = django.utils.timezone.now()
        super(Document, self).save(*args, **kwargs)
        #get correct file name
        if self.image:
            #try to create smaller preview pictures for device_overview
            try:
                low_img_name = self.image.name
                if not os.path.exists(os.path.join(settings.MEDIA_ROOT, 'lowres', low_img_name)):
                    low_img = PIL.Image.open(self.image)
                    try:
                        low_img = PIL.ImageOps.exif_transpose(low_img)
                    except:
                        pass
                    low_img.thumbnail((1280, 480))
                    low_img.save(os.path.join(settings.MEDIA_ROOT, 'lowres', low_img_name))
            except (ValueError, FileNotFoundError, PIL.UnidentifiedImageError) as e:
                pass
            except Exception as e:
                print(e)
                pass

            self.image_name = self.image.name
            super(Document, self).save(*args, **kwargs)

    class Meta:
        db_table = 'document'

    def __str__(self):
        return self.name if self.name != None else "not named document"


@receiver(signals.post_delete, sender=Document)
def delete_documents_from_disk(sender, instance, **kwargs):
    if instance.image_name:
        try:
            os.remove(os.path.join(settings.MEDIA_ROOT, instance.image_name))
        except FileNotFoundError as e:
            pass
        try:
            os.remove(os.path.join(settings.MEDIA_ROOT, 'lowres', instance.image_name))
        except FileNotFoundError as e:
            pass


class Reservation(models.Model):
    user = models.ForeignKey(User, models.DO_NOTHING)
    ordertime = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField()
    lecture_date = models.DateTimeField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'reservation'


class ReservationExperiment(models.Model):
    reservation = models.ForeignKey(Reservation, models.DO_NOTHING, blank=True, null=True)
    experiment = models.ForeignKey(Experiment, models.DO_NOTHING)
    status = models.IntegerField()
    # user = models.ForeignKey('accounts.User', models.DO_NOTHING)
    pinned_on = models.DateTimeField()

    class Meta:
        db_table = 'reservation_experiment'


class EditableContent(models.Model):
    location = models.TextField(blank=False, null=True)
    content = models.TextField(blank=False, null=True)


@receiver([signals.post_save, signals.post_delete], sender=Commit)
@receiver([signals.post_save, signals.post_delete], sender=User)
@receiver([signals.post_save, signals.post_delete], sender=Experiment)
@receiver([signals.post_save, signals.post_delete], sender=Device)
@receiver([signals.post_save, signals.post_delete], sender=Location)
@receiver([signals.post_save, signals.post_delete], sender=Label)
def clear_commit_cache(sender, instance, **kwargs):
    if sender == Commit:
        cache.delete(make_template_fragment_key('actual_commit', [instance.id]))
        #cache.delete(f'commit_dict-{instance.id}') # see handle_commit() in commits/views.py
    else:
        if sender == User:
            caches_to_delete = Commit.objects.filter(models.Q(author=instance) | models.Q(confirmed_by=instance))
        elif sender == Experiment:
            caches_to_delete = Commit.objects.filter(experiment=instance)
        elif sender == Device:
            caches_to_delete = Commit.objects.filter(device=instance)
        elif sender == Location:
            caches_to_delete = Commit.objects.filter(location=instance)
        elif sender == Label:
            caches_to_delete = Commit.objects.filter(label=instance)
        else:
            caches_to_delete = []
            print("supposedly unreachable")
        for element in caches_to_delete:
            cache.delete(make_template_fragment_key('actual_commit', [element.id]))
            #cache.delete(f'commit_dict-{element.id}')
