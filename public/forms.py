from django import forms
from public.models import EditableContent

class PresentedForm(forms.Form):
    experiment = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': "experiment_select",
                'id': "experiment_id",
                'placeholder': "Versuche auswählen..."
            }
        ),
        label="Präsentierte Versuche",
    )


class EditableContentForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for e in EditableContent.objects.all():
            field_name = e.location
            self.fields[field_name] = forms.CharField(required=True)
            self.initial[field_name] = e.content

    def save(self):
        for e in EditableContent.objects.all():
            e.content = self.data[e.location]
            e.save()
