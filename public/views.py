import json

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.html import escape

import experiment_list.forms
from accounts.decorators import *
from . import forms
from .models import Comment, EditableContent
from .utilities import *


def welcome(request):
    main_title = "Willkommen!"
    current_user = request.user
    form = None
    experiment_list = None
    if current_user.is_authenticated:
        main_title = f"Willkommen {current_user.username}!"
        if current_user.hasGroup("Schreiben - Versuche"):
            experiment_list = getExperiments(Experiment.objects.filter(status='available'))
            presented = getExperiments(Experiment.objects.filter(presented=True))
            presented = json.dumps(
                [dict(x, clean_name=escape(x["name"]), value=x["id"]) for x in presented])
            form = forms.PresentedForm()
            form.fields['experiment'].initial = presented

    welcometext = EditableContent.objects.get(location="Willkommenstext").content if EditableContent.objects.filter(location="Willkommenstext").exists() else ""
    helptab_guests = EditableContent.objects.get(location="Hilfetab-Gäste").content if EditableContent.objects.filter(location="Hilfetab-Gäste").exists() else ""
    helptab_lecturer = EditableContent.objects.get(location="Hilfetab-Dozenten").content if EditableContent.objects.filter(location="Hilfetab-Dozenten").exists() else ""
    helptab_faq = EditableContent.objects.get(location="Hilfetab-FAQ").content if EditableContent.objects.filter(location="Hilfetab-FAQ").exists() else ""

    context = {
        'form': form,
        'main_title': main_title,
        'experiment_list': experiment_list,
        'presented_list': getPresented(),
        'welcometext': welcometext,
        'helptab_guests': helptab_guests,
        'helptab_lecturer': helptab_lecturer,
        'helptab_faq': helptab_faq,
    }
    return render(request, 'welcome/welcome.html', context)


@login_required(login_url='login')
@allowed_users(['Schreiben - Versuche'])
def change_presented(request):
    if request.method == 'POST':
        experiments = request.POST.get('presented')
        Experiment.objects.all().update(presented=False)
        if experiments != '':
            experiments = json.loads(experiments)
            for e in experiments:
                current_experiment = Experiment.objects.get(id=e.get('value'))
                current_experiment.presented = True
                current_experiment.save()

        context = {
            'presented_list': getPresented(),
        }
        return render(request, 'welcome/slideshow.html', context=context)


def contact(request):
    main_title = 'Kontakt'
    return render(request, 'welcome/contact.html', {'main_title': main_title})


def impressum(request):
    imprint_text = EditableContent.objects.get(location="Impressum").content if EditableContent.objects.filter(location="Impressum").exists() else ""
    main_title = 'Impressum'
    return render(request, 'welcome/impressum.html', {'main_title': main_title, "imprint_text": imprint_text})

def datenschutz(request):
    main_title = 'Datenschutz'
    return render(request, 'welcome/datenschutz.html', {'main_title' :main_title})


@login_required(login_url='login')
@allowed_users(allowed_roles=['Sicherheitsstufe - VLA'])
def update_editable_content(request):
    if request.method == 'POST':
        form = forms.EditableContentForm(request.POST)
        form.save()
    form = forms.EditableContentForm()
    context = {"form": form,
               "main_title": "Inhalte Bearbeiten"}
    return render(request, 'forms/edit_content.html', context=context)


@login_required(login_url='login')
def bbcode_preview(request):
    if request.method == 'POST':
        content = request.POST.get('content')
        return render(request, 'forms/bbcode_preview.html', {'content': content})


@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Versuche - Tab öffentliche Kommentare',
                              'Lesen - Versuche - Tab Interna Demo',
                              'Lesen - Versuche - Tab Interna VLA'])
def comments(request):
    user = request.user

    form = None
    comments_public = None
    comments_demo = None
    comments_vla = None
    comment_choices = []

    if user.hasGroup('Lesen - Versuche - Tab öffentliche Kommentare'):
        comment_choices.append(('PUBLIC', "Für eingeloggte Nutzer sichtbar"))
        comments_public = Comment.objects.filter(type='PUBLIC').order_by('-timestamp')
    if user.hasGroup('Lesen - Versuche - Tab Interna Demo'):
        comment_choices.append(('DEMO', "Interna Demo"))
        comments_demo = Comment.objects.filter(type='DEMO').order_by('-timestamp')
    if user.hasGroup('Lesen - Versuche - Tab Interna VLA'):
        comment_choices.append(('VLA', "Interna VLA"))
        comments_vla = Comment.objects.filter(type='VLA').order_by('-timestamp')

    if comment_choices:
        form = experiment_list.forms.commentForm()
        form.fields['type'].choices = comment_choices

    context = {
        'comments_public': comments_public,
        'comments_demo': comments_demo,
        'comments_vla': comments_vla,
        'comment_choices': comment_choices,
        'form': form,
    }
    return render(request, 'comments/overview.html', context)
