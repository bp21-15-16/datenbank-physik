import os

from django import template
from django.template import Template, Context

from devices.forms import DocumentForm, DocumentFormSet
from experiment_list.forms import DocumentFormExp, DocumentFormSetExp
from ..models import Label, Document, Device, Experiment

register = template.Library()


@register.simple_tag
def show_document(document_form, user):
    # choices are
    # SECURITY_CHOICES = (
    #     ('', 'Sichheitstufe wählen'),
    #     (0, 'TU Darmstadt'),
    #     (1, 'VLA'),
    #     (2, 'Öffentlich')
    # )
    return True


@register.simple_tag
def define(val=None):
    return val


@register.simple_tag
def concat_all(*args):
    """concatenate all args"""
    return ''.join(map(str, args))


@register.simple_tag
def get_children(cur, categories):
    res = [item for item in categories if item["parent_id"] == cur["id"]]
    return None if len(res) == 0 else res


@register.simple_tag
def labels():
    return Label.objects.all()


@register.simple_tag
def set(val=None):
    return val


@register.simple_tag
def getDocForm():
    from django.forms import inlineformset_factory
    f = inlineformset_factory(Device,
                              Document,
                              form=DocumentForm,
                              extra=1,
                              max_num=10,
                              absolute_max=10,
                              can_delete=True
                              )
    s = f()
    #  i create a formset here because i created a form i get in all fields double name values
    #  to avoid repeated name of widgets i change name values with java script
    #  it's a way around but not the best solution
    return s.forms[0]


@register.simple_tag
def getDocFormExp():
    from django.forms import inlineformset_factory
    f = inlineformset_factory(Experiment,
                              Document,
                              form=DocumentFormExp,
                              extra=1,
                              max_num=10,
                              absolute_max=10,
                              can_delete=True
                              )
    s = f()
    #  i create a formset here because i created a form i get in all fields double name values
    #  to avoid repeated name of widgets i change name values with java script
    #  it's a way around but not the best solution
    return s.forms[0]


@register.simple_tag
def getDocFormSet():
    return DocumentFormSet


@register.simple_tag
def getDocFormSetExp():
    return DocumentFormSetExp


@register.simple_tag
def getDocMaxID():
    if len(Document.objects.all()) == 0:
        return 0
    return Document.objects.latest('id').id + 1


@register.simple_tag
def toSigleQuotes(txt):
    return txt.replace("\"", "\'")


@register.simple_tag
def getCounter():
    context = Context({})
    temp = Template("{{ forloop.counter0 }}")
    temp = temp.render(context)
    return temp.replace("\"", "\'")


@register.simple_tag
def getDeviceDocumentFormTemplate(device):
    context = Context({})
    context["device"] = device
    context["tempForm"] = DocumentForm()
    temp = Template("{% include 'forms/documentForm.html' with device=device%}")
    temp = temp.render(context)
    temp = temp.replace("\"", "\'")
    temp = temp.replace("onclick='hideDeleteOn('device_documents___name__', '__name__' );'",
                        "onclick=\"hideDeleteOn('device_documents___name__', '__name__' );\"")
    # print(temp)
    return temp

@register.simple_tag
def getExperimentDocumentFormTemplate(experiment):
    context = Context({})
    context["experiment"] = experiment
    context["tempForm"] = DocumentFormExp()
    temp = Template("{% include 'forms/documentFormExperiment.html' with experiment=experiment%}")
    temp = temp.render(context)
    temp = temp.replace("\"", "\'")
    temp = temp.replace("onclick='hideDeleteOn('experiment_documents___name__', '__name__' );'",
                        "onclick=\"hideDeleteOn('experiment_documents___name__', '__name__' );\"")
    # print(temp)
    return temp