from django.urls import path, re_path
from . import views


urlpatterns = [
    path('', views.welcome),
    path('Impressum', views.impressum, name="impressum"),
    path('Datenschutz', views.datenschutz, name="datenschutz"),
    path('bbcode_preview/', views.bbcode_preview, name="bbcode_preview"),
    path('comments/', views.comments, name="comments"),
    path('contact/', views.contact, name="kontakt"),
    path('presented/', views.change_presented, name="presented"),
    path('edit_content/', views.update_editable_content, name="edit_content"),
    ]
