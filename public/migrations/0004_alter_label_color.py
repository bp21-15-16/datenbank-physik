# Generated by Django 4.0.3 on 2022-03-29 19:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('labels', '0001_initial'),
        ('public', '0003_alter_label_color'),
    ]

    operations = [
        migrations.AlterField(
            model_name='label',
            name='color',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='labels.color'),
        ),
    ]
