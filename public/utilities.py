from .models import Experiment

def commit_category(request, form):
    category = form.save(commit=False)
    category.save()


## for nested lists
## TODO parse the tree to json tree instead of recursivly calling the html template
def _filter(_d):
    return {a: b for a, b in _d.items() if a != 'parent_id'}


def group_vals(_d, _start=None):
    return [_filter({**i, 'children': group_vals(_d, i['id'])})
            for i in _d if i['parent_id'] == _start]


res = ""


def result():
    global res
    from pprint import pprint as pp
    print(res)
    return res


def dict2tree(lst):
    global res
    for i in lst:

        start = "<li data-jstree=\"{{\"opened\":false,\"selected\":false}}\" id={}>  {} : {}" \
            .format(i["id"], i["token"], i["name"])
        end = "</li>"

        if len(i["children"]) != 0:
            # print(res)
            res = res + start + "<ul>"
            dict2tree(i["children"])
            res = res + "</ul>" + end

        else:
            res = res + start + end

def get_whole_token(object):
    token = object.token
    while object.parent is not None:
        object = object.parent
        token =  object.token + "." + token
    return token


def getPresented():
    presented_list = Experiment.objects.filter(presented=True)
    presented_list = [
        (x, x.document_set.filter(marked=1, typ__startswith='image').first().image_name
        if x.document_set.filter(marked=1, typ__startswith='image').exists() else None)
        for x in presented_list]
    return presented_list

def getExperiments(query):
    experiment_query = list(query.select_related('category', 'category__parent', 'category__parent__parent'))
    exp_choices = []
    for e in experiment_query:
        token = "" if e.category is None else get_whole_token(e.category)
        exp_choices.append({'id': e.id, 'name': f'{token}.{e.experiment_number} {e.name}'})
    return sorted(exp_choices, key=lambda d: d['name'])

def getExperimentsPrepTime(query):
    experiment_query = list(query.select_related('category'))
    exp_choices = []
    for e in experiment_query:
        exp_choices.append({'id': e.id, 'preparation_time': e.preparation_time if e.preparation_time is not None else 1440})
    return exp_choices
