from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.inventory_showAll, name="inventory"),
    path('create/', views.inventory_create, name="inventory_create"),
    re_path(r'^(?P<inventoryid>\d+)/view/$', views.inventory_view, name="inventory_view"),
    re_path(r'^(?P<inventoryid>\d+)/end/$', views.inventory_end, name="inventory_end"),
    re_path(r'^(?P<inventoryid>\d+)/delete/$', views.inventory_delete, name="inventory_delete"),
    path('<int:inventoryid>/remove/<int:deviceid>', views.inventory_remove_device, name="inventory_remove_device"),

    # AJAX
    path('change_device/', views.inventory_change_device, name="inventory_change_device"),
    path('fullpage/', views.fullpage_inventory, name="fullpage_inventory"),

    path('loan/', views.loan_showAll, name="loan"),
    path('loan/create/', views.loan_create, name="loan_create"),
    re_path(r'^loan/(?P<loanid>\d+)/return/$', views.loan_return, name="loan_return"),
    re_path(r'^loan/(?P<loanid>\d+)/view/$', views.loan_view, name="loan_view"),
    re_path(r'^loan/(?P<loanid>\d+)/delete/$', views.loan_delete, name="loan_delete"),
]