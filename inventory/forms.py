from django import forms

from public.models import Device
from .models import Inventory, InventoryItem, Loan, LoanItem


def getDeviceToken():
    """Appends device id to device name and returns formatted list of device names"""
    device_list = list(Device.objects.all().values_list('id', 'name'))
    device_list = [list(x) for x in device_list]
    for d in device_list:
        d[1] = str(d[0]) + " " + d[1]
    device_list.append([0, 'Gerät auswählen'])
    device_list.sort(key=lambda x: x[0])
    return device_list


class CreateInventoryForm(forms.ModelForm):
    """Form for creating a new inventory"""
    class Meta:
        model = Inventory
        fields = ('name',)


class EditInventoryForm(forms.ModelForm):
    """Form for editing an existing inventory"""
    name = forms.CharField(
        label = "Bemerkung",
        widget=forms.TextInput(
            attrs={
                   'placeholder': "Nutzername..."
            }
        )
    )

    items = forms.ModelMultipleChoiceField(
        label="Inventierte Geräte",
        queryset=InventoryItem.objects.all(),
        widget=forms.SelectMultiple()
    )

    class Meta:
        model = Inventory
        fields = ('name', 'items')


class CreateLoanForm(forms.ModelForm):
    enddate = forms.DateTimeField(
        label="Rückgabe",
        widget=forms.DateTimeInput(
            attrs={
                'type': 'date',
            }
        ),
    )

    loaned_to = forms.CharField(
        label="Verliehen an",
        widget=forms.TextInput(
            attrs={
                'placeholder': "Verliehen an...",
            }
        )
    )

    comment = forms.CharField(
        label="Zusatzinformation",
        required=False,
        widget=forms.Textarea(
            attrs={
                'placeholder': "Zusatzinformation...",
            }
        ),
    )

    type = forms.ChoiceField(
        choices=Loan.LOAN_CHOICES,
        label="Typ",
        widget=forms.Select(
            attrs={
                'onChange': "switchType(this);",
            }
        ),
        initial='loan',
    )

    info = forms.ImageField(
        required=False,
        label="Bild",
        widget=forms.FileInput(
        )
    )

    field_order = ['type', 'loaned_to', 'enddate', 'comment', 'info']

    class Meta:
        model = Loan
        fields = ('loaned_to', 'type', 'comment', 'enddate', 'info',)


class LoanReturnForm(forms.ModelForm):



    class Meta:
        model = Loan
        fields = ('comment',)


class LoanItemForm(forms.Form):
    item = forms.ChoiceField(
        choices=getDeviceToken(),
        # empty_label="Gerät auswählen",
        widget=forms.Select(
            attrs={
                'class': "device_select",
                'placeholder': "Geräte auswählen..."
            }
        ),
    )

    amount = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                'min': 1,
                'max': 100,
            }
        ),
        disabled=True,
    )

    comment = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Anmerkung...",
                'id': "loanitem_comment",
            }
        ),
        disabled=True,
    )