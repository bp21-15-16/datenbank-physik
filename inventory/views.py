import json
import locale

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string

from accounts.decorators import *
from devices.views import get_administration
from . import forms
from .models import *
from .utilities import *

locale.setlocale(locale.LC_TIME, locale.normalize(''))
User = get_user_model()


@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Inventur'], url='devices')
def inventory_showAll(request):
    """Shows the active and all completed inventories"""
    current_user = request.user
    active_inventories = Inventory.objects.filter(active=True)
    inactive_inventories = Inventory.objects.filter(active=False)

    active_buttons = [["Anzeigen", 'blue', 'inventory_view', 'icons/ic_eye_24px.svg']]
    inactive_buttons = [["Anzeigen", 'blue', 'inventory_view', 'icons/ic_eye_24px.svg']]

    if current_user.hasGroup("Schreiben - Inventur"):
        active_buttons.append(["Beenden", 'green', 'inventory_end', 'icons/x.svg'])
        inactive_buttons.append(["Löschen", 'red', 'inventory_delete', 'icons/x.svg'])

    if active_inventories:
        messages.warning(request, "Es kann immer nur eine Inventur aktiv sein.")

    context = {
        'active_inventories': active_inventories,
        'inactive_inventories': inactive_inventories,
        'main_title': "Inventur",
        'active_buttons': active_buttons,
        'inactive_buttons': inactive_buttons,
        'already_active': get_activeInventory(),
    }

    return render(request, 'inventory/overview/inventory_list.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Inventur'], url='inventory')
def inventory_create(request):
    """Creating a new inventory"""
    if get_activeInventory() is None:
        current_user = User.objects.get(id=request.user.id)

        form = forms.CreateInventoryForm()
        if request.method == 'POST':
            form = forms.CreateInventoryForm(request.POST)
            if form.is_valid():
                inventory = form.save()
                inventory.start(current_user)
                name = inventory.name if inventory.name is not None else ""
                messages.success(request, f"Inventur {name} gestartet.")

                return redirect('inventory')

        context = {
            'form': form,
        }

        return render(request, 'forms/default.html', context)
    else:
        return redirect('inventory')


@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Inventur'], url='devices')
def inventory_view(request, inventoryid):
    """Detailed view of one inventory"""
    try:
        current_inventory = Inventory.objects.get(id=inventoryid)
    except ObjectDoesNotExist as e:
        raise Http404

    hits = current_inventory.inventoryitem_set.values_list('item', flat=True).distinct()
    hits = Device.objects.filter(id__in=hits)
    misses = Device.objects.all().exclude(id__in=hits)

    context = {
        'inventory': current_inventory,
        'misses': misses,
        'main_title': f"Inventur vom {current_inventory.startdate.strftime('%d.%m.%Y')}",
        'page_buttons': [["Übersicht - Inventur", 'inventory']],
    }

    return render(request, 'inventory/inventory.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Inventur'], url='inventory')
def inventory_end(request, inventoryid):
    """Ending an active inventory"""
    try:
        i = Inventory.objects.get(id=inventoryid)
        name = i.name if i.name is not None else ""
        i.end()
        messages.success(request, f"Inventur {name} erfolgreich beendet.")
    except Inventory.DoesNotExist:
        messages.error(request, "Die angeforderte Inventur existiert nicht.")
        return redirect('inventory')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('inventory')

    return redirect('inventory')


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Inventur'], url='inventory')
def inventory_delete(request, inventoryid):
    """Deleting an inactive inventory"""
    try:
        i = Inventory.objects.get(id=inventoryid)
        name = i.name if i.name is not None else ""
        i.delete()
        messages.success(request, f"Inventur {name} erfolgreich gelöscht.")
    except Inventory.DoesNotExist:
        messages.error(request, "Die angeforderte Inventur existiert nicht.")
        return redirect('inventory')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('inventory')

    return redirect('inventory')


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Inventur'], url='devices')
def fullpage_inventory(request):
    """Tab inventory in device fullpage"""
    if request.method == 'POST':
        already_in_inventory = request.POST.get('already_in_inventory')
        device_id = request.POST.get('device_id')
        info = request.POST.get('text')
        inventory = get_activeInventory()

        device = Device.objects.get(id=device_id)

        if already_in_inventory == "true":
            inventory.remove(device)
        else:
            inventory.inventoryitem_set.add(InventoryItem(item=device, info=info), bulk=False)

        context = {
            'base_info': {
              'id': device_id,
            },
            'tabs': {
                'administration': {
                    'content': get_administration(device),
                },
            }
        }

        return render(request, 'fullpage/inventory.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Inventur'], url='inventory')
def inventory_change_device(request):
    """Adds a device to an inventory or removes the device from an inventory, if it was already catalogued"""
    if request.method == 'POST':
        device_id = request.POST.get('device_id')
        already_in_inventory = request.POST.get('inventory')
        inventory = get_activeInventory()

        device = Device.objects.get(id=device_id)

        if already_in_inventory == "true":
            inventory.remove(device)
        else:
            inventory.add(device)

        return HttpResponse(status=204)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Inventur'], url='inventory')
def inventory_remove_device(request, inventoryid, deviceid):
    """Remove an device from an active inventory in the inventory view"""
    try:
        d = Device.objects.get(id=deviceid)
        i = Inventory.objects.get(id=inventoryid)
        i.remove(d)
    except Device.DoesNotExist:
        messages.error(request, "Das angeforderte Gerät existiert nicht.")
        return redirect('inventory_view', inventoryid=inventoryid)
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('inventory_view', inventoryid=inventoryid)

    messages.success(request, f"Gerät erfolgreich entfernt.")
    return redirect('inventory_view', inventoryid=inventoryid)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Verleih'], url='devices')
def loan_showAll(request):
    current_user = request.user

    active_loans = Loan.objects.filter(active=True).order_by('startdate')
    inactive_loans = Loan.objects.filter(active=False)

    active_buttons = [["Anzeigen", 'blue', 'loan_view', 'icons/ic_eye_24px.svg']]
    inactive_buttons = [["Anzeigen", 'blue', 'loan_view', 'icons/ic_eye_24px.svg']]
    page_buttons = []

    if current_user.hasGroup("Schreiben - Verleih"):
        active_buttons.append(["Rückgabe", 'green', 'loan_return', 'icons/ic_publish_24px.svg'])
        inactive_buttons.append(["Löschen", 'red', 'loan_delete', 'icons/x.svg'])
        page_buttons.append(["Verleih/Reparatur hinzufügen", 'loan_create', 'icons/ic_add_24px.svg'])

    due = False

    for l in active_loans:
        if l.is_due(): due = True
    if due:
        messages.error(request, "Rot markierte Verleihelemente sind überfällig.")

    context = {
        'active_loans': active_loans,
        'inactive_loans': inactive_loans,
        'main_title': "Verleih und Reparatur",
        'active_buttons': active_buttons,
        'inactive_buttons': inactive_buttons,
        'page_buttons': page_buttons,
    }

    return render(request, 'inventory/overview/loan_list.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Verleih'], url='devices')
def loan_view(request, loanid):
    try:
        current_loan = Loan.objects.get(id=loanid)
    except ObjectDoesNotExist as e:
        raise Http404

    if current_loan.type == 'loan':
        title = f"Verleih vom {current_loan.startdate.strftime('%d.%m.%Y')} an {current_loan.loaned_to}"
    else:
        title = f"Reparatur vom {current_loan.startdate.strftime('%d.%m.%Y')}"

    context = {
        'loan': current_loan,
        'current_time': timezone.now(),
        'main_title': title,
        'page_buttons': [["Übersicht - Verleih", 'loan']],
    }

    return render(request, 'inventory/loan.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Verleih'], url='loan')
def loan_return(request, loanid):
    try:
        l = Loan.objects.get(id=loanid)
        l.end_loan()
    except Loan.DoesNotExist:
        messages.error(request, "Der angeforderte Verleih existiert nicht.")
        return redirect('loan')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('loan')

    messages.success(request, f"Rückgabe abgeschlossen.")
    return redirect('loan')


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Verleih'], url='loan')
def loan_delete(request, loanid):
    try:
        l = Loan.objects.get(id=loanid)
        l.delete()
        messages.success(request, f"Element erfolgreich gelöscht.")
    except Loan.DoesNotExist:
        messages.error(request, "Das angeforderte Element existiert nicht.")
        return redirect('loan')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('loan')

    return redirect('loan')


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Verleih'], url='loan')
def loan_create(request):
    current_user = User.objects.get(id=request.user.id)

    loanform = forms.LoanItemForm()
    loanform.fields['item'].choices = forms.getDeviceToken()
    form = forms.CreateLoanForm()
    if request.method == 'POST':
        form = forms.CreateLoanForm(request.POST, request.FILES)
        devices = request.POST.get('devices')
        devices = json.loads(devices)
        if form.is_valid():
            loan = form.save()

            for item in devices:
                id = int(item['id'])
                amount = int(item['amount'])
                comment = item['comment']
                if comment == '':
                    comment = None
                loan.add_device(id, amount, comment)

            loan.start(current_user)

            return redirect('loan')

    context = {
        'form': form,
        'loanform': loanform,
        'available': getAvailable(),
        'loanitem': render_to_string('inventory/loanitem.html'),
        'page_buttons': [["Übersicht - Verleih", 'loan']],
    }

    return render(request, 'inventory/loan_form.html', context)
