from inventory.models import Inventory
from public.models import Device


def get_activeInventory():
    """Returns the active inventory and None if there isn't an active inventory"""
    inventory = Inventory.objects.filter(active=True)
    if inventory.exists():
        inventory = inventory.order_by('-startdate')[0]

    return inventory if inventory else None


def getAvailable():
    """
    Returns list where the first element is an device id and the second element is the available amount of this device
    """
    device_list = Device.objects.all()
    encoded = []
    for d in device_list:
        encoded.append([d.id, d.get_available()])
    return encoded
