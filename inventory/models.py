import os

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.db import models
from django.db.models import signals
from django.dispatch import receiver
from django.http import request
from django.utils import timezone

from public.models import Device, Location, Document

User = get_user_model()


class Loan(models.Model):
    """DB model for loans"""
    startdate = models.DateTimeField(auto_now_add=True)
    enddate = models.DateTimeField(null=True, blank=True)
    loaned_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    loaned_to = models.CharField(max_length=255, blank=True, null=True)
    active = models.BooleanField(default=False)

    LOAN_CHOICES = (
        ('loan', "Verleih"),
        ('repair', "E-Werkstatt"),
    )
    type = models.CharField(max_length=255, choices=LOAN_CHOICES)
    comment = models.TextField(blank=True, null=True)
    info = models.FileField(upload_to='', blank=True, null=True)

    def __str__(self):
        return self.comment

    def delete(self, *args, **kwargs):
        if self.info:
            try:
                os.remove(os.path.join(settings.MEDIA_ROOT, self.info.name))
            except Exception as e:
                messages.add_message(request, messages.ERROR, e)

        super(Loan, self).delete(*args, **kwargs)

    def add_device(self, id, amount, comment):
        device = Device.objects.get(id=id)
        if self.type == 'loan':
            device.add_loan(amount)
        else:
            device.add_repair(amount)
        self.loanitem_set.add(LoanItem(item=device, amount=amount, comment=comment), bulk=False)


    def start(self, user):
        self.active = True
        self.loaned_by = user
        self.save()

    def end_loan(self):
        self.active = False
        for l in self.loanitem_set.all():
            if self.type == 'loan':
                l.item.remove_loan(l.amount)
            else:
                l.item.remove_repair(l.amount)
        self.enddate = timezone.now()
        self.save()

    def is_due(self):
        if self.type == 'loan':
            return self.enddate <= timezone.now()
        return False


class LoanItem(models.Model):
    """DB model for loan instances"""
    item = models.ForeignKey(Device, on_delete=models.CASCADE, null=True)
    amount = models.IntegerField()
    comment = models.CharField(max_length=255, blank=True, null=True)
    loan = models.ForeignKey(Loan, on_delete=models.CASCADE)

    def __str__(self):
        return self.item.name


class Inventory(models.Model):
    """DB model for inventories"""
    name = models.CharField(max_length=255, blank=True, null=True)
    startdate = models.DateTimeField(auto_now_add=True)
    enddate = models.DateTimeField(null=True, blank=True)
    active = models.BooleanField(default=False)
    started_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        """Returns inventory name as a string"""
        return self.name

    def start(self, user):
        """Starting an inventory"""
        self.active = True
        self.started_by = user
        self.save()

    def end(self):
        """Ending an inventory"""
        self.active = False
        self.enddate = timezone.now()
        self.save()

    def add(self, device):
        """Adding a device to an active inventory"""
        item = self.inventoryitem_set.filter(item=device)
        if not item.exists():
            self.inventoryitem_set.add(InventoryItem(item=device), bulk=False)

    def remove(self, device):
        """Removing a device from an active inventory"""
        items = self.inventoryitem_set.filter(item=device).delete()

    def has_device(self, device):
        """Returns true if the device is catalogued in this inventory"""
        if self.inventoryitem_set.filter(item=device.id):
            return True
        else:
            return False

    def clear(self):
        """Removes all catalogued devices from this inventory"""
        self.inventoryitem_set.all().delete()


class InventoryItem(models.Model):
    """DB model for an inventory instance"""
    item = models.ForeignKey(Device, on_delete=models.CASCADE, null=True)
    info = models.TextField(blank=True, null=True)
    inventory = models.ForeignKey(Inventory, on_delete=models.CASCADE)

    def __str__(self):
        """Returns device name as a string"""
        return self.item.name

