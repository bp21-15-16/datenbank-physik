Update Python Requirements:

1.1 select venv as source
source /home/g15_16/venvs/datenbank-physik/bin/activate

1.2 change directory to /home/g15_16/datenbank-physik

1.3 install python requirements:
pip install -r requirements.txt


Update Node-Modules:

1.1 change directory to /home/g15_16/datenbank-physik/frontend

1.2a update node-modules:
npm audit fix

1.2b update all node-modules ignores backwarts-compatibility (could introduce breaking changes):
npm audit fix --force

1.3 update webpack:
npm run build

1.4 move new webpack statics to right static folder (check that venv is still active else activate it, see above):
cd .. (you now should be in /home/g15_16/datenbank-physik/)
python manage.py collectstatic --noinput
sudo systemctl restart apache2

All should be Up2Date now!