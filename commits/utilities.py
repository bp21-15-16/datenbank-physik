from django.template.defaulttags import register
from django.utils.html import escape
from django.core.cache import cache

@register.filter
def translate_to_nominativ(identifier):
    try:
        name = {"experiment": "Der <b>Versuch</b>",
                "device": "Das <b>Gerät</b>",
                "description": "Die <b>Info</b>",
                "comment": "Die <b>Beschreibung</b>",
                "name": "Der <b>Name</b>",
                "execution_time": "Die <b>Durchführungsdauer</b>",
                "preparation_time": "Die <b>Vorbereitungsdauer</b>",
                "category": "Die <b>Kategorie</b>",
                "experiment_number": "<b>Versuchnummer</b>",
                "amount": "Die <b>Anzahl</b>",
                "ifpcode": "Der <b>IFP-Code</b>",
                "inventorynumber": "Die <b>Inventarnumme</b>r",
                "labels": "Die <b>Label</b>",
                "loanedTo": "Der <b>Verliehenenstatus</b>",
                "repair": "Der <b>Reparaturstatus</b>",
                "safety_signs": "Die <b>Sicherheitszeichen</b>",
                "status": "Der <b>Status</b>",
                "supplier": "Der <b>Hersteller</b>",
                "ordernumber": "Die <b>Bestellnummer</b>",
                "tags": "Die <b>Schlagworte</b>",
                "location": "Der <b>Standort</b>",
                "devices": "Die <b>Geräte</b>",
                }[identifier]
    except KeyError as e:
        name = "fatalerror"
    return name


@register.filter
def translate_to_genitiv(identifier):
    try:
        name = {"experiment": "des <b>Versuches</b>",
                "device": "des <b>Gerätes</b>",
                "location": "des <b>Standortes</b>",
                "category": "der <b>Kategorie</b>",
                }[identifier]
    except KeyError as e:
        name = "fatalerror"
    return name


def get_translations(identifier):
    try:
        name = {"!create": "create",
                "!delete": "delete",
                "!migration": "migration",
                "amount": "amount anzahl",
                "category": "category kategorie",
                "comment": "comment beschreibung",
                "description": "description info",
                "execution_time": "executiontime durchführungsdauer",
                "experiment_number": "experimentnumber versuchnummer versuchsnummer",
                "ifpcode": "ifpcode",
                "inventorynumber": "inventorynumber inventarnummer",
                "labels": "labels",
                "loanedTo": "loanedto verliehenenstatus",
                "name": "versuchname gerätname kategoriename",
                "ordernumber": "ordernumber bestellnummer",
                "preparation_time": "preparationtime vorbereitungsdauer",
                "repair": "repair reparaturstatus",
                "safety_signs": "safetysigns sicherheitszeichen sicherheitskennzeichen",
                "status": "status",
                "supplier": "supplier hersteller",
                "tags": "tags schlagwörter",
                "location": "location standort",
                "devices": "devices geräte",
                }[identifier]
    except KeyError as e:
        name = "fatalerror"
    return name


def get_list_of_commits(commit_query):
    commits = []
    for commit in commit_query:
        # locmemcache doesnt work properly with multithreading, so we disable this now.
        # in case anyone wants to switch over to memcached or something similar, then these lines can be included again
        # also see models.py in public/ for deletion of commit entries
        #commit_dict = cache.get(f'commit_dict-{commit.id}')
        #if commit_dict is None:
        field = commit.field
        data = [escape(commit.old_data), escape(commit.new_data)]
        author = commit.author.username if commit.author else None
        confirmed_by = commit.confirmed_by.username if commit.confirmed_by else None

        if commit.experiment:
            commit_type = 'experiment'
            id = commit.experiment.id
            title = commit.experiment.name
        elif commit.device:
            commit_type = 'device'
            id = commit.device.id
            title = commit.device.name
        elif commit.category:
            commit_type = 'category'
            id = commit.category_id
            title = commit.category.name
        elif commit.label_id:
            commit_type = 'label'
            id = commit.label_id
            title = commit.label.name
        elif commit.location_id:
            commit_type = 'location'
            id = commit.location_id
            title = commit.location.name
        else:
            continue  # no applicable type found; malformed or unknown commit
        commit_id = commit.id
        commit_dict = {'field': field,
                       'data': data,
                       'author': author,
                       'status': commit.status,
                       'commit_type': commit_type,
                       'id': id,
                       'commit_id': commit_id,
                       'title': title,
                       'timestamp': commit.timestamp,
                       'confirmed_at': commit.confirmed_at,
                       'confirmed_by': confirmed_by,
                       'human_readable_field': get_translations(field)}
        #cache.set(f'commit_dict-{commit.id}', commit_dict)
        # end of the if intendation, if there was an if intendation
        commits.append(commit_dict)
    return commits
