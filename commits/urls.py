from django.urls import path
from . import views

urlpatterns = [
    path('', views.commits_overview, name='commits'),
    path('handle_commit/', views.handle_commit),
    path('<str:_>/', views.commits_overview),
    path('needs_confirmation/', views.commits_overview, name='unconfirmed_commits'),
]
