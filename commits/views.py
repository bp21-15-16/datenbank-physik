from django.http import HttpResponse
from django.shortcuts import render
from public.models import Commit
import time
from . import utilities
import django.utils.timezone
from accounts.decorators import *
from django.contrib.auth.decorators import login_required


@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Änderungen'])
def commits_overview(request, _=None):
    start_timestamp = time.time()
    commit_query = Commit.objects\
        .select_related('author', 'confirmed_by', 'experiment', 'device', 'location')\
        .exclude(field='!migration')\
        .order_by('-timestamp')
    commits = utilities.get_list_of_commits(commit_query)
    context = {'commits': commits,
               'main_title': 'Änderungen',
               'start_timestamp': start_timestamp,
               'view_timestamp': time.time(),
               }
    return render(request, 'commits/commits.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Änderungen freigeben'])
def handle_commit(request):
    if request.method == 'POST':
        try:
            commit_id = request.POST.get('commit_id')
            commit = Commit.objects.select_related('experiment', 'device').get(id=commit_id)
            if commit.status == 'not_handled':
                action = request.POST.get('action')
                current_time = django.utils.timezone.now()
                if action == 'accept':  # we're accepting the commit
                    commit.confirmed_at = current_time
                    commit.confirmed_by = request.user
                    commit.status = 'man_approved'
                    commit.save()
                    if commit.experiment_id:  # and we're updating the experiment
                        if commit.field == '!delete':
                            commit.experiment.delete()
                        else:
                            if commit.field == 'name':
                                commit.experiment.name = commit.new_data
                            elif commit.field == 'description':
                                commit.experiment.description = commit.new_data
                            elif commit.field == 'comment':
                                commit.experiment.comment = commit.new_data
                            commit.experiment.save()
                        response = '''<div class="commit_confirmed_text">
                                        <span style="color:darkgreen">Die Änderung am Versuch wurde erfolgreich bestätigt.</span>
                                      </div>'''
                    elif commit.device_id:  # or alternatively updating the appropriate device
                        if commit.field == '!delete':
                            commit.device.delete()
                        else:
                            if commit.field == 'name':
                                commit.device.name = commit.new_data
                            elif commit.field == 'description':
                                commit.device.description = commit.new_data
                            elif commit.field == 'comment':
                                commit.device.comment = commit.new_data
                            commit.device.save()
                        response = '''<div class="commit_confirmed_text">
                                        <span style="color:darkgreen">Die Änderung am Gerät wurde erfolgreich bestätigt.</span>
                                      </div>'''
                    elif commit.category_id:
                        if commit.field == '!delete':
                            commit.category.delete()
                        response = '''<div class="commit_confirmed_text">
                                        <span style="color:darkgreen">Die Änderung an der Kategorie wurde erfolgreich bestätigt.</span>
                                      </div>'''
                    elif commit.location_id:
                        if commit.field == '!delete':
                            commit.location.delete()
                        response = '''<div class="commit_confirmed_text">
                                        <span style="color:darkgreen">Die Änderung an dem Standort wurde erfolgreich bestätigt.</span>
                                      </div>'''
                    else:
                        response = '''<div class="commit_confirmed_text">
                                        <span style="color: red">Die Anfrage konnte nicht verarbeitet werden</span><br/>
                                      </div>'''

                elif action == 'deny':  # denying the commit. no need to update experiments or devices
                    commit.confirmed_at = current_time
                    commit.confirmed_by = request.user
                    commit.status = 'man_rejected'
                    commit.save()
                    response = '''<div class="commit_confirmed_text">
                                    <span style="color:red">Die Änderung wurde erfolgreich abgelehnt.</span><br/>
                                  </div>'''
            else:
                response = '''<div class="commit_confirmed_text">
                                <span style="color:red">Diese Änderung wurde bereits bearbeitet.</span><br/>
                              </div>'''

        except Exception as e:  # try catch absolutely everything
            response = f'''<div class="commit_confirmed_text">
                            <span style="color: red">Beim Verarbeiten der Anfrage ist ein fataler Fehler aufgetreten.</span>
                          </div>'''
        return HttpResponse(response, status=200)
