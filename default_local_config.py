# This file provides default values for the local_config to fall back to.
# It is however strongly recommended creating an appropriate local_config.py, due to security reasons alone.
# The local_config.py needs all of these values and will look very similar to this, except with custom, private values
# This file should be updated and committed whenever changes are being made in the local_config or settings

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# default: Not so random "Secret" Key, used by Django as a Salt in various places.
SECRET_KEY = "django-insecure-6dgajwuc5sm%8dq3!ej*%rh#-c3x&(^a7=a4q_oi(0#k#3e1z*"

CSRF_TRUSTED_ORIGINS = []

# default: Database login credentials and hostname for locally hosted database
DATABASE_USER = 'root'
DATABASE_PASSWORD = ''
DATABASE_HOST = '127.0.0.1'

# default: get images from local media folder
MEDIA_URL = 'media/'
MEDIA_ROOT = 'media'

# default: Email-Settings
EMAIL_HOST = ''
EMAIL_PORT = ''
EMAIL_USE_TLS = True
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
DEFAULT_FROM_EMAIL = ''
DEFAULT_RECIPIENT_ADDRESS = ['', ]
