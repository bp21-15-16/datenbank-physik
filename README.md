## Datenbank der Vorlesungsassistenz Physik
### Ziele dieser Anwendung
Diese Anwendung wurde im Rahmen des Bachelorpraktikums 2021/22 der TU Darmstadt entwickelt. Es handelt sich hierbei um eine Webanwendung, die die Aufgaben der Vorlesungsassistenz (VLA) Physik vereinfachen soll.
### Funktionen
Es stehen folgende Funktionalitäten zur Vefügung:
1. Übersicht für Versuche und Inventar
2. Erstellen, Bearbeiten und Löschen von Versuchen und Geräten, inklusive Dateiupload
3. Kategorien bzw. Standorte, nach denen die Versuche bzw. die Geräte gruppiert sind
4. Filter und Suche für Versuche und Inventar
5. Detailansicht für einzelne Versuche und Geräte
6. Label, um bei Versuchen ganze Gruppen von Geräten referenzieren zu können
7. Inventuren
8. Geräteverleih und Buchhaltung von Geräten in der Werkstatt
9. Buchungen von Versuchen
10. Modulare Rechteverwaltung (Eine Tabelle aller Rechte liegt bei)
11. Eine Änderungshistorie und die Sicherheitsfunktion, dass größere Änderungen von einem Admin angenommen werden müssen, bevor sie wirksam werden
### Verwendete Technologien
Für das Frontend wurden CSS mit der Erweiterung Less CSS, HTML erweitert durch die Django Template Language, und JavaScript in Kombination mit den Bibliotheken JQuery, Tagify, Select2, Slick, Flatpickr und MathJax genutzt.
Das Backend wurde in Python mithilfe des Web-Frameworks Django realisiert. Die Anwendung operiert dabei auf einer MySQL-Datenbank.
### Installationsanleitung
Eine detaillierte Installationsanleitung zur Weiterentwicklung dieser Anwendung liegt dem Projekt bei.
### Deployment-Anleitung
Eine Anleitung zum Aufsetzen der Anwendung liegt dem Projekt ebenfalls bei.
### Lizenz
Dieses Projekt unterliegt der GNU General Public License v3.0. Die Lizenz liegt dem Projekt bei. Mehr Informationen zur GNU GPLv3 [hier](https://www.gnu.org/licenses/gpl-3.0.en.html#:~:text=The%20GNU%20General%20Public%20License%20is%20a%20free%2C,your%20freedom%20to%20share%20and%20change%20the%20works.?msclkid=3365a854a61111ec8ee07b51ef781a80).
