from django.shortcuts import redirect


def unauthenticated_user(view_func):
    """Users that are already logged in should not be able to access the /login url"""
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('home')

        return view_func(request, *args, **kwargs)

    return wrapper_func


def allowed_users(allowed_roles=[], url='home'):
    """Checks if user is in the needed group for this view"""
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):

            group = None
            if request.user.groups.exists():
                group = list(request.user.groups.all().values_list('name', flat=True))

            if not set(group).isdisjoint(allowed_roles):
                return view_func(request, *args, **kwargs)
            else:
                return redirect(url)
        return wrapper_func
    return decorator
