from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, UsernameField, SetPasswordForm, \
    PasswordChangeForm

from labels.models import Color
from .models import User, PermissionPreset


class CreateUserForm(UserCreationForm):
    """Form for creating a new user"""
    username = UsernameField(
        label='Nutzername',
        help_text='',
        widget=forms.TextInput(
            attrs={'autofocus': True,
                   'autocomplete': 'off',
                   'placeholder': "Nutzername...",
                   }
        )
    )

    email = forms.EmailField(
        label='E-Mail Adresse',
        widget=forms.EmailInput(
            attrs={'placeholder': "E-Mail...",
                   }
        )
    )

    password1 = forms.CharField(
        label='Passwort',
        widget=forms.PasswordInput(
            attrs={
                'placeholder': "Passwort...",
            }
        )
    )
    password2 = forms.CharField(
        label='Passwort wiederholen',
        widget=forms.PasswordInput(
            attrs={'placeholder': "Passwort wiederholen...",
                   }
        )
    )

    color = forms.ModelChoiceField(
        required=False,
        queryset=Color.objects.filter(type='user'),
        empty_label="Farbe auswählen...",
        label="Farbe",
        widget=forms.Select(
            attrs={
                'class': "color_select",
                'id': "color_id",
            }
        )
    )

    preset = forms.ModelChoiceField(
        required=False,
        queryset=PermissionPreset.objects.order_by('-pk'),
        widget=forms.Select(
            attrs={
                'onChange': "applyPermissionsFromPreset(this);",
            }
        ),
        empty_label="Benutzerdefiniert",
    )

    groups = forms.CharField(
        label="Rechte",
        required=False,
        widget=forms.Textarea(
            attrs={
                'class': "group_checkbox",
            }
        )
    )

    field_order = ['username', 'email', 'password1', 'password2', 'color', 'preset', 'groups']

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'preset', 'color',)


class ChangeUserForm(UserChangeForm):
    """Form for changing user data"""
    password = None
    username = forms.CharField(
        label="Nutzername",
        max_length=100,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': "Nutzername...",
            }
        )
    )

    email = forms.EmailField(
        label="E-Mail Adresse",
        required=True,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': "E-Mail...",
            }
        )
    )

    color = forms.ModelChoiceField(
        required=False,
        queryset=Color.objects.filter(type='user'),
        empty_label="Farbe auswählen...",
        label="Farbe",
        widget=forms.Select(
            attrs={
                'class': "color_select",
                'id': "color_id",
            }
        )
    )

    preset = forms.ModelChoiceField(
        required=False,
        queryset=PermissionPreset.objects.order_by('-pk'),
        widget=forms.Select(
            attrs={
                'onChange': "applyPermissionsFromPreset(this);",
            }
        ),
        empty_label="Benutzerdefiniert",
    )

    groups = forms.CharField(
        label="Rechte",
        required=False,
        widget=forms.Textarea(
            attrs={
                'class': "group_checkbox",
            }
        )
    )

    field_order = ['username', 'email', 'color', 'preset', 'groups']

    class Meta:
        model = User
        fields = ('username', 'email', 'preset', 'color',)


class ChangePasswordForm(SetPasswordForm):
    """Form for changing a users password"""
    new_password1 = forms.CharField(
        label='Neues Passwort',
        widget=forms.PasswordInput(
            attrs={'placeholder': "Neues Passwort...",
                   }
        )
    )

    new_password2 = forms.CharField(
        label='Neues Passwort wiederholen',
        widget=forms.PasswordInput(
            attrs={'placeholder': "Neues Passwort...",
                   }
        )
    )

    class Meta:
        model = User


class ChangePresetForm(forms.ModelForm):
    """Form for changing a preset"""
    preset = forms.ModelChoiceField(
        required=False,
        queryset=PermissionPreset.objects.order_by('-pk'),
        widget=forms.Select(
            attrs={
                'onChange': "changeNameField(this); applyPermissionsFromPreset(this); toggleDelete(this);",
                'class': "preset_select",
            }
        ),
        empty_label="Neues Preset:",
    )

    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Name...",
            }
        )
    )

    groups = forms.CharField(
        label="Rechte",
        required=False,
        widget=forms.Textarea(
            attrs={
                'class': "group_checkbox",
            }
        )
    )

    field_order = ['preset', 'name', 'groups']

    class Meta:
        model = PermissionPreset
        fields = ('name',)


class ProfileForm(UserChangeForm):
    """Form for a user to edit his username and password"""
    password = None
    username = forms.CharField(
        label="Nutzername",
        max_length=100,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': "Nutzername...",
            }
        )
    )

    email = forms.EmailField(
        label="E-Mail Adresse",
        required=True,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': "E-Mail...",
            }
        )
    )

    field_order = ['username', 'email']

    class Meta:
        model = User
        fields = ('username', 'email')


class ProfilePasswordForm(PasswordChangeForm):
    old_password = forms.CharField(
        label='Altes Passwort',
        widget=forms.PasswordInput(
            attrs={'placeholder': "Altes Passwort...",
                   }
        )
    )

    new_password1 = forms.CharField(
        label='Neues Passwort',
        widget=forms.PasswordInput(
            attrs={'placeholder': "Neues Passwort...",
                   }
        )
    )

    new_password2 = forms.CharField(
        label='Neues Passwort wiederholen',
        widget=forms.PasswordInput(
            attrs={'placeholder': "Neues Passwort...",
                   }
        )
    )

    class Meta:
        model = User


class ColorForm(forms.ModelForm):
    """Form for picking a color"""
    selector = forms.ModelChoiceField(
        required=False,
        queryset=Color.objects.filter(type='user').order_by('-pk'),
        widget=forms.Select(
            attrs={
                'onChange': "changeNameField(this); toggleDelete(this);",
                'id': "select_id",
            }
        ),
        empty_label="Neue Farbe:",
        label="Farbe",
    )

    name = forms.CharField()

    color = forms.CharField(
        initial='#b3b3b3',
        widget=forms.TextInput(attrs={
            'type': 'color',
        }),
        label="Farbwert"
    )

    field_order = ['selector', 'name', 'color']

    class Meta:
        model = Color
        fields = ('name', 'color')

    def clean_name(self):
        """Checks if a color with this name already exists, returns name if not"""
        name = self.cleaned_data.get('name')
        instance = self.instance

        try:
            match = Color.objects.get(name=name, type='user')
            if match == instance:
                return name
        except Color.DoesNotExist:
            # Unable to find a color, this is fine
            return name

        # A color was found with this as a username, raise an error
        raise forms.ValidationError("Dieser Name ist bereits vergeben.")
