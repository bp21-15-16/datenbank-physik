from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import PermissionsMixin, Group
from django.db import models
from django.utils.translation import gettext_lazy as _

from labels.models import Color


class PermissionPreset(models.Model):
    """DB model for presets"""
    name = models.CharField(
        _('preset name'),
        unique=True,
        max_length=80,
    )

    groups = models.ManyToManyField(Group)

    def __str__(self):
        """Returns presets name as a sring"""
        return self.name

    def getGroups(self):
        """Gets groups of the preset"""
        return self.groups


class User(AbstractUser):
    """DB model for users"""
    email = models.EmailField(
        _('email address'),
        unique=False,
        blank=True,
    )

    preset = models.ForeignKey(PermissionPreset, on_delete=models.SET_NULL, null=True)
    color = models.ForeignKey(Color, on_delete=models.SET_NULL, null=True)

    def get_color(self):
        return self.color.color if self.color else '#b3b3b3'

    def hasGroup(self, group):
        """Returns true if a given user is in a certain group"""
        return self.groups.filter(name=group).exists()
