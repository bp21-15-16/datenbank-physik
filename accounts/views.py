from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import render

from labels.utilities import *
from . import forms
from .decorators import *
from .models import *
from .utilities import *

# application wide declarations
User = get_user_model()  # Returns the custom user model defined in models.py


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Nutzer'], url='user')
def registerPage(request):
    """Creating a new user"""
    groups = list(Group.objects.all().values('id', 'name'))
    form = forms.CreateUserForm()
    if request.method == 'POST':
        form = forms.CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            addGroupsToUser(form.data['groups'], user)

            if form.data['preset'] != '':
                updateUserPreset(user)

            return redirect('user')

    context = {
        'form': form,
        'main_title': "Nutzer erstellen",
        'page_buttons': [["Rechte bearbeiten", 'rights_edit', 'icons/key-fill.svg'],
                         ["Farben verwalten", 'user_color_edit', 'icons/palette.svg']],
        'group_presets': getGroupPresets(),
        'js_script': 'js/select_permission_presets.js',
        'groups': groups,
        'color_data': buildColorData('user'),
    }
    return render(request, 'accounts/user_form.html', context)


@unauthenticated_user
def loginPage(request):
    """Login"""
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        next = request.POST.get('next')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect(next if next is not None else 'home')
        else:
            messages.error(request, "Nutzername oder Passwort inkorrekt!")

    context = {
        'main_title': "Login",
    }
    return render(request, 'accounts/login.html', context)


def logoutUser(request):
    """Logout"""
    current_user = request.user
    logout(request)
    response = redirect('home')
    response.delete_cookie('comment_category')

    messages.success(request, f"Nutzer {current_user.username} erfolgreich ausgeloggt.")

    return response


@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Nutzer'])
def user_showAll(request):
    """List of all users"""
    user_list = User.objects.all()

    page_buttons = []
    buttons = []
    if request.user.hasGroup("Schreiben - Nutzer"):
        page_buttons.append(["Neuer Nutzer", 'user_create', 'icons/ic_person_add_24px.svg'])
        page_buttons.append(["Rechte bearbeiten", 'rights_edit', 'icons/key-fill.svg'])

        buttons.append(["Bearbeiten", 'blue', 'user_edit', 'icons/pencil-fill.svg']),
        buttons.append(["Passwort ändern", 'blue', 'user_edit_password', 'icons/key-fill.svg']),
        buttons.append(["Löschen", 'red', 'user_delete', 'icons/person-x-fill.svg'])

    context = {
        'user_list': user_list,
        'main_title': "Nutzer",
        'page_buttons': page_buttons,
        'buttons': buttons,
    }
    return render(request, 'accounts/user_list.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Nutzer'], url='user')
def user_edit(request, userid):
    """Editing a user"""
    groups = list(Group.objects.all().values('id', 'name'))
    # get the user object from User model with the given id
    try:
        current_user = User.objects.get(id=userid)
    except ObjectDoesNotExist as e:
        raise Http404
    # get list of groups of current user
    initial_groups = sorted(list(current_user.groups.all().values('id', 'name')), key=lambda k: k['name'])

    if request.method == 'POST':
        form = forms.ChangeUserForm(request.POST, instance=current_user)
        if form.is_valid():
            user = form.save()
            user.groups.clear()
            addGroupsToUser(form.data['groups'], user)

            if form.data['preset'] != '':
                updateUserPreset(user)

            messages.success(request, f"Nutzer {user.username} erfolgreich geändert.")
            return redirect('user')
    else:
        form = forms.ChangeUserForm(instance=current_user)

    context = {
        'form': form,
        'userid': userid,
        'main_title': f"Nutzer {current_user.username} bearbeiten",
        'page_buttons': [["Rechte bearbeiten", 'rights_edit', 'icons/key-fill.svg'],
                         ["Farben verwalten", 'user_color_edit', 'icons/palette.svg']],
        'group_presets': getGroupPresets(),
        'js_script': 'js/select_permission_presets.js',
        'groups': groups,
        'initial': initial_groups,
        'color_data': buildColorData('user'),
    }
    return render(request, 'accounts/user_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Nutzer'], url='user')
def user_edit_password(request, userid):
    """Editing a user password"""
    try:
        if request.user.id == userid:
            current_user = request.user
        else:
            current_user = User.objects.get(id=userid)
    except ObjectDoesNotExist as e:
        raise Http404

    if request.method == 'POST':
        form = forms.ChangePasswordForm(current_user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)

            messages.success(request, f"Passwort von {user.username} erfolgreich geändert.")
            return redirect('user')
    else:
        form = forms.ChangePasswordForm(current_user)

    context = {'form': form,
               'main_title': f"Passwort von {current_user.username} bearbeiten",
               }
    return render(request, 'accounts/user_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Nutzer'], url='user')
def edit_rights(request):
    """Editing rights of a preset"""
    groups = list(Group.objects.all().values('id', 'name'))

    message = None

    if request.method == 'POST':
        form = forms.ChangePresetForm(request.POST)
        presetid = form.data['preset']
        # check if preset already exists, if so overwrite it, if not create new one
        if presetid != '':
            preset = PermissionPreset.objects.get(id=presetid)
            form = forms.ChangePresetForm(request.POST, instance=preset)
            message = f"Preset {preset.name} erfolgreich geändert."
        else:
            form = forms.ChangePresetForm(request.POST)

        if form.is_valid():
            preset = form.save()
            groups = form.data['groups']
            preset.groups.clear()
            if groups != '':
                groups = json.loads(groups)
                for g in groups:
                    preset.groups.add(g.get('value'))

            updateUserRights(preset)

            if message is None:
                message = f"Preset {preset.name} erfolgreich erstellt."
            messages.success(request, message)

            return redirect('user')
    else:
        form = forms.ChangePresetForm()

    context = {'form': form,
               'main_title': "Rechte bearbeiten",
               'group_presets': getGroupPresets(),
               'js_script': 'js/select_permission_presets.js',
               'groups': groups,
               }
    return render(request, 'accounts/user_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Nutzer'], url='user')
def preset_delete(request, presetid):
    """Delete a preset"""
    preset = None
    try:
        p = PermissionPreset.objects.get(id=presetid)
        p.delete()
        messages.success(request, f"Preset {p.name} erfolgreich gelöscht.")
    except PermissionPreset.DoesNotExist:
        messages.error(request, "Das angeforderte Preset existiert nicht.")
        return redirect('rights_edit')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('rights_edit')

    return redirect('rights_edit')


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Nutzer'], url='user')
def user_delete(request, userid):
    """Delete a user"""
    user = None
    try:
        u = User.objects.get(id=userid)
        u.delete()
        messages.success(request, f"Nutzer {u.username} erfolgreich gelöscht.")
    except User.DoesNotExist:
        messages.error(request, "Der angeforderte Nutzer existiert nicht.")
        return redirect('user')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('user')

    return redirect('user')


@login_required(login_url='login')
def profile(request):
    """Show user profile"""
    current_user = request.user
    if request.method == 'POST':
        form = forms.ProfileForm(request.POST, instance=current_user)
        if form.is_valid():
            user = form.save()

            messages.success(request, f"Nutzer {user.username} erfolgreich geändert.")
            return redirect('home')
    else:
        form = forms.ProfileForm(instance=current_user)

    context = {
        'form': form,
        'main_title': f"Nutzer {current_user.username} bearbeiten",
        'page_buttons': [["Passwort ändern", 'profile_password', 'icons/key-fill.svg']],
    }

    return render(request, 'accounts/user_form.html', context)


@login_required(login_url='login')
def profile_password(request):
    """Changing own user password"""
    current_user = request.user
    if request.method == 'POST':
        form = forms.ProfilePasswordForm(current_user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)

            messages.success(request, f"Passwort von {user.username} erfolgreich geändert.")
            return redirect('profile')
    else:
        form = forms.ProfilePasswordForm(current_user)

    context = {'form': form,
               'main_title': f"Passwort von {current_user.username} bearbeiten",
               }
    return render(request, 'accounts/user_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Nutzer'], url='user')
def user_color(request):
    """Picking a color"""
    color_list = [[x[0], str(x[1])] for x in list(Color.objects.filter(type='user').values_list('id', 'color'))]
    message = None
    if request.method == 'POST':
        form = forms.ColorForm(request.POST)

        colorid = form.data['selector']
        if colorid != '':
            color = Color.objects.get(id=colorid)
            form = forms.ColorForm(request.POST, instance=color)
            message = f"Farbe {color.name} erfolgreich geändert."

        if form.is_valid():
            color = form.save()
            color.type = 'user'
            color.save()

            if message is None:
                message = f"Farbe {color.name} erfolgreich erstellt."
            messages.success(request, message)

            return redirect('user')

    else:
        form = forms.ColorForm()

    context = {
        'form': form,
        'main_title': "Farben verwalten",
        'js_script': 'js/color_form.js',
        'color_list': color_list,
        'type': 'user',
    }

    return render(request, 'forms/color.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Nutzer'], url='user')
def user_color_delete(request, colorid):
    """Deleting a color"""
    try:
        c = Color.objects.get(id=colorid)
        c.delete()
        messages.success(request, f"Farbe {c.name} erfolgreich gelöscht.")
    except Color.DoesNotExist:
        messages.error(request, "Die angeforderte Farbe existiert nicht.")
        return redirect('user_color_edit')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('user_color_edit')

    return redirect('user_color_edit')
