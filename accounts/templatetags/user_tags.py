from django import template
from django.contrib.auth.models import Group
from ..models import PermissionPreset

register = template.Library()

# Defines common tags that can be used in templates to check certain attributes of user


# returns true if a user is in a given group
@register.filter(name='has_group')
def has_group(user, group_name):
    group = Group.objects.filter(name=group_name)
    if group:
        group = group.first()
        return group in user.groups.all()
    else:
        return False
