import json

from .models import PermissionPreset, User


def getGroupPresets():
    """
    Gets all presets with their groups
    :return: a 2d list of all presets with their respective groups in the form of [[<preset_id>, <group_id>], ...],
    where each entry resembles a relationship between a preset and a group,
    a preset with x groups will result in x entries
    """
    # remove entries that contain "None" as Group
    group_list = PermissionPreset.objects.exclude(groups=None).values_list('id', 'groups', 'groups__name').order_by('groups__name')
    group_list = list(group_list)
    group_list = [list(x) for x in group_list]
    return group_list


def addGroupsToUser(groups, user):
    """Add groups to a user"""
    if groups != '':
        groups = json.loads(groups)
        for g in groups:
            user.groups.add(g.get('value'))


def updateUserRights(preset):
    """Updates all users rights if they have the given preset"""
    user_list = User.objects.filter(preset=preset.id)
    groups = preset.groups.all()
    for u in user_list:
        u.groups.clear()
        for g in groups:
            u.groups.add(g.id)


def updateUserPreset(user):
    """
    Sets the preset field of the given user to none (Benutzerdefiniert) if
    selected groups and selected preset don't match
    """
    groups = list(user.groups.all())
    preset = list(user.preset.groups.all())
    if groups != preset:
        user.preset = None
        user.save()
