from django.urls import path, re_path

from public import views as public
from . import views

urlpatterns = [
    path('login/', views.loginPage, name="login"),
    path('logout/', views.logoutUser, name="logout"),

    path('profile/', views.profile, name="profile"),
    path('profile/password', views.profile_password, name="profile_password"),

    path('user/', views.user_showAll, name="user"),
    path('user/create/', views.registerPage, name="user_create"),
    re_path(r'^user/(?P<userid>\d+)/delete$', views.user_delete, name="user_delete"),
    re_path(r'^user/(?P<userid>\d+)/edit$', views.user_edit, name="user_edit"),
    re_path(r'^user/(?P<userid>\d+)/edit/password$', views.user_edit_password, name="user_edit_password"),

    path('', public.welcome, name="home"),

    path('user/rights/', views.edit_rights, name="rights_edit"),
    re_path(r'^user/rights/(?P<presetid>\d+)/delete$', views.preset_delete, name="preset_delete"),
    path('user/color/', views.user_color, name="user_color_edit"),
    re_path(r'^user/color/(?P<colorid>\d+)/delete$', views.user_color_delete, name="user_color_delete"),
]


