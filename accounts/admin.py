from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CreateUserForm, UserChangeForm
from .models import User


class UserAdmin(UserAdmin):
    add_form = CreateUserForm
    form = UserChangeForm
    model = User
    list_display = ['email', 'username',]

try:
    admin.site.register(User, UserAdmin)
except admin.sites.AlreadyRegistered:
    pass