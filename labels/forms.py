from django import forms

from public.models import Label
from .utilities import *


class CreateLabelForm(forms.ModelForm):
    """Form for creating a new label"""
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Labelname...",
            }
        )
    )

    color = forms.ModelChoiceField(
        required=False,
        queryset=Color.objects.filter(type='label'),
        empty_label="Farbe auswählen...",
        label="Farbe",
        widget=forms.Select(
            attrs={
                'class': "color_select",
                'id': "color_id",

            }
        )
    )

    experiment = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': "experiment_select",
                'id': "experiment_id",
                'placeholder': "Versuche auswählen...",
            }
        ),
        label="Zugewiesene Versuche",
        required=False,
    )

    device = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': "device_select",
                'id': "device_id",
                'placeholder': "Geräte auswählen...",
            }
        ),
        label="Geräte",
        required=False,
    )

    class Meta:
        model = Label
        fields = '__all__'

    def clean_name(self):
        """Checks if any label with this name already exists, returns name if not"""
        name = self.cleaned_data.get('name')

        try:
            Label.objects.get(name=name)
        except Label.DoesNotExist:
            # Unable to find a label, this is fine
            return name

        # A label was found with this as a name, raise an error
        raise forms.ValidationError("Dieser Name ist bereits vergeben.")


class ChangeLabelForm(forms.ModelForm):
    """Form for editing a label"""
    color = forms.ModelChoiceField(
        required=False,
        queryset=Color.objects.filter(type='label'),
        label="Farbe",
        empty_label="Farbe auswählen...",
        widget=forms.Select(
            attrs={
                'class': "color_select",
                'id': "color_id",
            }
        )
    )

    experiment = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': "experiment_select",
                'id': "experiment_id",
                'placeholder': "Versuche auswählen...",
            }
        ),
        label="Zugewiesene Versuche",
        required=False,
    )

    device = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': "device_select",
                'id': "device_id",
                'placeholder': "Geräte auswählen...",
            }
        ),
        label="Geräte",
        required=False,
    )

    class Meta:
        model = Label
        fields = '__all__'

    def clean_name(self):
        """Checks if any label with this name already exists, returns name if not"""
        name = self.cleaned_data.get('name')
        instance = self.instance

        try:
            match = Label.objects.get(name=name)
            if match == instance:
                return name
        except Label.DoesNotExist:
            # Unable to find a label, this is fine
            return name

        # A label was found with this name, raise an error
        raise forms.ValidationError("Dieser Name ist bereits vergeben.")


class ColorForm(forms.ModelForm):
    """Form for picking a color"""
    selector = forms.ModelChoiceField(
        required=False,
        queryset=Color.objects.filter(type='label').order_by('-pk'),
        widget=forms.Select(
            attrs={
                'onChange': "changeNameField(this); toggleDelete(this);",
                'id': "select_id",
            }
        ),
        empty_label="Neue Farbe:",
        label="Farbe"
    )

    name = forms.CharField(
        widget=forms.TextInput(attrs={
            'placeholder': 'Name der Farbe...'
        }),
    )

    color = forms.CharField(
        initial='#b3b3b3',
        widget=forms.TextInput(attrs={
            'type': 'color'
        }),
        label="Farbwert"
    )

    field_order = ['selector', 'name', 'color']

    class Meta:
        model = Color
        fields = ('name', 'color')

    def clean_name(self):
        """Checks if any label with this color already exists, returns name if not"""
        name = self.cleaned_data.get('name')
        instance = self.instance

        try:
            match = Color.objects.get(name=name, type='label')
            if match == instance:
                return name
        except Color.DoesNotExist:
            # Unable to find a color, this is fine
            return name

        # A color was found with this name, raise an error
        raise forms.ValidationError("Dieser Name ist bereits vergeben.")
