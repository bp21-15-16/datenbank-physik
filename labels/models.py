from django.db import models


class Color(models.Model):
    """DB model for colors"""
    name = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    COLOR_CHOICES = (
        ('label', "Label"),
        ('user', "Nutzer"),
    )
    type = models.CharField(max_length=255, choices=COLOR_CHOICES, null=True)

    def __str__(self):
        return self.name
