from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import render

from accounts.decorators import *
from public.models import Label, Device, Experiment
from . import forms
from .utilities import *


@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Label'])
def label_showAll(request):
    """Overview of all labels"""
    label_list = Label.objects.all()

    page_buttons = []
    buttons = [["Ansehen", 'blue', 'label_view', 'icons/ic_eye_24px.svg']]
    if request.user.hasGroup("Schreiben - Label"):
        page_buttons.append(["Neues Label", 'label_create', 'icons/ic_add_24px.svg'])
        page_buttons.append(["Farben verwalten", 'color_edit', 'icons/palette.svg'])

        buttons.append(["Bearbeiten", 'blue', 'label_edit', 'icons/pencil-fill.svg'])
        buttons.append(["Löschen", 'red', 'label_delete', 'icons/x.svg'])

    context = {
        'label_list': label_list,
        'main_title': "Labels",
        'page_buttons': page_buttons,
        'buttons': buttons,
    }
    return render(request, 'labels/label_list.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Label'], url='label')
def label_edit(request, labelid):
    """Editing an existing label"""
    # Builds json formatted lists of all experiments and devices
    experiment_list = getExperiments(Experiment.objects)
    device_list = getDevices(Device.objects)

    # Get the current label object
    try:
        current_label = Label.objects.get(id=labelid)
    except ObjectDoesNotExist as e:
        raise Http404

    # Get correctly formatted list of all devices belonging to the label
    label_devices = getDevices(current_label.device_set)
    label_devices = json.dumps([dict(x, clean_name=escape(x["name"]), value=x["id"]) for x in label_devices])

    # Get correctly formatted list of all experiments belonging to the label
    label_experiments = getExperiments(current_label.experiment_set)
    label_experiments = json.dumps([dict(x, clean_name=escape(x["name"]), value=x["id"]) for x in label_experiments])

    if request.method == 'POST':
        form = forms.ChangeLabelForm(request.POST, instance=current_label)
        if form.is_valid():
            label = form.save()

            # Clear all experiments and devices from label
            label.experiment_set.clear()
            label.device_set.clear()

            # Decode json data from form of selected experiments and devices
            experiments = form.data['experiment']
            if experiments != '':
                experiments = json.loads(experiments)
                for e in experiments:
                    label.experiment_set.add(e.get('value'))

            devices = form.data['device']
            if devices != '':
                devices = json.loads(devices)
                for d in devices:
                    label.device_set.add(d.get('value'))

            messages.success(request, f"Label {label.name} erfolgreich geändert.")
            return redirect('label')

    else:
        initial = {
            'device': label_devices,
            'experiment': label_experiments,
        }
        form = forms.ChangeLabelForm(instance=current_label, initial=initial)

    context = {
        'main_title': f"Label {current_label.name} bearbeiten",
        'form': form,
        'color_data': buildColorData('label'),
        'js_script': 'js/label_form.js',
        'experiment_list': experiment_list,
        'device_list': device_list,
        'page_buttons': [["Farben verwalten", 'color_edit', 'icons/palette.svg']]
    }

    return render(request, 'labels/label_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Label'], url='label')
def label_create(request):
    """Creating a new label"""
    # Builds json formatted lists of all experiments and devices
    experiment_list = getExperiments(Experiment.objects)
    device_list = getDevices(Device.objects)

    form = forms.CreateLabelForm()
    if request.method == 'POST':
        form = forms.CreateLabelForm(request.POST)
        if form.is_valid():
            label = form.save()

            # Decode json data from form of selected experiments and devices
            experiments = form.data['experiment']
            if experiments != '':
                experiments = json.loads(experiments)
                for e in experiments:
                    label.experiment_set.add(e.get('value'))

            devices = form.data['device']
            if devices != '':
                devices = json.loads(devices)
                for d in devices:
                    label.device_set.add(d.get('value'))

            messages.success(request, f"Label {label.name} erfolgreich erstellt.")
            return redirect('label')

    context = {
        'form': form,
        'main_tile': "Label erstellen",
        'color_data': buildColorData('label'),
        'js_script': 'js/label_form.js',
        'experiment_list': experiment_list,
        'device_list': device_list,
        'page_buttons': [["Farben verwalten", 'color_edit', 'icons/palette.svg']]
    }

    return render(request, 'labels/label_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Lesen - Label'])
def label_view(request, labelid):
    """Detailed view of one label"""
    try:
        current_label = Label.objects.get(id=labelid)
    except ObjectDoesNotExist as e:
        raise Http404

    context = {
        'current_label': current_label,
        'main_title': f"Label {current_label.name}",
        'page_buttons': [["Übersicht - Labels", 'label', 'icons/ic_eye_24px.svg']],
    }

    return render(request, 'labels/label_overview.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Label'], url='label')
def label_delete(request, labelid):
    """Deleting a label"""
    try:
        l = Label.objects.get(id=labelid)
        l.delete()
        messages.success(request, f"Label {l.name} erfolgreich gelöscht.")
    except Label.DoesNotExist:
        messages.error(request, "Das angeforderte Label existiert nicht.")
        return redirect('label')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('label')

    return redirect('label')


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Label'], url='label')
def label_color(request):
    """Creating a color"""
    color_list = [[x[0], str(x[1])] for x in list(Color.objects.filter(type='label').values_list('id', 'color'))]

    message = None

    if request.method == 'POST':
        form = forms.ColorForm(request.POST)

        colorid = form.data['selector']
        if colorid != '':
            color = Color.objects.get(id=colorid)
            form = forms.ColorForm(request.POST, instance=color)
            message = f"Farbe {color.name} erfolgreich geändert."

        if form.is_valid():
            color = form.save()
            color.type = 'label'
            color.save()

            if message is None:
                message = f"Farbe {color.name} erfolgreich erstellt."
            messages.success(request, message)

            return redirect('label')

    else:
        form = forms.ColorForm()

    context = {
        'form': form,
        'main_title': "Farben verwalten",
        'js_script': 'js/color_form.js',
        'color_list': color_list,
        'type': 'label',
    }

    return render(request, 'forms/color.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['Schreiben - Label'], url='label')
def color_delete(request, colorid):
    """Deleting a color"""
    try:
        c = Color.objects.get(id=colorid)
        c.delete()
        messages.success(request, f"Farbe {c.name} erfolgreich gelöscht.")
    except Color.DoesNotExist:
        messages.error(request, "Die angeforderte Farbe existiert nicht.")
        return redirect('color_edit')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return redirect('color_edit')

    return redirect('color_edit')
