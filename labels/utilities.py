import json

from django.utils.html import escape

from .models import Color


def get_whole_token(object):
    """Returns token of location/category concatenated with its parents token, separated with ."""
    token = object.token
    while object.parent is not None:
        object = object.parent
        token = object.token + "." + token
    return token


def getExperiments(query):
    """Takes a query of experiments and returns json with category tokens appended to the name"""
    experiment_query = list(query.select_related('category', 'category__parent', 'category__parent__parent'))
    exp_choices = []
    for e in experiment_query:
        token = "" if e.category is None else get_whole_token(e.category)
        exp_choices.append({'id': e.id, 'name': f'{token}.{e.experiment_number} {e.name}'})
    return sorted(exp_choices, key=lambda d: d['name'])


def getDevices(query):
    """Gets all devices as a sorted list of dictionaries"""
    device_query = list(query.all())
    device_choices = []
    for d in device_query:
        device_choices.append({'id': d.id, 'name': f'{d.id} {d.name}'})
    return device_choices


def buildColorData(type):
    """Builds the html elements for the color form"""
    color_data = list(Color.objects.filter(type=type).values_list('id', 'name', 'color'))
    color_data = [list(x) for x in color_data]
    for c in color_data:
        c[1] = escape(c[1])  # name is already being escaped here, so we dont need to do it again in the html below
        c[2] = f"""
                <div style="display: flex; justify-content: left; align-items: center;">
                    <div style="height: 15px; width: 15px; background-color: {c[2]}; margin: 5px; border-radius: 5px;"></div>
                    <div style="color:black">{c[1]}</div>
                </div>"""
    color_data.append(
        [
            '',
            "Farbe auswählen...",
            """
            <div style="display: flex; justify-content: left; align-items: center;">
                    <div style="height: 15px; width: 15px; background-color: #b3b3b3; margin: 5px; border-radius: 5px;"></div>
                    <div style="color:black">Farbe auswählen...</div>
            </div>""",
        ]
    )
    return color_data


def encodeToJson(dict):
    """Takes a dictonary and encodes it to json compatible format"""
    encoded = []
    for d in dict:
        d = {'value': d.get('id'), 'name': d.get('name')}
        encoded.append(d)
    encoded = json.dumps(encoded)
    return encoded


def colorJson(dict):
    """Takes a dictonary of ids and colors and encodes it to json compatible format"""
    encoded = []
    for d in dict:
        d = {'value': d.get('id'), 'name': d.get('color')}
        encoded.append(d)
    encoded = json.dumps(encoded)
    return encoded
