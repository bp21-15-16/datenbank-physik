from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.label_showAll, name="label"),
    path('create/', views.label_create, name="label_create"),
    re_path(r'^(?P<labelid>\d+)/edit/$', views.label_edit, name="label_edit"),
    re_path(r'^(?P<labelid>\d+)/delete/$', views.label_delete, name="label_delete"),

    path('color/', views.label_color, name="color_edit"),
    re_path(r'^color/(?P<colorid>\d+)/delete/$', views.color_delete, name="color_delete"),

    re_path(r'^(?P<labelid>\d+)/view/$', views.label_view, name="label_view"),
]